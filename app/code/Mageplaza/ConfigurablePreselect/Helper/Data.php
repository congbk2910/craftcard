<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ConfigurablePreselect\Helper;

use Magento\Catalog\Model\Product;
use Mageplaza\ConfigurablePreselect\Model\Config\Source\PreselectType;
use Mageplaza\Core\Helper\AbstractData;

/**
 * Class Data
 * @package Mageplaza\ConfigurablePreselect\Helper
 */
class Data extends AbstractData
{
    const CONFIG_MODULE_PATH = 'mpconfigurableproductspreselect';

    /**
     * @param null $storeId
     *
     * @return mixed
     */
    public function getPreselectTypeConfig($storeId = null)
    {
        return $this->getConfigGeneral('preselect_type', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return mixed
     */
    public function isApplyCategory($storeId = null)
    {
        return $this->getConfigGeneral('apply_category', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return bool
     */
    public function isProductDefaultAttribute($storeId = null)
    {
        return (bool) $this->isEnabled($storeId) &&
            $this->getPreselectTypeConfig($storeId) === PreselectType::DEFAULT_PRODUCT;
    }

    /**
     * @return bool
     */
    public function applyCategory()
    {
        if (!$this->isEnabled()) {
            return false;
        }

        $listCategoryAction = [
            'catalog_category_view',
            'catalogsearch_result_index'
        ];

        if (in_array($this->_request->getFullActionName(), $listCategoryAction, true)) {
            return (bool) $this->isApplyCategory();
        }

        return true;
    }

    /**
     * @param Product $product
     * @param $type
     *
     * @return mixed|string
     */
    public function getPreselectSku($product, $type)
    {
        $childData    = [];
        $preselectSku = '';
        $children     = $product->getTypeInstance()->getUsedProducts($product);

        /** @var Product $child */
        foreach ($children as $child) {
            if (!$child->getIsSalable()) {
                continue;
            }
            $childData[] = [
                'sku'   => $child->getSku(),
                'price' => $child->getFinalPrice()
            ];
        }

        if ($childData) {
            switch ($type) {
                case PreselectType::CHEAPEST:
                    $preselectSku = $this->getCheapestSku($childData);
                    break;
                case PreselectType::MOST_EXPENSIVE:
                    $preselectSku = $this->getMostExpensiveSku($childData);
                    break;
                case PreselectType::FIRST_ATTRIBUTE:
                    $preselectSku = $childData[0]['sku'];
                    break;
                case PreselectType::DEFAULT_PRODUCT:
                    $preselectSku = $product->getMpDefaultPreselected();
                    break;
            }
        }

        return $preselectSku;
    }

    /**
     * @param $childData
     *
     * @return mixed|string
     */
    public function getMostExpensiveSku($childData)
    {
        $sku      = $childData[0]['sku'];
        $minPrice = 0;

        foreach ($childData as $data) {
            if ($data['price'] > $minPrice) {
                $minPrice = $data['price'];
                $sku      = $data['sku'];
            }
        }

        return $sku;
    }

    /**
     * @param $childData
     *
     * @return mixed|string
     */
    public function getCheapestSku($childData)
    {
        $maxPrice = $childData[0]['price'];
        $sku      = $childData[0]['sku'];

        foreach ($childData as $data) {
            if ($data['price'] < $maxPrice) {
                $maxPrice = $data['price'];
                $sku      = $data['sku'];
            }
        }

        return $sku;
    }
}
