<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ConfigurablePreselect\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class PreselectType
 * @package Mageplaza\ConfigurablePreselect\Model\Config\Source
 */
class PreselectType implements ArrayInterface
{
    const FIRST_ATTRIBUTE = 'first';
    const DEFAULT_PRODUCT = 'default_product';
    const CHEAPEST        = 'cheapest';
    const MOST_EXPENSIVE  = 'most_expensive';

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::FIRST_ATTRIBUTE, 'label' => __('First-option Preselect')],
            ['value' => self::DEFAULT_PRODUCT, 'label' => __('Default-option Preselect')],
            ['value' => self::CHEAPEST, 'label' => __('Cheapest-option Preselect')],
            ['value' => self::MOST_EXPENSIVE, 'label' => __('Most Expensive-option Preselect')],
        ];
    }
}
