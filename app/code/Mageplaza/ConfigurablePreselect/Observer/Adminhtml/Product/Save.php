<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ConfigurablePreselect\Observer\Adminhtml\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Mageplaza\ConfigurablePreselect\Helper\Data as HelperData;

/**
 * Class Save
 * @package Mageplaza\ConfigurablePreselect\Observer\Adminhtml\Product
 */
class Save implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * Save constructor.
     *
     * @param RequestInterface $request
     * @param HelperData $_helperData
     */
    public function __construct(
        RequestInterface $request,
        HelperData $_helperData,
        ProductFactory $productFactory
    ) {
        $this->_request        = $request;
        $this->_helperData     = $_helperData;
        $this->_productFactory = $productFactory;
    }

    /**
     * Set data for product after save
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();
        if ($product->getTypeId() === 'configurable') {
            $matrixData = $this->_request->getParam('configurable-matrix-serialized') ?:
                $this->_request->getParam('product')['configurable-matrix-serialized'];

            if ($matrixData && $this->_helperData->isProductDefaultAttribute($product->getStoreId())) {
                $preselectSku      = '';
                $childProductsData = HelperData::jsonDecode($matrixData);

                foreach ($childProductsData as $childProductData) {
                    if (isset($childProductData['checked']) && $childProductData['checked']) {
                        $preselectSku = $childProductData['sku'];
                    }
                }

                $productId   = $product->getId();
                $productSave = $this->_productFactory->create()->load($productId);

                $productSave->setStoreId($product->getStoreId())
                    ->setData('mp_default_preselected', $preselectSku)
                    ->save();
            }
        }
    }
}
