<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ConfigurablePreselect\Plugin\Block\Product\View\Type;

use Magento\ConfigurableProduct\Block\Product\View\Type\Configurable as TypeConfigurable;
use Mageplaza\ConfigurablePreselect\Helper\Data as HelperData;
use Mageplaza\ConfigurablePreselect\Model\Config\Source\PreselectType;

/**
 * Class Configurable
 * @package Mageplaza\ConfigurablePreselect\Plugin\Block\Product\View\Type
 */
class Configurable
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * Configurable constructor.
     *
     * @param HelperData $helperData
     */
    public function __construct(HelperData $helperData)
    {
        $this->_helperData = $helperData;
    }

    /**
     * @param TypeConfigurable $subject
     * @param $result
     *
     * @return string
     */
    public function afterGetJsonConfig(
        TypeConfigurable $subject,
        $result
    ) {
        if (!$this->_helperData->applyCategory() || $subject->getProduct()->hasPreconfiguredValues()) {
            return $result;
        }

        $product            = $subject->getProduct();
        $type               = $this->_helperData->getPreselectTypeConfig();
        $preSelectSku       = $this->_helperData->getPreselectSku(
            $product,
            $type
        );
        $preSelectProductId = $product->getIdBySku($preSelectSku);

        if ($preSelectSku) {
            $config        = HelperData::jsonDecode($result);
            $defaultValues = [];
            $length        = count($config['attributes']);
            $count         = 0;
            $productIds    = [];

            foreach ($config['attributes'] as $attributeId => $attribute) {
                foreach ($attribute['options'] as $key => $option) {
                    $optionId = $option['id'];

                    if ($type !== PreselectType::FIRST_ATTRIBUTE
                        && in_array($preSelectProductId, $option['products'], true)
                    ) {
                        $defaultValues[$attribute['code']] = $optionId;
                    }
                    if ($count < $length && $type === PreselectType::FIRST_ATTRIBUTE) {
                        if ($key === 0 && count($option['products'])) {
                            if ($count == '0') {
                                $defaultValues[$attribute['code']] = $optionId;
                                $productIds                        = $option['products'];
                                break;
                            }else{
                                if(array_intersect($productIds, $option['products'])){
                                    $defaultValues[$attribute['code']] = $optionId;
                                    $productIds                        = $option['products'];
                                    break;
                                }
                            }
                        } else if (count($option['products'])) {
                            if ($count == '0') {
                                $defaultValues[$attribute['code']] = $optionId;
                                $productIds                        = $option['products'];
                                break;
                            }else{
                                if(array_intersect($productIds, $option['products'])){
                                    $defaultValues[$attribute['code']] = $optionId;
                                    $productIds                        = $option['products'];
                                    break;
                                }
                            }
                        }
                    }
                    if ($count === $length && $type === PreselectType::FIRST_ATTRIBUTE) {
                        if ($key === 0 && array_intersect($productIds, $option['products'])) {
                            $defaultValues[$attribute['code']] = $optionId;
                            break;
                        }
                        if ($key === 1) {
                            $defaultValues[$attribute['code']] = $optionId;
                        }
                    }
                }
                $count++;
            }
            if (isset($config['preSelectedGallery']) && empty($config['preSelectedGallery'])) {
                unset($config['preSelectedGallery']);
            }

            $config['preselect'] = $defaultValues;
            $result              = HelperData::jsonEncode($config);
        }

        return $result;
    }
}
