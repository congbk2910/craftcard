<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ConfigurablePreselect\Plugin\Ui\Component;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form as FormAlias;
use Mageplaza\ConfigurablePreselect\Helper\Data as HelperData;

/**
 * Class Form
 * @package Mageplaza\ConfigurablePreselect\Plugin\Ui\Component
 */
class Form
{
    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Form constructor.
     *
     * @param HelperData $helperData
     * @param StoreManagerInterface $storeManager
     * @param Registry $registry
     */
    public function __construct(
        HelperData $helperData,
        StoreManagerInterface $storeManager,
        Registry $registry
    ) {
        $this->_helperData   = $helperData;
        $this->_storeManager = $storeManager;
        $this->_registry     = $registry;
    }

    /**
     * @param FormAlias $subject
     * @param $result
     *
     * @return mixed
     * @throws NoSuchEntityException
     * @SuppressWarnings(Unused)
     */
    public function afterGetDataSourceData(FormAlias $subject, $result)
    {
        if (!$this->_helperData->isProductDefaultAttribute($this->_storeManager->getStore()->getId())) {
            return $result;
        }

        $product = $this->_registry->registry('current_product');
        /** set checked preselect when load page on product edit page */
        if (isset($result['data']) && $product && $product->getTypeId() === 'configurable') {
            foreach ($result['data'] as $key => $value) {
                if ($key === 'configurable-matrix') {
                    foreach ($value as $matrixKey => $configurableMatrix) {
                        if ($configurableMatrix['sku'] === $product->getMpDefaultPreselected()) {
                            $result['data']['configurable-matrix'][$matrixKey]['checked'] = 1;
                        } else {
                            $result['data']['configurable-matrix'][$matrixKey]['checked'] = 0;
                        }
                    }
                }
            }
        }

        return $result;
    }
}
