<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ConfigurablePreselect\Plugin\Ui\DataProvider\Product\Form\Modifier;

use Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\ConfigurablePanel as ModifierConfigurablePanel;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form;
use Mageplaza\ConfigurablePreselect\Helper\Data as HelperData;

/**
 * Class ConfigurablePanel
 * @package Mageplaza\ConfigurablePreselect\Plugin\Ui\DataProvider\Product\Form\Modifier
 */
class ConfigurablePanel
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * ConfigurablePanel constructor.
     *
     * @param HelperData $helperData
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        HelperData $helperData,
        StoreManagerInterface $storeManager
    ) {
        $this->_helperData   = $helperData;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param ModifierConfigurablePanel $subject
     * @param $result
     *
     * @return mixed
     * @throws NoSuchEntityException
     * @SuppressWarnings(Unused)
     */
    public function afterModifyMeta(ModifierConfigurablePanel $subject, $result)
    {
        if (!$this->_helperData->isProductDefaultAttribute($this->_storeManager->getStore()->getId())) {
            return $result;
        }

        $radioData = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement'   => Form\Element\Input::NAME,
                        'componentType' => Form\Field::NAME,
                        'dataType'      => Form\Element\DataType\Text::NAME,
                        'component'     => 'Mageplaza_ConfigurablePreselect/js/form/element/preselect-radio',
                        'elementTmpl'   => 'Mageplaza_ConfigurablePreselect/form/components/preselect-radio',
                        'label'         => __('Default Preselect Option'),
                        'sortOrder'     => 0
                    ],
                ],
            ],
        ];
        array_unshift(
            $result['configurable']['children']['configurable-matrix']['children']['record']['children'],
            $radioData
        );

        return $result;
    }
}
