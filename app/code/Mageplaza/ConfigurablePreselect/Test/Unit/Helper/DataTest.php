<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Mageplaza
 * @package   Mageplaza_ConfigurablePreselect
 * @copyright Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license   https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ConfigurablePreselect\Test\Unit\Helper;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\ConfigurablePreselect\Helper\Data;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class DataTest
 * @package Mageplaza\ConfigurablePreselect\Test\Unit\Helper
 */
class DataTest extends TestCase
{
    /**
     * @var Context|PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * @type StoreManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    protected $storeManager;

    /**
     * @type ObjectManagerInterface|PHPUnit_Framework_MockObject_MockObject
     */
    protected $objectManager;

    /**
     * @var Data|PHPUnit_Framework_MockObject_MockObject
     */
    private $object;

    protected function setUp()
    {
        $this->context       = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()->getMock();
        $this->objectManager = $this->getMockBuilder(ObjectManagerInterface::class)
            ->getMock();
        $this->storeManager  = $this->getMockBuilder(StoreManagerInterface::class)
            ->getMock();

        $this->object = new Data(
            $this->context,
            $this->objectManager,
            $this->storeManager
        );
    }

    public function testAdminInstance()
    {
        $this->assertInstanceOf(Data::class, $this->object);
    }

    public function testGetPreselectSku()
    {
        $sku   = 'product-sku';
        $price = 100;

        /** @var Product $product */
        $product = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()->getMock();

        $typeInstance = $this->getMockForAbstractClass(
            AbstractType::class,
            [],
            '',
            false,
            false,
            true,
            ['getUsedProducts']
        );

        $product->method('getTypeInstance')->willReturn($typeInstance);
        $typeInstance->method('getUsedProducts')->with($product)->willReturn([$product]);

        $product->method('getSku')->willReturn($sku);
        $product->method('getPrice')->willReturn($price);

        $this->assertEquals($sku, $this->object->getPreselectSku($product, 'cheapest'));
    }
}
