/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'Magento_Ui/js/form/element/single-checkbox',
    'underscore',
    'uiRegistry'
], function ($, Checkbox, _, registry) {
    'use strict';

    return Checkbox.extend({

        /**
         * @inheritdoc
         */
        initialize: function () {
            this._super();

            var provider = registry.get('product_form.product_form_data_source'),
                parent = provider.get(this.parentScope);

            this.checked(parent.checked);

            return this;
        },

        /**
         * @inheritdoc
         */
        initConfig: function () {
            this._super();
            this.inputName = 'configurable-matrix[preselect_radio]';
        },

        /**
         * set checked when click radio
         *
         * @returns {boolean}
         */
        onChecked: function (self) {
            var provider = registry.get('product_form.product_form_data_source'),
                currentData = provider.get(this.parentScope),
                records = provider.get('data.configurable-matrix');

            $('input[name="configurable-matrix[preselect_radio]"]:not(#' + self.uid + ')').prop('checked', false);
            _.each(records, function (record) {
                record.checked = currentData.record_id === record.record_id && record.checked === 0 ? 1 : 0;
            });

            return true;
        }
    });
});