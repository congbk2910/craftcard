/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery'
], function ($) {
    'use strict';

    return function (widget) {
        $.widget('mage.configurable', widget, {
            /**
             * Override default options values settings with either URL query parameters or
             * initialized inputs values.
             * @private
             */
            _overrideDefaults: function () {


                var nbIndex = window.location.href.indexOf('&attribute');
                if(nbIndex !== -1) {
                    var query = window.location.href.substr(nbIndex + 1);
                    // var childIndex = query.indexOf('&');
                    // var optionQuery = query.replace('attribute','');
                    var optionQuery =  query.split('attribute').join('');
                    this._parseQueryParams(optionQuery);
                }
                this._super();
                // this._setInitialOptionsLabels();
            },
            _fillSelect: function (element){
                this._super(element);
                element.remove(0);

            },
            _configureForValues: function () {
                this._super();
                if (this.options.spConfig.defaultValues) {
                    var dataGallery = $(this.options.mediaGallerySelector),
                        option = this.options.spConfig.defaultValues,
                        self = this;

                    var interval = setInterval(function () {
                        var galleryObject = dataGallery.data('gallery');
                        if(galleryObject) {
                            self._changeProductImage();
                            clearInterval(interval);
                        };
                    }, 100);
                }
            },
            /**
             *
             * @private
             */
            _initializeOptions: function () {
                this._super();
                var preselectOps = this.options.spConfig.preselect;

                if (preselectOps) {
                    var self = this,
                        dataGallery = $(this.options.mediaGallerySelector);

                    if (dataGallery.length) {
                        dataGallery.on('gallery:loaded', function () {
                            self.options.settings.each($.proxy(function (index, element) {
                                var code = element.config.code,
                                    preselect = self.options.spConfig.preselect[code];

                                self.element = element;
                                element.value = preselect || '';
                                self._configureElement(element);
                            }, self));
                        });
                    } else {
                        self.options.settings.each($.proxy(function (index, element) {
                            var code = element.config.code,
                                preselect = self.options.spConfig.preselect[code];

                            self.element = element;
                            element.value = preselect || '';
                            self._configureElement(element);
                        }, self));
                    }
                }
            }
        });
    }
});
