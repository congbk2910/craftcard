/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ConfigurablePreselect
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',

    /** use to initialize the price-box so that it will update price when auto-select
     * the reason: in module-swatches/view/frontend/templates/product/listing/renderer.phtml
     * swatch-renderer is loaded before price-box AND doesn't have the auto-select
     * when this extension enable, with this approach (mixin)
     * -> auto-select enable -> select attributes before price-box load -> price is not updated accordingly
     * With layered, price-box is loaded before swatch-renderer
     */

    'Magento_Catalog/js/price-box'
], function ($) {
    'use strict';

    return function (widget) {
        $.widget('mage.SwatchRenderer', widget, {
            /**
             * @inheritDoc
             */
            _EventListener: function () {
                this._super();
                // if (this.options.jsonConfig.defaultValues) {
                //     return;
                // }
                var preselectOps = this.options.jsonConfig.preselect;

                if (preselectOps) {
                    var self = this,
                        dataGallery = $(this.options.mediaGallerySelector);

                    if (dataGallery.length) {
                        dataGallery.on('gallery:loaded', function () {
                            self._EmulateSelected(preselectOps);
                        });
                    } else {
                        self._EmulateSelected(preselectOps);
                    }
                }
            },

            /**
             * fix bug: uncheck after filter on 2.1
             * @inheritDoc
             */
            _EmulateSelected: function (selectedAttributes) {
                $.each(selectedAttributes, $.proxy(function (attributeCode, optionId) {
                    var elem = this.element.find('.' + this.options.classes.attributeClass +
                        '[attribute-code="' + attributeCode + '"] [option-id="' + optionId + '"]'),
                        parentInput = elem.parent();

                    if (elem.hasClass('selected')) {
                        return;
                    }

                    if (parentInput.hasClass(this.options.classes.selectClass)) {
                        parentInput.val(optionId);
                        parentInput.trigger('change');
                    } else {
                        elem.trigger('click');
                    }
                }, this));
            }
        });

        return $.mage.SwatchRenderer;
    };
});
