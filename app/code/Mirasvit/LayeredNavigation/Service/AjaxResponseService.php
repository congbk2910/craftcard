<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-navigation
 * @version   1.0.99
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\LayeredNavigation\Service;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Module\Manager as ModuleManager;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Layout\Element;
use Magento\Framework\View\Result\Page;
use Magento\Store\Model\StoreManagerInterface;
use Mirasvit\LayeredNavigation\Model\Config;
use Mirasvit\LayeredNavigation\Model\Config\HorizontalBarConfig;

class AjaxResponseService
{
    private $resultRawFactory;

    private $urlBuilder;

    private $urlService;

    private $config;

    private $storeId;

    private $request;

    private $horizontalFiltersConfig;

    private $moduleManager;

    public function __construct(
        RawFactory $resultRawFactory,
        UrlInterface $urlBuilder,
        UrlService $urlService,
        Config $config,
        StoreManagerInterface $storeManager,
        Http $request,
        HorizontalBarConfig $horizontalFiltersConfig,
        ModuleManager $moduleManager
    ) {
        $this->resultRawFactory        = $resultRawFactory;
        $this->urlBuilder              = $urlBuilder;
        $this->urlService              = $urlService;
        $this->config                  = $config;
        $this->storeId                 = $storeManager->getStore()->getStoreId();
        $this->request                 = $request;
        $this->horizontalFiltersConfig = $horizontalFiltersConfig;
        $this->moduleManager           = $moduleManager;
    }

    /**
     * @param Page $page
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function getAjaxResponse(Page $page)
    {
        $layout = $page->getLayout();
        if (in_array($this->request->getFullActionName(), ['brand_brand_view', 'all_products_page_index_index', 'catalog_category_view'])) {
            $productsHtml = $this->getBlockHtml($page, 'category.products.list');
        } else {
            $productsHtml = $this->getBlockHtml($page, 'category.products', 'search.result');
        }
        $leftNavHtml = $this->getBlockHtml($page, 'catalog.leftnav', 'catalogsearch.leftnav');
        $filterExpanderHtml = $this->getBlockHtml($page, 'mst-nav.filterExpander');
        $breadcrumbHtml     = $this->getBlockHtml($page, 'breadcrumbs');
        $pageTitleHtml      = $this->getBlockHtml($page, 'page.main.title');

        $categoryViewData = '';
        $children         = $layout->getChildNames('category.view.container');
        foreach ($children as $child) {
            $categoryViewData .= $layout->renderElement($child);
        }
        $categoryViewData     = '<div class="category-view">' . $categoryViewData . '</div>';
        $horizontalNavigation = ($this->horizontalFiltersConfig
            ->isUseCatalogLeftnavHorisontalNavigation($this->storeId)
        ) ? $layout->getBlock('catalog.leftnav')->toHtml()
            . $layout->getBlock('m.catalog.navigation.horizontal.state')->toHtml()
            //: $layout->getBlock('m.catalog.horizontal')->toHtml();
               : '';

        $data = [
            'products'         => $productsHtml,
            'leftnav'          => $leftNavHtml . $filterExpanderHtml,
            'breadcrumbs'      => $breadcrumbHtml,
            'pageTitle'        => $pageTitleHtml,
            'categoryViewData' => $categoryViewData,
            'url'              => $this->config->isSeoFiltersEnabled() ? 'mNavigationAjax->getAjaxResult' : $this->getCurrentUrl(),
            'horizontalBar'    => '<div class="mst-nav__horizontal-bar">' . $horizontalNavigation . '</div>',
        ];

        try {
            $sidebarTag   = $layout->getElementProperty('div.sidebar.additional', Element::CONTAINER_OPT_HTML_TAG);
            $sidebarClass = $layout->getElementProperty('div.sidebar.additional', Element::CONTAINER_OPT_HTML_CLASS);

            if (method_exists($layout, 'renderNonCachedElement')) {
                $sidebarAdditional = $layout->renderNonCachedElement('div.sidebar.additional');
            } else {
                $sidebarAdditional = '';
            }

            $data['sidebarAdditional']         = $sidebarAdditional;
            $data['sidebarAdditionalSelector'] = $sidebarTag . '.' . str_replace(' ', '.', $sidebarClass);
        } catch (\Exception $e) {
        }

        if ($this->moduleManager->isEnabled('Lof_AjaxScroll')) {
            $data['products'] .= $layout->createBlock('Lof\AjaxScroll\Block\Init')
                ->setTemplate('Lof_AjaxScroll::init.phtml')->toHtml();
            $data['products'] .= $layout->createBlock('Lof\AjaxScroll\Block\Init')
                ->setTemplate('Lof_AjaxScroll::scripts.phtml')->toHtml();
            $data['products'] .= "<script>window.ias.nextUrl = window.ias.getNextUrl();</script>";
        }

        if ($this->moduleManager->isEnabled('Mirasvit_Scroll')) {
            $data['products'] .= $layout->createBlock('Mirasvit\Scroll\Block\Scroll')
                ->setTemplate('Mirasvit_Scroll::scroll.phtml')->toHtml();
        }

        $data = $this->prepareAjaxData($data);

        return $this->createResponse($data);
    }

    /**
     * @param Page   $page
     * @param string $blockName
     * @param string $fallbackBlockName
     *
     * @return string
     */
    private function getBlockHtml(Page $page, $blockName, $fallbackBlockName = '')
    {
        $layout = $page->getLayout();

        $block = $layout->getBlock($blockName);

        if (!$block && $fallbackBlockName) {
            $block = $layout->getBlock($fallbackBlockName);
        }

        return $block ? $block->toHtml() : '';
    }

    /**
     * @param string[] $data
     *
     * @return string
     */
    protected function prepareAjaxData($data)
    {
        $map = [
            '&amp;'         => '&',
            '?isAjax=1&'    => '?',
            '?isAjax=1'     => '',
            '&isAjax=1'     => '',
            '?isAjax=true&' => '?',
            '?isAjax=true'  => '',
            '&isAjax=true'  => '',
        ];

        foreach ($map as $search => $replace) {
            $data = str_replace($search, $replace, $data);
        }

        return $data;
    }

    /**
     * @param string $data
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    protected function createResponse($data)
    {
        $response = $this->resultRawFactory->create()
            ->setHeader('Content-type', 'text/plain')
            ->setContents(\Zend_Json::encode($data));

        return $response;
    }

    /**
     * @return string
     */
    public function getCurrentUrl()
    {
        $params['_current']     = true;
        $params['_use_rewrite'] = true;
        $params['_query']       = ['_' => null];

        $currentUrl = $this->urlBuilder->getUrl('*/*/*', $params);
        $currentUrl = $this->urlService->getPreparedUrl($currentUrl);

        return $currentUrl;
    }
}
