define([
    'jquery',
    'Mirasvit_LayeredNavigation/js/config'
], function ($, config) {
    'use strict';

    function leftnavUpdate(leftnav) {
        var navigation = '.sidebar.sidebar-main .block.filter, .block-content.filter-content';

        if (leftnav) {
            $(navigation).not(".mst-nav__horizontal-bar .block.filter").first().replaceWith(leftnav);
            $(navigation).not(".mst-nav__horizontal-bar .block.filter").first().trigger('contentUpdated');
        }
    }

    function productsUpdate(products) {
        if (products) {
            $(config.getAjaxProductListWrapperId()).replaceWith(products);

            // trigger events
            $(config.getAjaxProductListWrapperId()).trigger('contentUpdated');

            setTimeout(function () {
                // execute after swatches are loaded
                $(config.getAjaxProductListWrapperId()).trigger('amscroll_refresh');
            }, 500);

            if ($.fn.lazyload) {
                // lazyload images for new content (Smartwave_Porto theme)
                $(config.getAjaxProductListWrapperId() + ' .porto-lazyload').lazyload({
                    effect: 'fadeIn'
                });
            }
        }
    }

    function additionalSidebarUpdate(selector, sidebar) {
        if (selector && selector != '' && sidebar) {
            $(selector).replaceWith(sidebar);
        }
    }

    function pageTitleUpdate(pageTitle) {
        $('#page-title-heading').closest('.page-title-wrapper').replaceWith(pageTitle);
        $('#page-title-heading').trigger('contentUpdated');
    }

    function breadcrumbsUpdate(breadcrumbs) {
        $('.breadcrumbs').replaceWith(breadcrumbs);
        $('.breadcrumbs').trigger('contentUpdated');
    }

    function updateCategoryViewData(categoryViewData) {
        if (categoryViewData != '') {
            if ($(".category-view").length == 0) {
                $('<div class="category-view"></div>').insertAfter('.page.messages');
            }

            $(".category-view").replaceWith(categoryViewData);
        }
    }

    function horizontalNavigationUpdate(horizontalNav, isHorizontalByDefault) {
        const horizontalNavigation = '.mst-nav__horizontal-bar';

        if (horizontalNav) {
            if (isHorizontalByDefault == 1) {
                $("#layered-filter-block").remove();
            }

            $(horizontalNavigation).first().replaceWith(horizontalNav);
            $(horizontalNavigation).first().trigger('contentUpdated');
        }
    }

    function updateUrlPath(targetUrl) {
        targetUrl.replace('&amp;', '&');
        targetUrl.replace('%2C', ',');

        window.mNavigationAjaxscrollCompatibility = 'true';
        window.location = targetUrl;
    }

    return function (data, isHorizontalByDefault) {
        if (data['ajaxscroll'] == 'true') {
            updateUrlPath(data.url);
        }

        leftnavUpdate(data['leftnav']);
        horizontalNavigationUpdate(data['horizontalBar'], isHorizontalByDefault);
        productsUpdate(data['products']);
        additionalSidebarUpdate(data['sidebarAdditionalSelector'], data['sidebarAdditional']);
        pageTitleUpdate(data['pageTitle']);
        breadcrumbsUpdate(data['breadcrumbs']);
        updateCategoryViewData(data['categoryViewData']);
    };
});
