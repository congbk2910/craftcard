define([
    'jquery',
    'Mirasvit_LayeredNavigation/js/config',
    'Mirasvit_LayeredNavigation/js/action/update-content',
    'Mirasvit_LayeredNavigation/js/ajax/pagination',
    'Mirasvit_LayeredNavigation/js/helper/overlay',
    'Mirasvit_LayeredNavigation/js/navigation/theme-compatibility'
], function ($, config, updateContent, initPaging, overlay, themeCompatibility) {

    /**
     * Widget responsible for initializing AJAX layered navigation, toolbar and paging.
     */
    $.widget('mst.layeredNavigation', {
        cache: [],

        options: {
            cleanUrl:                   '',
            overlayUrl:                 '',
            isSeoFilterEnabled:         false,
            isFilterClearBlockInOneRow: false,
            isHorizontalByDefault:      false
        },

        _create: function () {
            window.mNavigationConfigData = this.options;

            this._bind();

            themeCompatibility();
            initPaging();
        },

        _bind: function () {
            $(document).on(config.getAjaxCallEvent(), function (event, url) {
                var cachedData = this.getCacheData(url);

                if (cachedData) {
                    this.updatePage(url, cachedData);
                } else {
                    this.requestUpdate(url);
                }

                this.addBrowserHistory(url);
            }.bind(this));

            if (typeof window.history.replaceState === 'function') {
                /** Browser back button */
                window.onpopstate = function (e) {
                    if (e.state) {
                        window.location.href = e.state.url;
                    }
                }.bind(this);
            }
        },

        updatePage: function (url, result) {
            updateContent(result, window.mNavigationConfigData.isHorizontalByDefault);

            themeCompatibility();
            initPaging();
        },

        addBrowserHistory: function (url) {
            window.history.pushState({url: url}, '', url);

            return true;
        },

        requestUpdate: function (url) {
            overlay.show();

            $.ajax({
                url:      url,
                data:     {
                    isAjax: true
                },
                cache:    true,
                method:   'GET',
                success:  function (result) {
                    try {
                        result = $.parseJSON(result);
                        this.createCache(this.getCacheId(url), result);
                        this.updatePage(url, result);
                    } catch (e) {
                        if (window.mNavigationAjaxscrollCompatibility !== 'true') {
                            console.log(e);

                            window.location = url;
                        }
                    }
                }.bind(this),
                error:    function () {
                    window.location = url;
                }.bind(this),
                complete: function () {
                    overlay.hide();
                }.bind(this)
            });
        },

        getCacheId: function (data) {
            return JSON.stringify(data);
        },

        getCacheData: function (data) {
            var cacheId = this.getCacheId(data);

            return this.cache[cacheId];
        },

        createCache: function (cacheId, result) {
            this.cache[cacheId] = result;
        }
    });

    return $.mst.layeredNavigation;
});
