define([
    'jquery'
], function ($) {
    'use strict';

    return {
        isAjax: function () {
            return window.mstNavAjax;
        },

        getAjaxCallEvent: function () {
            return 'mst-nav__ajax-call';
        },

        getAjaxProductListWrapperId: function () {
            return '#m-navigation-product-list-wrapper';
        },
    };
});
