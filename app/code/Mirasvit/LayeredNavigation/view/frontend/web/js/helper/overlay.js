define([
    'jquery'
], function ($) {
    'use strict';

    function scrollToHeader() {
        var headerHeight = $('.page-header:first').height();

        $('html, body').animate({
            scrollTop: headerHeight
        }, 500, 'easeOutExpo');
    }

    /**
     * Component implements overlay logic for layered navigation.
     */
    return {
        className: 'navigation-overlay',

        show: function () {
            //scrollToHeader();

            $('.columns').append($('<div><i class="fa fa-spinner fa-spin"></i></div>').addClass(this.className));
        },

        hide: function () {
            $('.' + this.className).remove();
        }
    };
});
