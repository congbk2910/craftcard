define([
    'jquery',
    'Mirasvit_LayeredNavigation/js/config',
    'Mirasvit_LayeredNavigation/js/action/apply-filter'
], function ($, config, applyFilter) {
    'use strict';

    /**
     * Work with default filters.
     */
    $.widget('mst.navLabelRenderer', {
        _create: function () {
            $('[data-element = filter]', this.element).each(function (idx, item) {
                const $item = $(item);

                $item.on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    this.toggleCheckbox($item);

                    const url = $item.prop('tagName') === 'A'
                        ? $item.prop('href')
                        : $('a', $item).prop('href');

                    applyFilter(url);
                }.bind(this))
            }.bind(this));
        },

        toggleCheckbox: function ($el) {
            const $checkbox = $('input[type=checkbox]', $el);

            $checkbox.prop('checked', !$checkbox.prop('checked'));
        }
    });

    return $.mst.navLabelRenderer;
});
