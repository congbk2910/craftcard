<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-seo-filter
 * @version   1.0.22
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\SeoFilter\Service;

use Mirasvit\SeoFilter\Model\Config;
use Mirasvit\SeoFilter\Model\Context;
use function GuzzleHttp\Psr7\build_query;

class FriendlyUrlService
{
    const QUERY_FILTERS = ['cat'];

    /**
     * @var RewriteService
     */
    private $rewriteService;

    /**
     * @var UrlService
     */
    private $urlService;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Config
     */
    private $config;

    /**
     * FriendlyUrlService constructor.
     *
     * @param RewriteService $rewrite
     * @param UrlService     $urlService
     * @param Config         $config
     * @param Context        $context
     */
    public function __construct(
        RewriteService $rewrite,
        UrlService $urlService,
        Config $config,
        Context $context
    ) {
        $this->rewriteService = $rewrite;
        $this->urlService     = $urlService;
        $this->context        = $context;
        $this->config         = $config;
    }

    /**
     * Create friendly urls for Layered Navigation (add and remove filters)
     *
     * @param string $attributeCode
     * @param string $filterValue ex: 100  50-60,60-   234,203
     * @param bool   $remove
     *
     * @return string|false
     */
    public function getUrl($attributeCode, $filterValue, $remove = false)
    {
        if (is_array($filterValue)) {
            $values = $filterValue;
        } else {
            $values = explode(Config::SEPARATOR_FILTER_VALUES, $filterValue);
        }

        $requiredFilters[$attributeCode] = [];

        foreach ($values as $value) {
            $requiredFilters[$attributeCode][$value] = $value;
        }

        // merge with previous filters
        foreach ($this->rewriteService->getActiveFilters() as $attr => $filters) {
            if (!$this->config->isMultiselectEnabled() && $attr == $attributeCode) {
                continue;
            }

            foreach ($filters as $filter) {
                $requiredFilters[$attr][$filter] = $filter;
            }
        }

        // remove filter
        if ($remove && isset($requiredFilters[$attributeCode])) {
            foreach ($values as $value) {
                unset($requiredFilters[$attributeCode][$value]);
            }
        }

        // merge all filters on one line f1-f2-f3-f4
        $filterLines = [];
        $queryParams = [];
        foreach ($requiredFilters as $attrCode => $filters) {
            $filterLine = [];
            $queryParam = [];

            foreach ($filters as $filter) {
                if (in_array($attrCode, self::QUERY_FILTERS)) {
                    $queryParam[] = $filter;
                } else {
                    $filterLine[] = $this->rewriteService->getRewrite($attrCode, $filter);
                }
            }

            if (in_array($attrCode, self::QUERY_FILTERS) && count($filters) == 0) {
                $queryParam[] = '';
            }

            if (count($queryParam)) {
                $queryParams[$attrCode] = implode(',', $queryParam);
            }

            if (count($filterLine)) {
                $filterLines[] = implode(Config::SEPARATOR_FILTERS, $filterLine);
            }
        }

        $filterLines = implode(Config::SEPARATOR_FILTERS, $filterLines);
        //sort filters
        $values = explode(Config::SEPARATOR_FILTERS, $filterLines);
        asort($values);
        $filterString = implode(Config::SEPARATOR_FILTERS, $values);

        return $this->getPreparedCurrentCategoryUrl($filterString, $queryParams);
    }

    /**
     * @param string $filterUrlString
     * @param array  $queryParams
     *
     * @return string
     */
    public function getPreparedCurrentCategoryUrl($filterUrlString, $queryParams)
    {
        $suffix = $this->urlService->getCategoryUrlSuffix();
        $url    = $this->context->getCurrentCategory()->getUrl();
        $url    = preg_replace('/\?.*/', '', $url);
        $url    = ($suffix && $suffix !== '/') ? str_replace($suffix, '', $url) : $url;
        if (!empty($filterUrlString)) {
            if ($separator = $this->config->getCustomSeparator()) {
                $url .= (substr($url, -1, 1) === '/' ? '' : '/') . $separator;
            }

            $url .= (substr($url, -1, 1) === '/' ? '' : '/') . $filterUrlString;
        }
        $url = $url . $suffix;

        $query = '';
        if (count($queryParams)) {
            $query = '?' . build_query($queryParams);
        }

        return $url . $query;
    }
}
