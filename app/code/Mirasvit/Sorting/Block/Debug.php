<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Block;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;

class Debug extends Template
{
    /**
     * @var string
     */
    protected $_template       = 'Mirasvit_Sorting::debug.phtml';

    /**
     * @var bool
     */
    private static $isStylesApplied = false;

    /**
     * Debug constructor.
     * @param Context $context
     * @param array $values
     * @param array $weights
     * @param \Magento\Catalog\Model\Product  $product
     */
    public function __construct(
        Context $context,
        \Magento\Catalog\Model\Product  $product,
        array $values,
        array $weights
    ) {
        parent::__construct($context, [
            'values'         => $values,
            'weights'         => $weights,
            'product'    => $product,
        ]);
    }

    /**
     * @return bool
     */
    public function isApplyStyles()
    {
        return !self::$isStylesApplied;
    }

    /**
     * @param string $type
     * @return array
     */
    public function getValues($type)
    {
        return $this->getData('values')[$type];
    }

    /**
     * @param string $type
     * @return array
     */
    public function getWeights($type)
    {
        return $this->getData('weights')[$type];
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->getData('product');
    }

    /**
     * @param string $type
     * @return int
     */
    public function getMaxScore($type)
    {
        $max = 0;
        foreach ($this->getValues($type) as $name => $value) {
            $max = max($max, $this->getWeights($type)[$name] * $value);
        }
        return $max;
    }

    /**
     * @param string $type
     * @return Phrase|int
     */
    public function getScore($type)
    {
        $weights = $this->getWeights($type);
        $score = 0;
        foreach ($this->getValues($type) as $name => $value) {
            if ($value == "-") {
                return $this->getProduct()->getData($name);
            }
            $score += $value*$weights[$name];
        }
        return $score;
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        $html = parent::_toHtml();

        self::$isStylesApplied = true;

        return $html;
    }
}
