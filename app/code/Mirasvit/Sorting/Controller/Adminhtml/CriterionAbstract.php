<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Registry;
use Mirasvit\Sorting\Api\Data\CriterionInterface;
use Mirasvit\Sorting\Api\Repository\CriterionRepositoryInterface;

abstract class CriterionAbstract extends Action
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var CriterionRepositoryInterface
     */
    protected $criterionRepository;

    /**
     * @var \Magento\Backend\Model\Session
     */
    private $session;

    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * CriterionAbstract constructor.
     * @param CriterionRepositoryInterface $criterionRepository
     * @param ForwardFactory $resultForwardFactory
     * @param Context $context
     */
    public function __construct(
        CriterionRepositoryInterface $criterionRepository,
        ForwardFactory $resultForwardFactory,
        Context $context
    ) {
        $this->criterionRepository  = $criterionRepository;
        $this->resultForwardFactory = $resultForwardFactory;

        $this->context = $context;
        $this->session = $context->getSession();

        parent::__construct($context);
    }

    /**
     * Initialize page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Mirasvit_Sorting::sorting');

        $resultPage->getConfig()->getTitle()->prepend(__('Improved Sorting'));
        $resultPage->getConfig()->getTitle()->prepend(__('Sorting Criteria'));

        return $resultPage;
    }

    /**
     * @return CriterionInterface|false
     */
    protected function initModel()
    {
        $model = $this->criterionRepository->create();

        if ($this->getRequest()->getParam(CriterionInterface::ID)) {
            $model = $this->criterionRepository->get($this->getRequest()->getParam(CriterionInterface::ID));
        }

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->context->getAuthorization()->isAllowed('Mirasvit_Sorting::sorting');
    }
}
