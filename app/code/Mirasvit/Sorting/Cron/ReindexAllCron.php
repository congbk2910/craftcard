<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Cron;

use Mirasvit\Sorting\Model\Indexer;

class ReindexAllCron
{
    /**
     * @var Indexer
     */
    private $indexer;

    /**
     * ReindexAllCron constructor.
     * @param Indexer $indexer
     */
    public function __construct(
        Indexer $indexer
    ) {
        $this->indexer = $indexer;
    }

    public function execute()
    {
        $this->indexer->executeFull();
    }
}
