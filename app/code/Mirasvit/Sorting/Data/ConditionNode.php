<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Data;

use Magento\Framework\DataObject;

class ConditionNode extends DataObject
{
    const SORT_BY        = 'sortBy';
    const ATTRIBUTE      = 'attribute';
    const RANKING_FACTOR = 'rankingFactor';
    const DIRECTION      = 'direction';
    const WEIGHT         = 'weight';
    const LIMIT          = 'limit';

    /**
     * @param array $data
     * @return $this
     */
    public function loadArray(array $data)
    {
        $this->addData($data);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSortBy()
    {
        return $this->getData(self::SORT_BY);
    }

    /**
     * @param string $value
     * @return ConditionNode
     */
    public function setSortBy($value)
    {
        return $this->setData(self::SORT_BY, $value);
    }

    /**
     * @return string
     */
    public function getAttribute()
    {
        return $this->getData(self::ATTRIBUTE);
    }

    /**
     * @param string $value
     * @return ConditionNode
     */
    public function setAttribute($value)
    {
        return $this->setData(self::ATTRIBUTE, $value);
    }

    /**
     * @return mixed
     */
    public function getRankingFactor()
    {
        return $this->getData(self::RANKING_FACTOR);
    }

    /**
     * @param string $value
     * @return ConditionNode
     */
    public function setRankingFactor($value)
    {
        return $this->setData(self::RANKING_FACTOR, $value);
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->getData(self::DIRECTION);
    }

    /**
     * @param string $value
     * @return ConditionNode
     */
    public function setDirection($value)
    {
        return $this->setData(self::DIRECTION, $value);
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        $val = (int)$this->getData(self::WEIGHT);

        return $val !== 0 ? $val : 1;
    }

    /**
     * @param string $value
     * @return ConditionNode
     */
    public function setWeight($value)
    {
        return $this->setData(self::WEIGHT, $value);
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->getData(self::LIMIT);
    }

    /**
     * @param string $value
     * @return ConditionNode
     */
    public function setLimit($value)
    {
        return $this->setData(self::LIMIT, $value);
    }
}
