<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class AttributeSetFactor implements FactorInterface
{
    use MappingTrait;

    const MAPPING = 'mapping';

    /**
     * @var Indexer
     */
    private $indexer;

    public function __construct(
        Indexer $indexer
    ) {
        $this->indexer = $indexer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Attribute Set';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Rank products based on attribute set.';
    }

    /**
     * @return false|string
     */
    public function getUiComponent()
    {
        return 'sorting_factor_attributeSet';
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     * @return mixed|void
     * @throws \Zend_Db_Statement_Exception
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $mapping = $rankingFactor->getConfigData(self::MAPPING, []);

        $this->indexer->delete($rankingFactor);

        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        $select = $connection->select();
        $select->from(
            ['e' => $resource->getTableName('catalog_product_entity')],
            ['entity_id', 'attribute_set_id']
        );

        $stmt = $connection->query($select);

        $this->indexer->startIndexation();

        while ($row = $stmt->fetch()) {
            $value = $this->getValue($mapping, $row['attribute_set_id']);

            $this->indexer->add($rankingFactor, $row['entity_id'], $value);
        }

        $this->indexer->finishIndexation();
    }
}
