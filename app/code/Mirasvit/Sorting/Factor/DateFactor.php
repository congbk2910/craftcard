<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class DateFactor implements FactorInterface
{
    const DATE_FIELD = 'date_field';
    const ZERO_POINT = 'zero_point';

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Indexer
     */
    private $indexer;

    public function __construct(
        Context $context,
        Indexer $indexer
    ) {
        $this->context = $context;
        $this->indexer = $indexer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Date';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Rank products based on Created At/Updated At date.';
    }

    /**
     * @return false|string
     */
    public function getUiComponent()
    {
        return 'sorting_factor_date';
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     * @return mixed|void
     * @throws \Zend_Db_Statement_Exception
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $dateField = $rankingFactor->getConfigData(self::DATE_FIELD, 'created_at');
        $zeroPoint = $rankingFactor->getConfigData(self::ZERO_POINT, 60);

        $this->indexer->delete($rankingFactor);

        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        $select = $connection->select();
        $select->from(
            ['e' => $resource->getTableName('catalog_product_entity')],
            ['entity_id', $dateField]
        );

        $stmt = $connection->query($select);

        $this->indexer->startIndexation();

        while ($row = $stmt->fetch()) {
            $createdAt = $row[$dateField];
            $days      = $this->getDaysDiff($createdAt);

            $value = 0;
            if ($zeroPoint > $days) {
                $value = ($zeroPoint - $days) / $zeroPoint * IndexInterface::MAX;
            }

            $this->indexer->add($rankingFactor, $row['entity_id'], $value);
        }

        $this->indexer->finishIndexation();
    }

    /**
     * @param mixed $date
     * @return float
     */
    private function getDaysDiff($date)
    {
        return ceil((time() - strtotime($date)) / 60 / 60 / 24);
    }
}
