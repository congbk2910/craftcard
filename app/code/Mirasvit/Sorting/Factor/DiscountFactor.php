<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class DiscountFactor implements FactorInterface
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var Indexer
     */
    private $indexer;

    public function __construct(
        Context $context,
        Indexer $indexer
    ) {
        $this->context = $context;
        $this->indexer = $indexer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Discount';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return "Calculation: The difference between regular price and special prices.";
    }

    /**
     * @return bool|false|string
     */
    public function getUiComponent()
    {
        return false;
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     *
     * @return mixed|void
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $this->indexer->delete($rankingFactor);

        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        /**
         * Regular product with regular price=32 and special price=24 has:
         * price=32, final_price=24, min_price=24, max_price=24
         * Configurable product with max regular price=50 and min special price=1 has:
         * price=0, final_price=0, min_price=1, max_price=50
         * Grouped product has:
         * price=NULL, final_price=NULL
         */
        $select = $connection->select();
        $select->from(['index' => $resource->getTableName('catalog_product_index_price')], [
            'entity_id',
            'value' => new \Zend_Db_Expr('(GREATEST(max_price, price) - GREATEST(final_price, min_price) ) / GREATEST(max_price, price) * 100'),
        ])->joinLeft(
            ['e' => $resource->getTableName('catalog_product_entity')],
            'e.entity_id = index.entity_id',
            []
        )
            ->where('e.type_id = ?', 'simple')
            ->where('index.price > 0')
            ->group('index.entity_id');

        $rows = $connection->fetchPairs($select);

        if (!count($rows)) {
            return;
        }

        $max = max(array_values($rows));

        if ($max == 0) {
            return;
        }

        $this->indexer->startIndexation();

        foreach ($rows as $productId => $value) {
            $value = $value / $max * IndexInterface::MAX;

            $this->indexer->add($rankingFactor, $productId, $value);
        }

        $this->indexer->finishIndexation();

        // calculate discount factor based on child discounts
        // catalog_product_super_link - contains relations between configurable, bundled, grouped to simple
        $select = $connection->select();
        $select->from(['link' => $resource->getTableName('catalog_product_super_link')], [
            'parent_id',
        ])->joinLeft(
            ['index' => $resource->getTableName(IndexInterface::TABLE_NAME)],
            'link.product_id = index.product_id',
            [new \Zend_Db_Expr('MAX(value)'),]
        )->where('index.factor_id = ?', $rankingFactor->getId())
            ->group('link.parent_id');

        $rows = $connection->fetchPairs($select);

        $this->indexer->startIndexation();

        foreach ($rows as $productId => $value) {
            $this->indexer->add($rankingFactor, $productId, $value);
        }

        $this->indexer->finishIndexation();
    }
}
