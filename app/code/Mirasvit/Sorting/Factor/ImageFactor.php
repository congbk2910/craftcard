<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Mirasvit\Core\Service\CompatibilityService;
use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class ImageFactor implements FactorInterface
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var Indexer
     */
    private $indexer;

    public function __construct(
        Context $context,
        Indexer $indexer
    ) {
        $this->context = $context;
        $this->indexer = $indexer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Image';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Rank products based on image availability.';
    }

    /**
     * @return bool|false|string
     */
    public function getUiComponent()
    {
        return false;
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $this->indexer->delete($rankingFactor);

        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        $select = $connection->select();
        $select->from(
            ['e' => $resource->getTableName('catalog_product_entity')],
            ['entity_id']
        )->group('entity_id');

        $attribute = $this->context->eavConfig->getAttribute(4, 'image');

        $conditions = [
            'eav.attribute_id = ' . $attribute->getId(),
            CompatibilityService::isEnterprise()
                ? 'eav.row_id = e.row_id'
                : 'eav.entity_id = e.entity_id',
        ];

        $select->joinLeft(
            ['eav' => $attribute->getBackend()->getTable()],
            implode(' AND ', $conditions),
            ['value']
        );

        $stmt = $connection->query($select);

        $this->indexer->startIndexation();

        while ($row = $stmt->fetch()) {
            $image = $row['value'];
            $value = IndexInterface::MAX;

            if (!$image || $image == 'no_selection') {
                $value = IndexInterface::MIN;
            }

            $this->indexer->add($rankingFactor, $row['entity_id'], $value);
        }

        $this->indexer->finishIndexation();
    }
}
