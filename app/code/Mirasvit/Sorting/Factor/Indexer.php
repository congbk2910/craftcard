<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Magento\Framework\App\ResourceConnection;
use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class Indexer
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * @var array
     */
    private $rowPool = [];

    /**
     * Indexer constructor.
     *
     * @param ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->resource   = $resource;
        $this->connection = $resource->getConnection();
    }

    /**
     * @return ResourceConnection
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     *
     * @return bool
     */
    public function delete(RankingFactorInterface $rankingFactor)
    {
        $this->connection->delete(
            $this->resource->getTableName(IndexInterface::TABLE_NAME),
            [IndexInterface::FACTOR_ID . ' = ' . $rankingFactor->getId()]
        );

        return true;
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     * @param int                    $productId
     * @param string                 $value
     * @param int                    $storeId
     * @param int                    $websiteId
     *
     * @return bool
     */
    public function insertRow(RankingFactorInterface $rankingFactor, $productId, $value, $storeId = 0, $websiteId = 0)
    {
        $this->connection->insert(
            $this->resource->getTableName(IndexInterface::TABLE_NAME),
            [
                IndexInterface::FACTOR_ID  => $rankingFactor->getId(),
                IndexInterface::PRODUCT_ID => $productId,
                IndexInterface::VALUE      => $value,
                IndexInterface::STORE_ID   => $storeId,
                IndexInterface::WEBSITE_ID => $websiteId,
            ]
        );

        return true;
    }

    public function startIndexation()
    {
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     * @param int                    $productId
     * @param string|int             $value
     * @param int                    $storeId
     * @param int                    $websiteId
     */
    public function add(RankingFactorInterface $rankingFactor, $productId, $value, $storeId = 0, $websiteId = 0)
    {
        $this->rowPool[] = [
            IndexInterface::FACTOR_ID  => $rankingFactor->getId(),
            IndexInterface::PRODUCT_ID => $productId,
            IndexInterface::VALUE      => $value,
            IndexInterface::STORE_ID   => $storeId,
            IndexInterface::WEBSITE_ID => $websiteId,
        ];

        if (count($this->rowPool) > 1000) {
            $this->push();
        }
    }

    public function finishIndexation()
    {
        $this->push();
    }

    private function push()
    {
        if (!$this->rowPool) {
            return;
        }

        $this->connection->insertMultiple(
            $this->resource->getTableName(IndexInterface::TABLE_NAME),
            $this->rowPool
        );

        $this->rowPool = [];
    }
}
