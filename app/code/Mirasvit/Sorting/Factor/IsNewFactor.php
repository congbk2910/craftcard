<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */




namespace Mirasvit\Sorting\Factor;

use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Mirasvit\Core\Service\CompatibilityService;
use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class IsNewFactor implements FactorInterface
{
    const ATTRIBUTE_NEW_FROM = "news_from_date";
    const ATTRIBUTE_NEW_TO   = "news_to_date";

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Indexer
     */
    private $indexer;

    /**
     * @var int
     */
    private $attributeNewFromId;

    /**
     * @var int
     */
    private $attributeNewToId;

    /**
     * @var Attribute
     */
    private $eavAttribute;

    /**
     * @var bool
     */
    private $isEE = false;

    public function __construct(
        Context $context,
        Indexer $indexer,
        Attribute $eavAttribute
    ) {
        $this->context      = $context;
        $this->indexer      = $indexer;
        $this->eavAttribute = $eavAttribute;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'New Product';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Rank products based on product new from - to values.';
    }

    /**
     * @return bool|false|string
     */
    public function getUiComponent()
    {
        return false;
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     *
     * @return void
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $this->indexer->delete($rankingFactor);

        $this->indexer->startIndexation();

        if (!$this->getAttributeIdByCode(self::ATTRIBUTE_NEW_FROM)
            || !$this->getAttributeIdByCode(self::ATTRIBUTE_NEW_FROM)) {
            $this->indexer->finishIndexation();

            return;
        }

        $this->attributeNewFromId = $this->getAttributeIdByCode(self::ATTRIBUTE_NEW_FROM);
        $this->attributeNewToId   = $this->getAttributeIdByCode(self::ATTRIBUTE_NEW_TO);

        $this->isEE = CompatibilityService::isEnterprise();

        $validProducts = $this->getValidFrom();

        foreach ($this->getValidTo() as $item) {
            if (!in_array($item, $validProducts)) {
                $validProducts[] = $item;
            }
        }

        foreach ($validProducts as $product) {
            $this->indexer->add($rankingFactor, $product['entity_id'], IndexInterface::MAX, $product['store_id']);
        }

        $this->indexer->finishIndexation();
    }

    /**
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    private function getValidFrom()
    {
        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        $selectValidFrom = $connection->select();


        if ($this->isEE) {
            $selectValidFrom->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['store_id']
            )->joinInner(
                ['e' => $resource->getTableName('catalog_product_entity')],
                'e.row_id = attr_datetime.row_id',
                ['entity_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewFromId
                . ' AND CURRENT_TIMESTAMP >= attr_datetime.value'
            );
        } else {
            $selectValidFrom->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['entity_id', 'store_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewFromId
                . ' AND CURRENT_TIMESTAMP >= attr_datetime.value'
            );
        }

        $validFrom = $connection->query($selectValidFrom)->fetchAll();

        if (!count($validFrom)) {
            return [];
        }

        $idsFrom = [];

        $idsFrom = array_map(function ($row) {
            return $row['entity_id'];
        }, $validFrom);

        //need this to remove ids with invalid value news_to_date
        if ($this->isEE) {
            $correctionSelect = $connection->select()->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['store_id']
            )->joinInner(
                ['e' => $resource->getTableName('catalog_product_entity')],
                'e.row_id = attr_datetime.row_id',
                ['entity_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewToId
                . ' AND CURRENT_TIMESTAMP >= attr_datetime.value'
                . ' AND e.entity_id IN (' . implode(',', $idsFrom) . ')'
            );
        } else {
            $correctionSelect = $connection->select()->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['entity_id', 'store_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewToId
                . ' AND CURRENT_TIMESTAMP >= attr_datetime.value'
                . ' AND attr_datetime.entity_id IN (' . implode(',', $idsFrom) . ')'
            );
        }

        $correction = $connection->query($correctionSelect)->fetchAll();

        $validFrom = array_filter($validFrom, function ($row) use ($correction) {
            return !in_array($row, $correction);
        });

        return $validFrom;
    }

    /**
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    private function getValidTo()
    {
        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        $selectValidTo = $connection->select();

        if ($this->isEE) {
            $selectValidTo->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['store_id']
            )->joinInner(
                ['e' => $resource->getTableName('catalog_product_entity')],
                'e.row_id = attr_datetime.row_id',
                ['entity_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewToId
                . ' AND CURRENT_TIMESTAMP <= attr_datetime.value'
            );
        } else {
            $selectValidTo->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['entity_id', 'store_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewToId
                . ' AND CURRENT_TIMESTAMP <= attr_datetime.value'
            );
        }

        $validTo = $connection->query($selectValidTo)->fetchAll();

        if (!count($validTo)) {
            return [];
        }

        $idsTo = [];

        $idsTo = array_map(function ($row) {
            return $row['entity_id'];
        }, $validTo);

        //need this to remove ids with invalid value news_from_date
        if ($this->isEE) {
            $correctionSelect = $connection->select()->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['store_id']
            )->joinInner(
                ['e' => $resource->getTableName('catalog_product_entity')],
                'e.row_id = attr_datetime.row_id',
                ['entity_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewFromId
                . ' AND CURRENT_TIMESTAMP <= attr_datetime.value'
                . ' AND e.entity_id IN (' . implode(',', $idsTo) . ')'
            );
        } else {
            $correctionSelect = $connection->select()->from(
                ['attr_datetime' => $resource->getTableName('catalog_product_entity_datetime')],
                ['entity_id', 'store_id']
            )->where(
                'attr_datetime.attribute_id = ' . $this->attributeNewFromId
                . ' AND CURRENT_TIMESTAMP <= attr_datetime.value'
                . ' AND attr_datetime.entity_id IN (' . implode(',', $idsTo) . ')'
            );
        }

        $correction = $connection->query($correctionSelect)->fetchAll();

        $validTo = array_filter($validTo, function ($row) use ($correction) {
            return !in_array($row, $correction);
        });

        return $validTo;
    }

    /**
     * @param string $code
     *
     * @return int|false
     */
    private function getAttributeIdByCode($code)
    {
        return $this->eavAttribute->getIdByCode('catalog_product', $code) ? : false;
    }
}
