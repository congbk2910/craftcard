<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class RatingFactor implements FactorInterface
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var Indexer
     */
    private $indexer;

    public function __construct(
        Context $context,
        Indexer $indexer
    ) {
        $this->context = $context;
        $this->indexer = $indexer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Product Rating';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Rank products based on overall rating.';
    }

    /**
     * @return bool|false|string
     */
    public function getUiComponent()
    {
        return false;
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     *
     * @return mixed|void
     * @throws \Zend_Db_Statement_Exception
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $this->indexer->delete($rankingFactor);

        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        $select = $connection->select();
        $select->from(
            ['e' => $resource->getTableName('catalog_product_entity')],
            ['entity_id']
        )->joinInner(
            ['vote' => $resource->getTableName('rating_option_vote')],
            'vote.entity_pk_value = e.entity_id',
            ['value' => new \Zend_Db_Expr('AVG(vote.percent) - 1 + COUNT(vote.vote_id) / 1000')]
        )->joinInner(
            ['review' => $resource->getTableName('review')],
            'review.review_id = vote.review_id',
            []
        )->where('review.status_id=1')
            ->group('e.entity_id');

        $stmt = $connection->query($select);

        while ($row = $stmt->fetch()) {
            $value = $row['value'];
            $this->indexer->insertRow($rankingFactor, $row['entity_id'], $value);
        }
    }
}
