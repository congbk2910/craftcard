<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Magento\Catalog\Model\ProductFactory as ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Model\ResourceModel\Iterator;
use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class RuleFactor implements FactorInterface
{
    const RULE = 'rule';

    /**
     * @var array
     */
    private $productIds = [];

    /**
     * @var ProductRule\RuleFactory
     */
    private $ruleFactory;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var Iterator
     */
    private $iterator;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Indexer
     */
    private $indexer;

    public function __construct(
        ProductRule\RuleFactory $ruleFactory,
        ProductCollectionFactory $productCollectionFactory,
        ProductFactory $productFactory,
        Iterator $iterator,
        Context $context,
        Indexer $indexer
    ) {
        $this->ruleFactory              = $ruleFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->iterator                 = $iterator;
        $this->productFactory           = $productFactory;
        $this->context                  = $context;
        $this->indexer                  = $indexer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Rule';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Rank products based on various different conditions.';
    }

    /**
     * @return false|string
     */
    public function getUiComponent()
    {
        return 'sorting_factor_rule';
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     *
     * @return ProductRule\Rule
     */
    public function getRule(RankingFactorInterface $rankingFactor)
    {
        $ruleData = $rankingFactor->getConfigData(self::RULE, []);

        $model = $this->ruleFactory->create();

        $model->loadPost($ruleData);

        return $model;
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     * @return mixed|void
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $this->indexer->delete($rankingFactor);

        $rule = $this->getRule($rankingFactor);


        $productCollection = $this->productCollectionFactory->create();

        $rule->getConditions()->collectValidatedAttributes($productCollection);

        $this->iterator->walk(
            $productCollection->getSelect(),
            [[$this, 'callbackValidateProduct']],
            [
                'product' => $this->productFactory->create(),
                'rule'    => $rule,
            ]
        );

        $this->indexer->startIndexation();

        foreach ($this->productIds as $id => $value) {
            $this->indexer->add(
                $rankingFactor,
                $id,
                $value ? IndexInterface::MAX : 0
            );
        }

        $this->indexer->finishIndexation();
    }

    /**
     * Callback function for product matching
     *
     * @param array $args
     *
     * @return void
     */
    public function callbackValidateProduct($args)
    {
        $product = clone $args['product'];
        $product->setData($args['row']);

        $rule = $args['rule'];

        $this->productIds[$product->getId()] = $rule->getConditions()->validate($product);
    }
}
