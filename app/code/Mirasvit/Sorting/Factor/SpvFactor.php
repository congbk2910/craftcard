<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Factor;

use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class SpvFactor implements FactorInterface
{
    const ZERO_POINT = 'zero_point';

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Indexer
     */
    private $indexer;

    /**
     * SpvFactor constructor.
     * @param Context $context
     * @param Indexer $indexer
     */
    public function __construct(
        Context $context,
        Indexer $indexer
    ) {
        $this->context = $context;
        $this->indexer = $indexer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'SPV';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'SPV = Sales Pre View = Number of Sales / Number of Product Page Views';
    }

    /**
     * @return false|string
     */
    public function getUiComponent()
    {
        return 'sorting_factor_spv';
    }

    /**
     * @param RankingFactorInterface $rankingFactor
     * @return mixed|void
     */
    public function reindexAll(RankingFactorInterface $rankingFactor)
    {
        $this->indexer->delete($rankingFactor);

        $zeroPoint = $rankingFactor->getConfigData(self::ZERO_POINT, 365);

        $date = date('Y-m-d', strtotime('-' . $zeroPoint . ' day', time()));

        $resource   = $this->indexer->getResource();
        $connection = $this->indexer->getConnection();

        $select = $connection->select()->from($resource->getTableName('report_viewed_product_index'), [
            'product_id',
            'value' => new \Zend_Db_Expr('COUNT(index_id)'),
        ])
            ->where('added_at >= ?', $date)
            ->group('product_id');
        $views  = $connection->fetchPairs($select);

        $select = $connection->select()->from($resource->getTableName('sales_order_item'), [
            'product_id',
            'value' => new \Zend_Db_Expr('SUM(qty_ordered)'),
        ])
            ->where('created_at >= ?', $date)
            ->group('product_id');
        $sales  = $connection->fetchPairs($select);

        $this->indexer->startIndexation();

        foreach ($sales as $productId => $nSales) {
            $nViews = isset($views[$productId]) ? $views[$productId] : 0;

            if ($nViews > $nSales) {
                $value = $nSales / $nViews * IndexInterface::MAX;
            } else {
                $value = 0;
            }

            $this->indexer->add($rankingFactor, $productId, $value);
        }

        $this->indexer->finishIndexation();
    }
}
