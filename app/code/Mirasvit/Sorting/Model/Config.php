<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\State;

class Config
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var RequestInterface
     */
    private $request;

    private $state;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        RequestInterface $request,
        State $state
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->request     = $request;
        $this->state       = $state;
    }

    /**
     * @return bool
     */
    public function isDebug()
    {
        return $this->isExtraDebug()
            || $this->request->getParam('debug') === 'sorting';
    }

    /**
     * @return bool
     */
    public function isExtraDebug()
    {
        return $this->request->getParam('debug') === 'sorting_';
    }

    /**
     * @return bool
     */
    public function isElasticSearch()
    {
        $engine = $this->scopeConfig->getValue('catalog/search/engine');

        return $engine === 'elasticsearch6' || $engine === 'elasticsearch' || $engine === 'elasticsearch5' || $engine === 'elastic';
    }

    /**
     * @return bool
     */
    public function isApplicable()
    {
        if ($this->state->getAreaCode() !== 'frontend') {
            return false;
        }

        if (php_sapi_name() == "cli") {
            return false;
        }

        return true;
    }

    /**
     * true - default
     * false - don't apply sorting for custom blocks
     *
     * @return bool
     */
    public function isApplySortingForCustomBlocks()
    {
        return $this->scopeConfig->getValue("mst_sorting/general/apply_if_empty");
    }
}
