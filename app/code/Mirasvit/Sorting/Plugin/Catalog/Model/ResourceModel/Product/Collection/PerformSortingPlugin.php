<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Plugin\Catalog\Model\ResourceModel\Product\Collection;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Mirasvit\Sorting\Model\Config;
use Mirasvit\Sorting\Service\CriteriaApplierService;

/**
 * @see \Magento\Catalog\Model\ResourceModel\Product\Collection
 */
class PerformSortingPlugin
{
    /**
     * @var CriteriaApplierService
     */
    private $criteriaApplierService;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * PerformSortingPlugin constructor.
     *
     * @param CriteriaApplierService $criteriaApplierService
     * @param Config                 $config
     */
    public function __construct(
        CriteriaApplierService $criteriaApplierService,
        Config $config
    ) {
        $this->criteriaApplierService = $criteriaApplierService;
        $this->config                 = $config;
    }

    /**
     * @param Collection $subject
     * @param bool       $print
     * @param bool       $log
     *
     * @return array
     */
    public function beforeLoad($subject, $print = false, $log = false)
    {
        if (!$this->config->isApplicable()) {
            return [$print, $log];
        }

        if (!$subject->isLoaded()) {
            try {
                $this->criteriaApplierService->processCollection($subject);
            } catch (\Exception $e) {
            }

            $this->collection = $subject;
        }

        return [$print, $log];
    }

    /**
     * @param mixed $subject
     * @param mixed $result
     *
     * @return mixed
     */
    public function afterApply($subject, $result)
    {
        if (!$this->config->isApplicable()) {
            return $result;
        }

        if ($this->collection && !$this->collection->isLoaded()) {
            try {
                $this->criteriaApplierService->processCollection($this->collection);
            } catch (\Exception $e) {
            }
        }

        return $result;
    }

    /**
     * Remove order by entity_id
     *
     * @param object $subject
     * @param string $field
     * @param string $direction
     *
     * @return array
     */
    public function beforeSetOrder($subject, $field, $direction = '')
    {
        if (!$this->config->isApplicable()) {
            return [$field, $direction];
        }

        if ($field === 'entity_id') {
            $field = '1';
        }

        return [$field, $direction];
    }
}
