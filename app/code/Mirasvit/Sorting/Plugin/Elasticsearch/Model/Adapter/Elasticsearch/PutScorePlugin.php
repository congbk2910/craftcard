<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Plugin\Elasticsearch\Model\Adapter\Elasticsearch;

use Mirasvit\Sorting\Api\Repository\CriterionRepositoryInterface;
use Mirasvit\Sorting\Api\Repository\RankingFactorRepositoryInterface;
use Mirasvit\Sorting\Service\ScoreCalculationService;

/**
 * @see \Magento\Elasticsearch\Model\Adapter\Elasticsearch
 */
class PutScorePlugin
{
    /**
     * @var CriterionRepositoryInterface
     */
    private $criterionRepository;

    /**
     * @var RankingFactorRepositoryInterface
     */
    private $rankingFactorRepository;

    /**
     * @var ScoreCalculationService
     */
    private $scoreCalculationService;

    /**
     * PutScorePlugin constructor.
     * @param CriterionRepositoryInterface $criterionRepository
     * @param RankingFactorRepositoryInterface $rankingFactorRepository
     * @param ScoreCalculationService $scoreCalculationService
     */
    public function __construct(
        CriterionRepositoryInterface $criterionRepository,
        RankingFactorRepositoryInterface $rankingFactorRepository,
        ScoreCalculationService $scoreCalculationService
    ) {
        $this->criterionRepository     = $criterionRepository;
        $this->rankingFactorRepository = $rankingFactorRepository;
        $this->scoreCalculationService = $scoreCalculationService;
    }

    /**
     * @param mixed $subject
     * @param array $docs
     * @return mixed
     */
    public function afterPrepareDocsPerStore($subject, $docs)
    {
        $productIds = array_keys($docs);

        foreach ($this->rankingFactorRepository->getCollection() as $factor) {
            $scores = $this->scoreCalculationService->getFactorScore($factor, $productIds);

            foreach ($productIds as $id) {
                $docs[$id]['sorting_factor_' . $factor->getId()] = $this->fetchScore($scores, $id);
            }
        }

        foreach ($this->criterionRepository->getCollection() as $criterion) {
            $scores = $this->scoreCalculationService->getCriterionScore($criterion, $productIds);

            foreach ($productIds as $id) {
                $docs[$id]['sorting_criteria_' . $criterion->getId()] = $this->fetchScore($scores, $id);
            }
        }

        return $docs;
    }

    /**
     * @param mixed $list
     * @param string $id
     * @return int
     */
    private function fetchScore($list, $id)
    {
        if (!isset($list[$id])) {
            return 1;
        }

        return $list[$id] >= 1 ? intval($list[$id]) : 1;
    }
}
