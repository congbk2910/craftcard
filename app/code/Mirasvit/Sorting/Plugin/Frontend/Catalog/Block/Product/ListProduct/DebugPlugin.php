<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Plugin\Frontend\Catalog\Block\Product\ListProduct;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\LayoutInterface;
use Mirasvit\Sorting\Block\Debug;
use Mirasvit\Sorting\Model\Config;
use Mirasvit\Sorting\Repository\RankingFactorRepository;
use Mirasvit\Sorting\Repository\CriterionRepository;
use Mirasvit\Sorting\Service\CriteriaApplierService;
use Mirasvit\Sorting\Service\CriteriaManagementService;
use Mirasvit\Sorting\Service\ScoreCalculationService;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

/**
 * @see \Magento\Catalog\Block\Product\ListProduct
 */
class DebugPlugin
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var RankingFactorRepository
     */
    private $rankingFactorRepository;

    /**
     * @var ScoreCalculationService
     */
    private $scoreCalculationService;
    /**
     * @var CriterionRepository
     */
    private $criterionRepository;

    /**
     * @var LayoutInterface
     */
    private $layout;
    /**
     * @var array
     */
    private $weights;
    /**
     * @var array
     */
    private $values;
    /**
     * @var CriteriaManagementService
     */
    private $criteriaManagement;
    /**
     * @var RequestInterface
     */
    private $request;

    private $renderedIds = [];
    /**
     * @var CriteriaApplierService
     */
    private $criteriaApplierService;

    /**
     * DebugPlugin constructor.
     * @param Config $config
     * @param CriterionRepository $criterionRepository
     * @param RankingFactorRepository $rankingFactorRepository
     * @param ScoreCalculationService $scoreCalculationService
     * @param LayoutInterface $layout
     * @param CriteriaManagementService $criteriaManagement
     * @param CriteriaApplierService $criteriaApplierService
     * @param RequestInterface $request
     */
    public function __construct(
        Config $config,
        CriterionRepository $criterionRepository,
        RankingFactorRepository $rankingFactorRepository,
        ScoreCalculationService $scoreCalculationService,
        LayoutInterface $layout,
        CriteriaManagementService $criteriaManagement,
        CriteriaApplierService $criteriaApplierService,
        RequestInterface $request
    ) {
        $this->config                  = $config;
        $this->rankingFactorRepository = $rankingFactorRepository;
        $this->criterionRepository = $criterionRepository;
        $this->scoreCalculationService = $scoreCalculationService;
        $this->layout                  = $layout;
        $this->criteriaManagement                  = $criteriaManagement;
        $this->criteriaApplierService                  = $criteriaApplierService;
        $this->request                  = $request;

        $this->values = [
            "global" => [],
            "criterion1" => [],
            "criterion2" => [],
        ];

        $this->weights = [
            "global" => [],
            "criterion1" => [],
            "criterion2" => [],
        ];
    }

    /**
     * @param mixed $subject
     * @param string $html
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function afterGetProductPrice($subject, $html, $product = null)
    {
        return $this->render($product) . $html;
    }


    /**
     * @param mixed $subject
     * @param string $html
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function afterGetProductDetailsHtml($subject, $html, $product = null)
    {
        return $this->render($product) . $html;
    }

    /**
     * @param mixed $subject
     * @param array $params
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function afterGetAddToCartPostParams($subject, $params, $product = null)
    {
        if ($this->config->isDebug()) {
            //uncomment if other are not working
            //echo $this->render($product);
        }
        return $params;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    private function render($product)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        if (!$product) {
            return "";
        }
        if (in_array($product->getId(), $this->renderedIds)) {
            return "";
        }
        $this->renderedIds[] = $product->getId();

        if (!$this->config->isDebug()) {
            return "";
        }
        $productId      = $product->getId();
        $this->setRankingFactorData($productId);
        $this->setCriterionData($productId);
        return $this->layout->createBlock(Debug::class, '', [
                'values'         => $this->values,
                'weights'         => $this->weights,
                'product'         => $product,
            ])->toHtml();
    }

    /**
     * @param int $productId
     */
    private function setRankingFactorData($productId)
    {
        $factors = $this->rankingFactorRepository->getCollection()
            ->addFieldToFilter(RankingFactorInterface::IS_ACTIVE, true)
            ->addFieldToFilter(RankingFactorInterface::IS_GLOBAL, true)
        ;
        foreach ($factors as $factor) {
            if (!$factor->getWeight()) {
                continue;
            }
            $scores = $this->scoreCalculationService->getFactorScore($factor, [$productId]);
            $score  = $scores[$productId];
            $this->values['global'][$factor->getName()] = $score;
            $this->weights['global'][$factor->getName()] = $factor->getWeight();
        }
    }

    /**
     * @param int $productId
     */
    private function setCriterionData($productId)
    {
        $criterion = $this->criteriaApplierService->getCurrentCriterion();
        if (!$criterion) {
            return;
        }
        $i = 1;
        foreach ($criterion->getConditionCluster()->getFrames() as $frame) {
            foreach ($frame->getNodes() as $node) {
                if ($node->getSortBy() == "attribute") {
                    $this->values['criterion' . $i][$node->getAttribute()] = "-";
                } else {
                    $factors = $this->rankingFactorRepository->getCollection()
                        ->addFieldToFilter(RankingFactorInterface::IS_ACTIVE, true)
                        ->addFieldToFilter(RankingFactorInterface::IS_GLOBAL, false)
                    ;
                    foreach ($factors as $factor) {
                        if ($node->getRankingFactor() == $factor->getId()) {
                            $scores = $this->scoreCalculationService->getFactorScore($factor, [$productId]);
                            $score = $scores[$productId];
                            $this->values['criterion' . $i][$factor->getName()] = $score;
                            $this->weights['criterion' . $i][$factor->getName()] = $node->getWeight();
                        }
                    }
                }
            }
            $i++;
        }
    }
}
