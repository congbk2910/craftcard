<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Plugin\Frontend\Catalog\Block\Product\ProductList\Toolbar;

use Magento\Catalog\Block\Product\ProductList\Toolbar;
use Magento\Framework\App\RequestInterface;
use Mirasvit\Sorting\Service\CriteriaManagementService;

class InitDefaultOrderPlugin
{
    /**
     * @var CriteriaManagementService
     */
    private $criteriaManagement;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * InitDefaultOrderPlugin constructor.
     * @param CriteriaManagementService $criteriaManagement
     * @param RequestInterface $request
     */
    public function __construct(
        CriteriaManagementService $criteriaManagement,
        RequestInterface $request
    ) {
        $this->criteriaManagement  = $criteriaManagement;
        $this->request             = $request;
    }

    /**
     * Initialize default sort order and direction.
     *
     * @param Toolbar                            $subject
     * @param \Magento\Framework\Data\Collection $collection
     */
    public function beforeSetCollection(Toolbar $subject, $collection)
    {
        $criterion = $this->criteriaManagement->getCurrentCriterion($this->request->getParam('product_list_order'));
        if ($criterion) {
            $subject->setDefaultOrder($criterion->getCode());
            $subject->setDefaultDirection($this->criteriaManagement->getDefaultDirection($criterion));
        }
    }
}
