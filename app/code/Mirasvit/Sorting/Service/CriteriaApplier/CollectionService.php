<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Service\CriteriaApplier;

use Magento\Eav\Model\Entity\Collection\AbstractCollection;

class CollectionService
{
    /**
     * Removing attributes order fields duplicates (added by Magento)
     *
     * @param AbstractCollection $collection
     *
     * @throws \Zend_Db_Select_Exception
     */
    public function removeOrderDuplicates(AbstractCollection $collection)
    {
        $sortOrders = $collection->getSelect()->getPart('order');

        foreach ($sortOrders as $key => $order) {
            if (count(array_keys($sortOrders, $order)) > 1) {
                unset($sortOrders[$key]);
            }
        }

        $collection->getSelect()->setPart('order', $sortOrders);
    }
}
