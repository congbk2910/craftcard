<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Service\CriteriaApplier;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Select;

class DatabaseService
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * DatabaseService constructor.
     * @param ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * @param Select $insertSelect
     *
     * @return Table
     * @throws \Zend_Db_Exception
     */
    public function populateTemporaryTable($insertSelect)
    {
        $connection = $this->resource->getConnection();
        $tableName  = $this->resource->getTableName($this->alias('mst_sorting', 'tmp'));
        $table      = $connection->newTable($tableName);
        $connection->dropTemporaryTable($table->getName());

        $table->addColumn(
            'sorting_entity_id',
            Table::TYPE_INTEGER,
            10,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity ID'
        );
        $table->addColumn(
            'sorting_score',
            Table::TYPE_INTEGER,
            32,
            ['nullable' => false],
            'Score'
        );

        $table->setOption('type', 'memory');
        $connection->createTemporaryTable($table);
//        $connection->createTable($table);

        $this->resource->getConnection()
            ->query($this->resource->getConnection()->insertFromSelect($insertSelect, $table->getName()));

        return $table;
    }

    /**
     * @param string ...$prefix
     *
     * @return string
     */
    public function alias(...$prefix)
    {
        $prefix = implode('_', $prefix) . '_' . substr(sha1(uniqid('', true)), 0, 4);

        return $prefix;
    }
}
