<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Service;

use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Select;
use Magento\Store\Model\StoreManager;
use Mirasvit\Sorting\Api\Data\CriterionInterface;
use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;
use Mirasvit\Sorting\Api\Repository\CriterionRepositoryInterface;
use Mirasvit\Sorting\Api\Repository\RankingFactorRepositoryInterface;
use Mirasvit\Sorting\Data\ConditionFrame;
use Mirasvit\Sorting\Data\ConditionNode;
use Mirasvit\Sorting\Model\Config;

class CriteriaApplierService
{
    const FLAG_NO_SORT   = 'NO_SORT';
    const FLAG_GLOBAL    = 'sorting_global';
    const FLAG_CRITERION = 'sorting_criterion';
    const FLAG_DIRECTION = 'sorting_direction';

    /**
     * @var RankingFactorRepositoryInterface
     */
    private $rankingFactorRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var CriteriaApplier\DatabaseService
     */
    private $databaseService;

    /**
     * @var CriteriaApplier\CollectionService
     */
    private $collectionService;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * @var CriterionRepositoryInterface
     */
    private $criterionRepository;

    /**
     * @var CriterionInterface
     */
    private $currentCriterion;

    /**
     * @var RequestInterface
     */
    private $request;

    public function __construct(
        RankingFactorRepositoryInterface $rankingFactorRepository,
        Config $config,
        CriteriaApplier\DatabaseService $databaseService,
        CriteriaApplier\CollectionService $collectionService,
        CriterionRepositoryInterface $criterionRepository,
        ResourceConnection $resource,
        RequestInterface $request,
        StoreManager $storeManager
    ) {
        $this->rankingFactorRepository = $rankingFactorRepository;
        $this->config                  = $config;
        $this->databaseService         = $databaseService;
        $this->collectionService       = $collectionService;
        $this->resource                = $resource;
        $this->criterionRepository     = $criterionRepository;
        $this->storeManager            = $storeManager;
        $this->request                 = $request;
    }

    /**
     * @param AbstractCollection $collection
     *
     * @return AbstractCollection
     */
    public function addGlobalRankingFactors(AbstractCollection $collection)
    {
        $rankingFactors = $this->rankingFactorRepository->getCollection();
        $rankingFactors->addFieldToFilter(RankingFactorInterface::IS_ACTIVE, 1)
            ->addFieldToFilter(RankingFactorInterface::IS_GLOBAL, 1);

        if ($rankingFactors->getSize()) {
            $collection->setFlag(self::FLAG_GLOBAL, true);
        }

        return $collection;
    }

    /**
     * @param CriterionInterface $criterion
     * @param AbstractCollection $collection
     * @param null               $dir
     *
     * @return AbstractCollection
     */
    public function addCriterion(CriterionInterface $criterion, AbstractCollection $collection, $dir = null)
    {
        $collection->setFlag(self::FLAG_CRITERION, $criterion);
        $collection->setFlag(self::FLAG_DIRECTION, $dir);
        $this->currentCriterion = $criterion;

        return $collection;
    }

    /**
     * @return CriterionInterface
     */
    public function getCurrentCriterion()
    {
        return $this->currentCriterion;
    }

    /**
     * @param AbstractCollection $collection
     *
     * @return AbstractCollection
     */
    public function processCollection(AbstractCollection $collection)
    {
        // to avoid conflict with the Advanced Product Feeds
        if ($collection->getFlag(self::FLAG_NO_SORT)) {
            return $collection;
        }

        /*if custom collection - apply all global and first criterion */
        if (!(bool)$collection->getFlag(self::FLAG_GLOBAL) && !(bool)$collection->getFlag(self::FLAG_CRITERION)) {
            if (!$this->config->isApplySortingForCustomBlocks()) {
                return $collection;
            }

            // don't apply sorting for flat bundle product collection
            if (get_class($collection->getEntity()) == "Magento\Catalog\Model\ResourceModel\Product\Flat") {
                return $collection;
            }

            $collection->setFlag(self::FLAG_GLOBAL, true);

            if (!in_array($this->request->getModuleName(), ['catalogsearch', 'searchautocomplete'])) {
                $criterion = $this->criterionRepository->getCollection()
                    ->addFieldToFilter(CriterionInterface::IS_ACTIVE, 1)
                    ->setOrder(CriterionInterface::IS_DEFAULT, 'desc')
                    ->setOrder(CriterionInterface::POSITION, 'asc')
                    ->getFirstItem();

                $collection->setFlag(self::FLAG_CRITERION, $criterion);
            }
        }

        $select = clone $collection->getSelect();
        $select->reset(\Zend_Db_Select::ORDER);

        if ($collection->getFlag(self::FLAG_GLOBAL) || $collection->getFlag(self::FLAG_CRITERION)) {
            if (!$this->config->isElasticSearch()) {
                $collection->getSelect()->reset(\Zend_Db_Select::ORDER);
            } else { //need this because with elasticsearch, in widgets e.created_at field goes first in ORDER BY clause
                $order = $collection->getSelect()->getPart(\Zend_Db_Select::ORDER);

                if (count($order) && is_array($order[0])
                    && strpos($order[0][0], 'created_at') !== false
                ) {
                    unset($order[0]);
                    $collection->getSelect()->setPart(\Zend_Db_Select::ORDER, $order);
                }
            }
        } else {
            return $collection;
        }

        if ($collection->getFlag(self::FLAG_GLOBAL)) {
            $rankingFactors = $this->rankingFactorRepository->getCollection();
            $rankingFactors->addFieldToFilter(RankingFactorInterface::IS_ACTIVE, 1)
                ->addFieldToFilter(RankingFactorInterface::IS_GLOBAL, 1);

            $frame = new ConditionFrame();

            foreach ($rankingFactors as $factor) {
                $node = new ConditionNode();
                $node->setRankingFactor($factor->getId())
                    ->setWeight($factor->getWeight());

                $frame->addNode($node);
            }

            $table = $this->summarize($select, $frame);

            $this->join($collection->getSelect(), $table, 'desc', 'global');
            $this->join($select, $table, 'desc', 'global');
        }

        if ($collection->getFlag(self::FLAG_CRITERION)) {
            /** @var CriterionInterface $criterion */
            $criterion = $collection->getFlag(self::FLAG_CRITERION);

            /** @var string $dir */
            $dir = $collection->getFlag(self::FLAG_DIRECTION);

            $cluster    = $criterion->getConditionCluster();
            $useAttrDir = true; // we can change direction only once for the top-level condition
            foreach ($cluster->getFrames() as $idx => $frame) {
                $rankingFrame = new ConditionFrame();

                foreach ($frame->getNodes() as $node) {
                    if (!$dir) {
                        $dir = $node->getDirection();
                    }

                    if ($node->getSortBy() == CriterionInterface::CONDITION_SORT_BY_ATTRIBUTE) {
                        $attribute = $node->getAttribute();
                        $dir       = $useAttrDir ? $dir : $node->getDirection();
                        $collection->setOrder($attribute, $dir);
                        

                        if ($attribute == 'name') {
                            $collection->getSelect()->order('name ' . $dir);
                        }

                        if ($useAttrDir) {
                            $useAttrDir = $useFactDir = false;
                        }
                    } else {
                        $rankingFrame->addNode($node);
                        $useAttrDir = false;
                    }
                }

                $table = $this->summarize($select, $rankingFrame);

                $this->join(
                    $collection->getSelect(),
                    $table,
                    $dir,
                    $idx == 0 ? 'criterion' : 'criterion' . $idx
                );
            }
        }

        $this->collectionService->removeOrderDuplicates($collection);

        if ($this->config->isExtraDebug()) {
            var_dump(get_class($collection) . '::' . $collection->getSelect()->assemble());
        }

        return $collection;
    }

    /**
     * @param Select         $select
     * @param ConditionFrame $frame
     *
     * @return Table|false
     */
    private function summarize(Select $select, ConditionFrame $frame)
    {
        if (!count($frame->getNodes())) {
            return false;
        }

        $storeId   = $this->storeManager->getStore()->getId();
        $websiteId = $this->storeManager->getWebsite()->getId();

        $select = clone $select;
        $select->reset(\Zend_Db_Select::COLUMNS)
            ->columns(['entity_id']);

        $subSelects = [];
        foreach ($frame->getNodes() as $node) {
            $subSelect = clone $select;

            $alias = $this->databaseService->alias('mst_sorting', $node->getRankingFactor());

            $subSelect->joinInner(
                [$alias => $this->resource->getTableName(IndexInterface::TABLE_NAME)],
                "$alias.product_id = e.entity_id AND $alias.website_id IN (0, $websiteId) AND $alias.store_id IN (0, $storeId)",
                ['score' => new \Zend_Db_Expr($alias . '.value * ' . $node->getWeight())]
            )
                ->where("$alias.factor_id = ?", $node->getRankingFactor())
                ->order('score desc')
                ->group('e.entity_id');

            if ($node->getLimit()) {
                $limit = $node->getLimit();

                if (strpos($limit, '.') !== false) {
                    $limit = explode('.', $limit);
                    $subSelect->limit($limit[0], $limit[1]);
                } else {
                    $subSelect->limit($limit);
                }
            }

            $table = $this->databaseService->populateTemporaryTable($subSelect);

            $subSelects[] = $this->resource->getConnection()->select()
                ->from($table->getName());
        }

        $unionSelect = $this->resource->getConnection()->select()
            ->from($this->resource->getConnection()->select()->union($subSelects), [])
            ->columns(['sorting_entity_id', 'sorting_score' => new \Zend_Db_Expr('SUM(sorting_score)')])
            ->group('sorting_entity_id')
            ->order('sorting_score desc');

        $insertSelect = $unionSelect;

        $table = $this->databaseService->populateTemporaryTable($insertSelect);

        return $table;
    }

    /**
     * @param Select      $select
     * @param Table|false $table
     * @param string      $dir
     * @param string      $suffix
     *
     * @return Select
     */
    private function join(Select $select, $table, $dir, $suffix)
    {
        if (!$table) {
            return $select;
        }

        $isJoined = false;

        foreach ($select->getPart(\Zend_Db_Select::FROM) as $item) {
            if ($item['tableName'] === $table->getName()) {
                $isJoined = true;
            }
        }

        if ($isJoined) {
            return $select;
        }

        $alias = $this->databaseService->alias('mst_sorting', $suffix);

        $select->joinLeft(
            [$alias => $table->getName()],
            $alias . '.sorting_entity_id = e.entity_id',
            ["sorting_score_$suffix" => 'sorting_score']
        )->order("$alias.sorting_score $dir");

        return $select;
    }
}
