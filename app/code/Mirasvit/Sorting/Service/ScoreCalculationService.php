<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Service;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManager;
use Mirasvit\Sorting\Api\Data\CriterionInterface;
use Mirasvit\Sorting\Api\Data\IndexInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;

class ScoreCalculationService
{
    /**
     * @var CriteriaApplierService
     */
    private $criteriaApplierService;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * ScoreCalculationService constructor.
     *
     * @param CriteriaApplierService   $criteriaApplierService
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ResourceConnection       $connection
     */
    public function __construct(
        CriteriaApplierService $criteriaApplierService,
        ProductCollectionFactory $productCollectionFactory,
        ResourceConnection $connection,
        StoreManager $storeManager
    ) {
        $this->criteriaApplierService   = $criteriaApplierService;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->resource                 = $connection;
        $this->storeManager             = $storeManager;
    }

    /**
     * @param CriterionInterface $criterion
     * @param array              $productIds
     *
     * @return array
     */
    public function getCriterionScore(CriterionInterface $criterion, array $productIds)
    {
        if (!count($productIds)) {
            return [];
        }

        $collection = $this->productCollectionFactory->create();
        $collection->addFieldToFilter('entity_id', $productIds);

        $collection->setFlag(CriteriaApplierService::FLAG_GLOBAL, true);
        $collection->setFlag(CriteriaApplierService::FLAG_CRITERION, $criterion);

        $collection = $this->criteriaApplierService->processCollection($collection);

        $rows = $this->resource->getConnection()
            ->fetchAll($collection->getSelect());

        $result = [];

        foreach ($productIds as $productId) {
            $result[$productId] = 0;
        }

        foreach ($rows as $row) {
            $id     = $row['entity_id'];
            $global = isset($row['sorting_score_global']) ? $row['sorting_score_global'] : 0;
            $local  = isset($row['sorting_score_criterion']) ? $row['sorting_score_criterion'] : 0;

            $result[$id] = $global + $local;
        }

        return $result;
    }

    /**
     * @param RankingFactorInterface $factor
     * @param array                  $productIds
     *
     * @return array
     */
    public function getFactorScore(RankingFactorInterface $factor, array $productIds)
    {
        if (!count($productIds)) {
            return [];
        }

        $websiteId = $this->storeManager->getWebsite()->getId();
        $storeId   = $this->storeManager->getStore()->getId();

        $select = $this->resource->getConnection()->select();
        $select->from($this->resource->getTableName(IndexInterface::TABLE_NAME))
            ->where(IndexInterface::FACTOR_ID . '= ?', $factor->getId())
            ->where(IndexInterface::WEBSITE_ID . ' IN (0, ?)', $websiteId)
            ->where(IndexInterface::STORE_ID . ' IN (0, ?)', $storeId)
            ->where(IndexInterface::PRODUCT_ID . ' IN (?)', $productIds);

        $rows = $this->resource->getConnection()
            ->fetchAll($select);

        $result = [];

        foreach ($productIds as $productId) {
            $result[$productId] = 0;
        }

        foreach ($rows as $row) {
            $id    = $row[IndexInterface::PRODUCT_ID];
            $score = $row[IndexInterface::VALUE];

            $result[$id] = $score;
        }

        return $result;
    }
}
