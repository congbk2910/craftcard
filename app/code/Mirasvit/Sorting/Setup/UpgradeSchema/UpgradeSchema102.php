<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */




namespace Mirasvit\Sorting\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Mirasvit\Sorting\Api\Data\IndexInterface;

class UpgradeSchema102 implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $connection = $setup->getConnection();
        $table      = $setup->getTable(IndexInterface::TABLE_NAME);

        $connection->addColumn(
            $table,
            IndexInterface::ID,
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => false,
                'unsigned' => true,
                'identity' => true,
                'primary'  => true,
                'comment'  => IndexInterface::ID,
            ]
        );

        $connection->addColumn(
            $table,
            IndexInterface::WEBSITE_ID,
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => true,
                'unsigned' => true,
                'default'  => null,
                'comment'  => IndexInterface::WEBSITE_ID,
            ]
        );

        $connection->addColumn(
            $table,
            IndexInterface::STORE_ID,
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => true,
                'unsigned' => true,
                'default'  => null,
                'comment'  => IndexInterface::STORE_ID,
            ]
        );
    }
}
