<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-sorting
 * @version   1.0.42
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Sorting\Ui\Preview;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Mirasvit\Sorting\Api\Data\CriterionInterface;
use Mirasvit\Sorting\Api\Data\RankingFactorInterface;
use Mirasvit\Sorting\Api\Repository\CriterionRepositoryInterface;
use Mirasvit\Sorting\Api\Repository\RankingFactorRepositoryInterface;
use Mirasvit\Sorting\Data\ConditionCluster;
use Mirasvit\Sorting\Data\ConditionFrame;
use Mirasvit\Sorting\Data\ConditionNode;
use Mirasvit\Sorting\Service\CriteriaApplierService;
use Mirasvit\Sorting\Service\ScoreCalculationService;

class DataProvider extends ProductDataProvider
{
    /**
     * @var CriterionRepositoryInterface
     */
    private $repository;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var CriteriaApplierService
     */
    private $criteriaApplierService;

    /**
     * @var ContextInterface
     */
    private $context;


    /**
     * @var RankingFactorRepositoryInterface
     */
    private $rankingFactorRepository;

    /**
     * @var CriterionRepositoryInterface
     */
    private $criterionRepository;

    /**
     * @var ScoreCalculationService
     */
    private $scoreCalculationService;

    /**
     * DataProvider constructor.
     * @param CriterionRepositoryInterface $repository
     * @param ProductCollectionFactory $productCollectionFactory
     * @param CriteriaApplierService $criteriaApplierService
     * @param RankingFactorRepositoryInterface $rankingFactorRepository
     * @param CriterionRepositoryInterface $criterionRepository
     * @param ContextInterface $context
     * @param ScoreCalculationService $scoreCalculationService
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        CriterionRepositoryInterface $repository,
        ProductCollectionFactory $productCollectionFactory,
        CriteriaApplierService $criteriaApplierService,
        RankingFactorRepositoryInterface $rankingFactorRepository,
        CriterionRepositoryInterface $criterionRepository,
        ContextInterface $context,
        ScoreCalculationService $scoreCalculationService,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->repository               = $repository;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->criteriaApplierService   = $criteriaApplierService;
        $this->context                  = $context;
        $this->rankingFactorRepository  = $rankingFactorRepository;
        $this->criterionRepository      = $criterionRepository;
        $this->scoreCalculationService  = $scoreCalculationService;

        parent::__construct($name, $primaryFieldName, $requestFieldName, $productCollectionFactory, [], [], $meta, $data);
    }

    /**
     * @return array|bool|mixed
     */
    public function getData()
    {
        $data = false;
        if ($this->context->getRequestParam('criterion')) {
            $data = $this->applyCriterion();
        }

        if ($this->context->getRequestParam('rankingFactor')) {
            $data = $this->applyRankingFactor();
        }

        return $data ? $data : parent::getData();
    }

    /**
     * @return array|mixed
     */
    private function applyCriterion()
    {
        $arCriterion = $this->context->getRequestParam('criterion');

        if (!isset($arCriterion[CriterionInterface::CONDITIONS])) {
            return parent::getData();
        }

        $conditions = $arCriterion[CriterionInterface::CONDITIONS];

        if (!is_array($conditions)) {
            return parent::getData();
        }

        $conditionCluster = new ConditionCluster();
        $conditionCluster->loadArray($conditions);

        $ids = [];

        $rankingFactors = $this->rankingFactorRepository->getCollection();
        $rankingFactors->addFieldToFilter(RankingFactorInterface::IS_ACTIVE, 1)
            ->addFieldToFilter(RankingFactorInterface::IS_GLOBAL, 1);
        foreach ($rankingFactors as $factor) {
            $ids[] = $factor->getId();
        }

        foreach ($conditionCluster->getFrames() as $frame) {
            foreach ($frame->getNodes() as $node) {
                if ($node->getSortBy() == CriterionInterface::CONDITION_SORT_BY_RANKING_FACTOR) {
                    $ids[] = $node->getRankingFactor();
                }
            }
        }

        $criterion = $this->criterionRepository->create();
        $criterion->setConditionCluster($conditionCluster);

        $this->criteriaApplierService->addGlobalRankingFactors($this->getCollection());
        $this->criteriaApplierService->addCriterion($criterion, $this->getCollection());

        # prevent "random" sorting, if scores are same
        $this->getCollection()->setOrder('entity_id');

        $data = parent::getData();

        $data = $this->addRankingFactorsToData($data, $ids);

        return $data;
    }

    /**
     * @return array|mixed
     */
    private function applyRankingFactor()
    {
        $arRankingFactor = $this->context->getRequestParam('rankingFactor');

        if (!isset($arRankingFactor[RankingFactorInterface::ID])) {
            return parent::getData();
        }

        $id = $arRankingFactor[RankingFactorInterface::ID];

        if (!$id) {
            return parent::getData();
        }

        $cluster = new ConditionCluster();

        $frame = new ConditionFrame();
        $cluster->addFrame($frame);

        $node = new ConditionNode();
        $frame->addNode($node);

        $node->setSortBy(CriterionInterface::CONDITION_SORT_BY_RANKING_FACTOR)
            ->setRankingFactor($id)
            ->setDirection('desc')
            ->setWeight($arRankingFactor['weight']);

        $criterion = $this->criterionRepository->create();
        $criterion->setConditionCluster($cluster);

        $this->criteriaApplierService->addCriterion($criterion, $this->getCollection());

        # prevent "random" sorting, if scores are same
        $this->getCollection()->setOrder('entity_id');

        $data = parent::getData();

        $data = $this->addRankingFactorsToData($data, [$id]);

        return $data;
    }

    /**
     * @param array $data
     * @param array $rankingFactorIds
     * @return mixed
     */
    private function addRankingFactorsToData($data, array $rankingFactorIds)
    {
        $items = [];
        foreach ($data['items'] as $item) {
            $items[$item['entity_id']] = $item;
        }

        foreach ($rankingFactorIds as $factorId) {
            $factor = $this->rankingFactorRepository->get($factorId);

            #set zero score
            foreach (array_keys($items) as $productId) {
                $items[$productId]['factors'][$factorId] = [
                    'label' => $factor->getName(),
                    'value' => 0,
                ];
            }

            $scores = $this->scoreCalculationService->getFactorScore($factor, array_keys($items));

            #set real score
            foreach ($scores as $productId => $score) {
                $items[$productId]['factors'][$factorId]['value'] = $score;
            }
        }

        foreach (array_keys($items) as $productId) {
            $items[$productId]['factors'] = array_values($items[$productId]['factors']);
        }

        $data['items'] = array_values($items);

        return $data;
    }

    /**
     * Native sorting is useless
     * {@inheritDoc}
     */
    public function addOrder($field, $direction)
    {
        return $this;
    }

    /**
     * @return \Magento\Eav\Model\Entity\Collection\AbstractCollection|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getCollection()
    {
        /** @var \Magento\Eav\Model\Entity\Collection\AbstractCollection $collection */
        $collection = parent::getCollection();
        $collection->addFieldToSelect('status');

        return $collection;
    }
}
