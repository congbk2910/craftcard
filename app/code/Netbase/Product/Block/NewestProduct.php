<?php

namespace Netbase\Product\Block;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Helper\Conditions;

class NewestProduct extends \Magento\Framework\View\Element\Template
{
    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var Registry
     */
    /**
     * @var Conditions
     */
    protected $conditionsHelper;

    protected $_coreRegistry;
    protected $_categoryFactory;
    public function __construct(
        Template\Context $context,
        CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        Conditions $conditionsHelper,
        Registry $registry,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_coreRegistry = $registry;
        $this->_categoryFactory = $categoryFactory;
        $this->conditionsHelper = $conditionsHelper;
    }
    public function filterNewestProduct($categories)
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addCategoriesFilter(['in' => $categories]);
        return $collection;
    }
    public function getCategoryIds($param)
    {
        $conditions = $this->conditionsHelper->decode($param);
        foreach ($conditions as $key => $condition) {
            if (!empty($condition['attribute']) && $condition['attribute'] == 'category_ids'){
                return $condition['value'];
            }
        }
        return '';
    }

    public function getProductCollection($catId): \Magento\Catalog\Model\ResourceModel\Product\Collection
    {
        $category = $this->_categoryFactory->create()->load($catId);
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addCategoryFilter($category);
        $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        return $collection;
    }

    public function checkProductExist($catId) {
        return $this->_categoryFactory->create()->load($catId)->getProductCount();
    }

    public function getHomeName()
    {
        $data = $this->_coreRegistry->registry('dataRequest');
        return $data['home'];
    }
}
