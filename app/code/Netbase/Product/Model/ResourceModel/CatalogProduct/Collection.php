<?php
namespace Netbase\Product\Model\ResourceModel\CatalogProduct;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Netbase\Product\Model\CatalogProduct', 'Netbase\Product\Model\ResourceModel\CatalogProduct');
    }

}