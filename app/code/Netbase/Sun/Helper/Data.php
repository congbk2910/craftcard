<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Netbase\Sun\Helper;

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

use Magento\Framework\Registry;
use Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $galleryReadHandler;
    /**
     * Catalog Image Helper
     *
     * @var \Magento\Catalog\Helper\Image
     */
    protected $imageHelper;
    protected $_objectManager;
    private   $_registry;
    protected $_filterProvider;
	protected $_filesystem ; 
    protected $_imageFactory;
    protected $_productRepository;
    protected $wishlistProvider;

    public function __construct(
		\Magento\Framework\Filesystem $filesystem,
        GalleryReadHandler $galleryReadHandler,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Image\AdapterFactory $imageFactory,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Design\Theme\ThemeProviderInterface $themeProvider,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        Registry $registry
    ){
        $this->wishlistProvider = $wishlistProvider;
        $this->imageHelper = $imageHelper;
        $this->galleryReadHandler = $galleryReadHandler;
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectManager;
        $this->_productRepository = $productRepository;
        $this->_filterProvider = $filterProvider;
        $this->_registry = $registry;
		$this->_filesystem = $filesystem;
		$this->_imageFactory = $imageFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_themeProvider = $themeProvider;
        $this->directory_list = $directory_list;
        
        parent::__construct($context);
    }

    public function getYoutubeEmbedUrl($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';
        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id ;
    }
    public function getGoogleFontPath()
    {
        $themeId = $this->_scopeConfig->getValue(
            \Magento\Framework\View\DesignInterface::XML_PATH_THEME_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getId()
        );

        /** @var $theme \Magento\Framework\View\Design\ThemeInterface */
        $theme = $this->_themeProvider->getThemeById($themeId);
        $themePath = $theme->getData('theme_path');
        $google_font_config = str_replace('/',DS,$this->directory_list->getPath('app')).DS.'design'.DS.'frontend'.DS.$themePath.DS.'web'.DS.'css'.DS.'source'.DS.'config'.DS.'google_font_used.json';

        return $google_font_config;
    }
    public function getMediaGalleryImages($product)
    {
        $_product = $this->_productRepository->get($product->getSku(), false, null, true);

        return $_product->getMediaGalleryImages();
    }

    public function addGallery($product) {
        $this->galleryReadHandler->execute($product);
    }
    public function getWishListItem() {
        $wishlistItems = 0;
        $currentUserWishlist = $this->wishlistProvider->getWishlist();
        if (count($currentUserWishlist->getItemCollection())) {
            $wishlistItems = $currentUserWishlist->getItemCollection()->count();
        }
//        if(is_array($wishlistItems)) {
//            return $wishlistItems->count();
//        }
        return $wishlistItems;
    }
    public function getGalleryImages(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        $images = $product->getMediaGalleryImages();
        if ($images instanceof \Magento\Framework\Data\Collection) {
            foreach ($images as $image) {
                /** @var $image \Magento\Catalog\Model\Product\Image */
                $image->setData(
                    'small_image_url',
                    $this->imageHelper->init($product, 'product_page_image_small')
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'medium_image_url',
                    $this->imageHelper->init($product, 'product_page_image_medium')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'large_image_url',
                    $this->imageHelper->init($product, 'product_page_image_large')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
            }
        }
        return $images;
    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
    public function getConfig($config_path, $storeId = null)
    {
		
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
    public function getModel($model) {
        return $this->_objectManager->create($model);
    }
    public function getCurrentStore() {
        return $this->_storeManager->getStore();
    }
    public function filterContent($content) {
        return $this->_filterProvider->getPageFilter()->filter($content);
    }
    public function getCategoryProductIds($current_category) {
        $category_products = $current_category->getProductCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('is_saleable', 1, 'left')
            ->addAttributeToSort('position','asc');
        $cat_prod_ids = $category_products->getAllIds();
        
        return $cat_prod_ids;
    }
    public function getPrevProduct($product) {
        $current_category = $product->getCategory();
        if(!$current_category) {
            foreach($product->getCategoryCollection() as $parent_cat) {
                $current_category = $parent_cat;
            }
        }
        if(!$current_category)
            return false;
        $cat_prod_ids = $this->getCategoryProductIds($current_category);
        $_pos = array_search($product->getId(), $cat_prod_ids);
        if (isset($cat_prod_ids[$_pos - 1])) {
            $prev_product = $this->getModel('Magento\Catalog\Model\Product')->load($cat_prod_ids[$_pos - 1]);
            return $prev_product;
        }
        return false;
    }
    public function getNextProduct($product) {
        $current_category = $product->getCategory();
        if(!$current_category) {
            foreach($product->getCategoryCollection() as $parent_cat) {
                $current_category = $parent_cat;
            }
        }
        if(!$current_category)
            return false;
        $cat_prod_ids = $this->getCategoryProductIds($current_category);
        $_pos = array_search($product->getId(), $cat_prod_ids);
        if (isset($cat_prod_ids[$_pos + 1])) {
            $next_product = $this->getModel('Magento\Catalog\Model\Product')->load($cat_prod_ids[$_pos + 1]);
            return $next_product;
        }
        return false;
    }
	public function getCategories(){
		/* get current category id */
		$categoryRegistry = $this->_objectManager->get('Magento\Framework\Registry')->registry('current_category');
		$arrayCategories = array();
		if($categoryRegistry) {
			$currentCateId = $categoryRegistry->getId();
			
			$category = $this->_objectManager->create('Magento\Catalog\Model\Category');
			$tree = $category->getTreeModel();
			$tree->load();
			$ids = $tree->getCollection()->getAllIds();
			$arr = array();
			if ($ids) {
				foreach ($ids as $id) {
					$cat = $this->_objectManager->create('Magento\Catalog\Model\Category');
					$cat->load($id);
		 
					if($cat->getParentId() == $currentCateId){
						$arrayCategories[$id] =
								array("parent_id" => $cat->getParentId(),
									"name" => $cat->getName(),
									"cat_id" => $cat->getId(),
									"cat_level" => $cat->getLevel(),
									"cat_url" => $cat->getUrl()
						);
					}
				}// for each ends
				
			}//if ids present
		}
		return $arrayCategories;
	}
	public function getcurrentcategory(){
		$categoryRegistry = $this->_objectManager->get('Magento\Framework\Registry')->registry('current_category');
		if($categoryRegistry) {
			$categoryname = $categoryRegistry->getName();
			return $categoryname;
		}
	}
	public function resizeImg($image, $width = null, $height = null)
    {
		$absolutePath = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('sun/header/').$image;

        $imageResized = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('resized/'.$width.'/').$image;         
        if(!empty($image)){
            $imageResize = $this->_imageFactory->create();         
            $imageResize->open($absolutePath);
            $imageResize->constrainOnly(TRUE);         
            $imageResize->keepTransparency(TRUE);         
            $imageResize->keepFrame(FALSE);         
            $imageResize->keepAspectRatio(FALSE);         
            $imageResize->resize($width,$height);  
            //destination folder                
            $destination = $imageResized ;    
            //save image      
            $imageResize->save($destination);
        }         

        $resizedURL = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'resized/'.$width.'/'.$image;
        return $resizedURL;
    }

    public function getBaseMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getConfigDesign($storeId = null)
    {
        $themeColor =  $this->getConfig('sun_design/general/theme_color', $storeId);
        return $themeColor;
    }
}
