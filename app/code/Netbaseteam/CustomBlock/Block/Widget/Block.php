<?php

namespace Netbaseteam\CustomBlock\Block\Widget;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Widget\Helper\Conditions;

class Block extends Template implements BlockInterface
{
    protected $_category;

    /**
     * @var Conditions
     */
    protected $conditionsHelper;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var string
     */
    protected $_template = "custom_block.phtml";

   public function __construct(
       Template\Context $context,
       \Magento\Catalog\Model\Category $category,
       Conditions $conditionsHelper,
       Registry $registry,
       array $data = []
   )
   {
       parent::__construct($context, $data);
       $this->conditionsHelper = $conditionsHelper;
       $this->_category = $category;
       $this->_registry = $registry;
   }
    public function getCategoryIds($param)
    {
        $conditions = $this->conditionsHelper->decode($param);
        foreach ($conditions as $key => $condition) {
            if (!empty($condition['attribute']) && $condition['attribute'] == 'category_ids')    {
                return $condition['value'];
            }
        }
        return '';
    }
    public function loadCategory($cid) {
        return $this->_category->load($cid)->getChildrenCategories();
    }
    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }
}