<?php

namespace Netbaseteam\CustomBlock\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class TextBlock extends Template implements BlockInterface
{
    protected $_template = "text_block.phtml";

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
}