<?php

/**
 * Product in category grid
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
namespace Netbaseteam\CustomCategory\Block\Adminhtml\Category\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\App\ObjectManager;

class Category extends \Netbaseteam\CustomCategory\Block\Adminhtml\Widget\Grid\Extended
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var CollectionFactory
     */
    protected $_categoryFactory;

    protected $_categorySelected;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Netbaseteam\CustomCategory\Model\CategorySelect $categorySelect,
        CategoryFactory $categoryFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_categorySelected = $categorySelect;
        $this->_categoryFactory = $categoryFactory;
        $this->_productFactory = $productFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('catalog_category_block');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    /**
     * @return array|null
     */
    public function getCategory()
    {
        return $this->_coreRegistry->registry('category');
    }

    /**
     * @param Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in category flag
        if ($column->getId() == 'cat_selected') {
            $catSelect = $this->_getSelectedCategories();
            if (empty($catSelect)) {
                $catSelect = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->_categorySelected->getCollection()->addFieldToFilter('cat_selected', ['in' => $catSelect]);
            } elseif (!empty($catSelect)) {
                $this->_categorySelected->getCollection()->addFieldToFilter('cat_selected', ['nin' => $catSelect]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {
        if ($this->getCategory()->getId()) {
            $this->setDefaultFilter(['in_category' => 1]);
        }
        $collection = $this->_categoryFactory->create()->getCollection();
        $collection->addAttributeToSelect('entity_id');
        $collection->addAttributeToSelect('name');
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        if ($storeId > 0) {
            $collection->addStoreFilter($storeId);
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_category_selected',
            [
                'type' => 'checkbox',
                'name' => 'in_category',
                'values' => $this->_getSelectedCategories(),
                'index' => 'entity_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'entity_id',
            [
                'header' => __('Cat ID'),
                'sortable' => true,
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name']);
        $this->addColumn(
            'category_selected',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'category_selected',
                'editable' => !$this->getCategory()->getProductsReadonly()
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('catalog/*/grid', ['_current' => true, 'custom_category' => '1']);
    }

    /**
     * @return array
     */
    protected function _getSelectedCategories()
    {

        $catId = $this->getCategory()->getId();
        $category = $this->_categorySelected->getCollection()->addFieldToFilter('cat_id', $catId);
        $arr = [];
        foreach ($category as $data) {
            $arr[] = $data->getCatSelected();
        }
        return $arr;
    }

}
