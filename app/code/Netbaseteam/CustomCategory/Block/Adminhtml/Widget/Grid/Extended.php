<?php

namespace Netbaseteam\CustomCategory\Block\Adminhtml\Widget\Grid;

class Extended extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var string
     */
    protected $_template = 'Netbaseteam_CustomCategory::widget/grid/extended.phtml';

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, array $data = [])
    {
        parent::__construct($context, $backendHelper, $data);
    }
}