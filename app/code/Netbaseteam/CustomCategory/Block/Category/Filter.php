<?php

namespace Netbaseteam\CustomCategory\Block\Category;

use Magento\Catalog\Model\Product\Attribute\Repository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Filter extends Template
{

    protected $_productAttributeRepository;

    public function __construct(
        Context $context,
        Repository $productAttributeRepository,
        array $data = []
    ){
        parent::__construct($context,$data);
        $this->_productAttributeRepository = $productAttributeRepository;
    }

    public function getMaterialOptions(){
        $manufacturerOptions = $this->_productAttributeRepository->get('farbe')->getOptions();
        $values = array();
        foreach ($manufacturerOptions as $manufacturerOption) {
            $values[] = $manufacturerOption->getLabel();
        }
        return $values;
    }
}