<?php

namespace Netbaseteam\CustomCategory\Block\Category;

use Magento\Catalog\Model\Category;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;

class Sidebar extends Template
{

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var Category
     */
    protected $_category;
    protected $_categoryCollectionFactory;
    protected $_storeManagerInterface;

 public function __construct(
     Template\Context $context,
     \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
     \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
     Category $category,
     Registry $registry,
     array $data = []
 )
 {
     parent::__construct($context, $data);
     $this->_registry = $registry;
     $this->_storeManagerInterface = $storeManagerInterface;
     $this->_category = $category;
     $this->_categoryCollectionFactory = $categoryCollectionFactory;
 }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getRootCategoryId()
    {
        return $this->_storeManagerInterface->getStore()->getRootCategoryId();
    }

    public function getCategoryCollection($cid) {
        return $this->_category->load($cid);
    }

}