<?php

namespace Netbaseteam\CustomCategory\Block;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class CategoryItem extends Template
{

    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $categoryHelper;

    protected $_productRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var Category
     */
    protected $_category;

    public function __construct(
        Category $category,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        CategoryRepository $categoryRepository,
        Template\Context $context,
        Registry $registry,
        array $data = []
    )
    {
        $this->_productRepository = $productRepository;
        $this->categoryHelper = $categoryHelper;
        $this->categoryRepository = $categoryRepository;
        $this->_registry = $registry;
        $this->_category = $category;
        parent::__construct($context, $data);

    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category')->getId();
    }
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function getSubcategories($cid) {
      return $this->_category->load($cid)->getChildrenCategories();
    }

    public function getModeCategory($cid) {
        return $this->_category->load($cid);
    }

    public function loadCategory($cid) {
        return $this->_category->load($cid);
    }
    public function getParentCatId() {
        return $this->_registry->registry('current_category')->getParentCategory()->getId();
    }

    public function getCategoriesCrumb()
    {
        $evercrumbs = array();
        $product = $this->_registry->registry('current_product');
        $categoryCollection = clone $product->getCategoryCollection();
        $categoryCollection->clear();
        $categoryCollection->addAttributeToSort('level', $categoryCollection::SORT_ORDER_DESC)->addAttributeToFilter('path', array('like' => "1/" . $this->_storeManager->getStore()->getRootCategoryId() . "/%"));
        $categoryCollection->setPageSize(1);
        $breadcrumbCategories = $categoryCollection->getFirstItem()->getParentCategories();
        foreach ($breadcrumbCategories as $category) {
            $evercrumbs[] = array(
                'label' => $category->getName(),
                'title' => $category->getName(),
                'link' => $category->getUrl(),
                'id' => $category->getId()
            );
        }
        $evercrumbs[] = array(
            'label' => $product->getName(),
            'title' => $product->getName(),
            'link' => ''
        );

        return $evercrumbs;
    }

    public function getCategoryItem()
    {
        $currentProduct = $this->getCurrentProduct();
        $categories = $currentProduct->getCategoryIds();
        $collection = '';
        foreach ($categories as $key => $value) {
            $levelCat = $this->_category->load($value)->getParentCategory()->getLevel();
            if($levelCat > 1) {
                $collection = $this->_category->load($value)->getParentCategory()->getEntityId();
            }
        }
        return $collection;
    }

    public function categoryRecursive($id)
    {
        $categories = $this->_category->load($id);
        if ($categories->hasChildren()) {
            echo "<ul class='main-sub-item-list'>";
            $subcategories = explode(',', $categories->getChildren());
            foreach ($subcategories as $data) {
                $subcategory = $this->_category->load($data);
                $subcategoyId = $subcategory->getEntityId();
                $subClass = $subcategory->hasChildren() ? 'has-sub-class' : '';
                $categoryObj = $this->categoryRepository->get($subcategoyId);
                $subcategoryUrl = $this->categoryHelper->getCategoryUrl($categoryObj);
                echo "<li class='item-level-sub $subClass'>";
                echo "<a href='$subcategoryUrl'>";
                echo $subcategory->getName();
                echo "</a>";
                if ($subcategory->hasChildren()) {
                    echo  "<div class='sub-level'>";
                          $this->categoryRecursive($data);
                    echo "</div>";
                }
                echo "</li>";
            }
            echo "</ul>";
        }
    }
    public function categoryRecursiveLevel($id)
    {
        $categories = $this->_category->load($id);
        if ($categories->hasChildren()) {
            echo "<ul class='main-sub-item-list'>";
            $subcategories = explode(',', $categories->getChildren());
            foreach ($subcategories as $data) {
                $subcategory = $this->_category->load($data);
                $subcategoyId = $subcategory->getId();
                $categoryObj = $this->categoryRepository->get($subcategoyId);
                $subcategoryUrl = $this->categoryHelper->getCategoryUrl($categoryObj);
                if($this->getParentCatId() == $subcategoyId) {
                    echo "<li class='item-level-sub selected'>";
                } else if($this->getCurrentCategory() == $subcategoyId) {
                    echo "<li class='item-level-sub selected'>";
                }
                else {
                    echo "<li class='item-level-sub'>";
                }
                echo "<a href='$subcategoryUrl'>";
                echo $subcategory->getName();
                echo "</a>";
                if ($subcategory->hasChildren()) {
                    echo  "<div class='sub-level'>";
                    $this->categoryRecursiveLevel($data);
                    echo "</div>";
                }
                echo "</li>";
            }
            echo "</ul>";
        }
        else {
            echo "<ul class='main-sub-item-list'>";
            $subcategoryUrl = $categories->getUrl();
            echo "<li class='item-level-sub'>";
            echo "<a href='$subcategoryUrl'>";
            echo $categories->getName();
            echo "</a>";
            echo "</li>";
            echo "</ul>";
        }
    }
    public function categoryRecursiveDetailsPage($id)
    {
        $categories = $this->_category->load($id);
        if ($categories->hasChildren()) {
            echo "<ul class='main-sub-item-list'>";
            $subcategories = explode(',', $categories->getChildren());
            foreach ($subcategories as $data) {
                $subcategory = $this->_category->load($data);
                $subcategoyId = $subcategory->getId();
                $categoryObj = $this->categoryRepository->get($subcategoyId);
                $subcategoryUrl = $this->categoryHelper->getCategoryUrl($categoryObj);
                echo "<li class='item-level-sub'>";
                echo "<a href='$subcategoryUrl'>";
                echo $subcategory->getName();
                echo "</a>";
                if ($subcategory->hasChildren()) {
                    echo  "<div class='sub-level'>";
                    $this->categoryRecursiveDetailsPage($data);
                    echo "</div>";
                }
                echo "</li>";
            }
            echo "</ul>";
        } else {
            echo "<ul class='main-sub-item-list'>";
            $subcategoryUrl = $categories->getUrl();
            echo "<li class='item-level-sub'>";
            echo "<a href='$subcategoryUrl'>";
            echo $categories->getName();
            echo "</a>";
            echo "</li>";
            echo "</ul>";
        }
    }
}
