<?php

namespace Netbaseteam\CustomCategory\Block\Product;

use Magento\Catalog\Model\Category;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class Details extends Template
{

    protected $_registry;
    protected $_category;

    public function __construct(
        Template\Context $context,
        Category $category,
        Registry $registry,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_registry = $registry;
        $this->_category = $category;
    }

    public function getCategoryList()
    {
        $currentProduct = $this->_registry->registry('current_product');
        $categories = $currentProduct->getCategoryIds();
        $collection = '';
        foreach ($categories as $key => $value) {
            $levelCat = $this->_category->load($value)->getParentCategory()->getLevel();
            if($levelCat > 1) {
                $collection = $this->_category->load($value)->getParentCategory()->getEntityId();
            }
        }
        return $this->_category->load($collection)->getChildrenCategories();
    }
}