<?php

namespace Netbaseteam\CustomCategory\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\Registry;
use Magento\Widget\Helper\Conditions;

class CategoryPage extends Template implements BlockInterface
{
    /**
     * @var string
     */
    protected $_template = "our_collection.phtml";

    /**
     * @var \Magento\CatalogWidget\Model\Rule
     */
    protected $rule;

    /**
     * @var Conditions
     */
    protected $conditionsHelper;

    /**
     * @var Registry
     */
    protected $_registry;
    protected $_productCollection;
    protected $_category;
    protected $_output;
    public function __construct(
        \Magento\Catalog\Model\Category $category,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
        Conditions $conditionsHelper,
        Registry $registry,
        \Magento\Catalog\Helper\Output $output,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_productCollection = $productCollection;
        $this->rule = $rule;
        $this->conditionsHelper = $conditionsHelper;
        $this->_category = $category;
        $this->_output =$output;
        $this->_registry = $registry;
    }

    public function filterSubCategories($catId) {
        return $this->_category->load($catId)->getChildrenCategories();
    }


    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getCategoryIds($param)
    {
        $conditions = $this->conditionsHelper->decode($param);
        foreach ($conditions as $key => $condition) {
            if (!empty($condition['attribute']) && $condition['attribute'] == 'category_ids')    {
                return $condition['value'];
            }
        }
        return '';
    }
    public function loadCid($cid) {
        return $this->_category->getCollection()->addFieldToFilter('entity_id', $cid);
    }
    public function filterCategoryCondition($cid) {
        return $this->_category->load($cid)->getChildrenCategories();
    }
}