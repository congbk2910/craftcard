<?php

namespace Netbaseteam\CustomCategory\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\Registry;
use Magento\Widget\Helper\Conditions;

class CustomBlock extends Template implements BlockInterface
{
    /**
     * @var Conditions
     */
    protected $conditionsHelper;

    protected $_category;
    /**
     * @var Registry
     */
    protected $_registry;
    /**
     * @var string
     */
    protected $_template = "unsere_block.phtml";

    public function __construct(
        Template\Context $context,
        \Magento\Catalog\Model\Category $category,
        Registry $registry,
        Conditions $conditionsHelper,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->conditionsHelper = $conditionsHelper;
        $this->_registry = $registry;
        $this->_category = $category;
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getCategoryIds($param)
    {
        $conditions = $this->conditionsHelper->decode($param);
        foreach ($conditions as $key => $condition) {
            if (!empty($condition['attribute']) && $condition['attribute'] == 'category_ids')    {
                return $condition['value'];
            }
        }
        return '';
    }

    public function filterSubCategories($cid) {
        return $this->_category->load($cid)->getChildrenCategories();
    }
    public function loadCid($cid) {
        return $this->_category->getCollection()->addFieldToFilter('entity_id', $cid);
    }

}