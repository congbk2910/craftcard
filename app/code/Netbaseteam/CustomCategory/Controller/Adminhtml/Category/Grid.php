<?php
namespace Netbaseteam\CustomCategory\Controller\Adminhtml\Category;

class Grid extends \Magento\Catalog\Controller\Adminhtml\Category\Grid {
    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\View\LayoutFactory $layoutFactory)
    {
        parent::__construct($context, $resultRawFactory, $layoutFactory);
    }
    /**
     * Grid Action
     * Display list of products related to current category
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $category = $this->_initCategory(true);
        $customCategory = $this->_request->getParam('custom_category');
        if (!$category) {
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('catalog/*/', ['_current' => true, 'id' => null]);
        }
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        if($customCategory == 1) {
            return $resultRaw->setContents(
                $this->layoutFactory->create()->createBlock(
                    \Netbaseteam\CustomCategory\Block\Adminhtml\Category\Tab\Category::class,
                    'category.items.grid'
                )->toHtml()
            );
        } else {
            return $resultRaw->setContents(
                $this->layoutFactory->create()->createBlock(
                    \Magento\Catalog\Block\Adminhtml\Category\Tab\Product::class,
                    'category.product.grid'
                )->toHtml()
            );
        }
    }
}