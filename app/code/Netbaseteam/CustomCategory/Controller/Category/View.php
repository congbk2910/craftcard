<?php

namespace Netbaseteam\CustomCategory\Controller\Category;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Category\Attribute\LayoutUpdateManager;
use Magento\Catalog\Model\Design;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Product\ProductList\ToolbarMemorizer;
use Magento\Catalog\Model\Session;
use Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class View extends \Magento\Catalog\Controller\Category\View {

    protected $category;

    public function __construct(
        Context $context, Design $catalogDesign,
        Session $catalogSession, Registry $coreRegistry,
        StoreManagerInterface $storeManager,
        CategoryUrlPathGenerator $categoryUrlPathGenerator,
        PageFactory $resultPageFactory, ForwardFactory $resultForwardFactory,
        Category $category,
        Resolver $layerResolver, CategoryRepositoryInterface $categoryRepository,
        ToolbarMemorizer $toolbarMemorizer = null, LayoutUpdateManager $layoutUpdateManager = null,
        CategoryHelper $categoryHelper = null, LoggerInterface $logger = null)
    {
        parent::__construct($context, $catalogDesign, $catalogSession, $coreRegistry, $storeManager, $categoryUrlPathGenerator, $resultPageFactory, $resultForwardFactory, $layerResolver, $categoryRepository, $toolbarMemorizer, $layoutUpdateManager, $categoryHelper, $logger);
        $this->category = $category;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $categoryId = (int)$this->getRequest()->getParam('id', false);
        $catLevel = $this->category->load($categoryId)->getLevel();
        if($catLevel == 2) {
            $resultPage->addHandle('category_1columns');
        }
        return parent::execute();

    }
}