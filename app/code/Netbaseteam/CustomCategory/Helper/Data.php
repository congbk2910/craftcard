<?php

namespace Netbaseteam\CustomCategory\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Magento\Framework\App\PageCache\Version;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Registry
     */
    protected $_registry;
    protected $_productRepository;
    protected $_product;
    protected $_category;
    protected $_categorySelected;
    protected $_cacheTypeList;
    protected $_cacheFrontendPool;
    public function __construct(
        ProductRepositoryInterface $productRepository,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool,
        \Magento\Catalog\Model\Category $category,
        \Netbaseteam\CustomCategory\Model\CategorySelect $categorySelect,
        Product $product,
        Context $context,
        Registry $registry
    )
    {
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_categorySelected = $categorySelect;
        $this->_category = $category;
        parent::__construct($context);
        $this->_registry = $registry;
        $this->_product = $product;
        $this->_productRepository = $productRepository;
    }

    public function getBlockByCategory($cid) {
        return $this->_categorySelected->getCollection()->addFieldToFilter('cat_id', $cid);
    }

    public function getProductId($pid) {
        return $this->_productRepository->getById($pid);
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getProductSku($pid)
    {
        return $this->_product->load($pid);
    }

    public function getCategoryMode($cat) {
        return $this->_category->load($cat)->getDisplayMode();
    }
    public function getProductCollection($sku)
    {
        return $this->_product->getCollection()->addFieldToSelect("*")
            ->addAttributeToFilter('sku', $sku)
            ->addMediaGalleryData()
            ->getFirstItem()
            ->getMediaGalleryImages();
    }
    public function flushCache()
    {
        $_types = [
            'full_page'
        ];

        foreach ($_types as $type) {
            $this->_cacheTypeList->cleanType($type);
        }
    }
}
