<?php

namespace Netbaseteam\CustomCategory\Model\Config\Source;

class Collection implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'auto-by-newest', 'label' => __('Automatic By Newest')],
            ['value' => 'auto-by-random', 'label' => __('Automatic By Random')],
            ['value' => 'manual-by-category', 'label' => __('Manual Select Category')],
        ];
    }
}