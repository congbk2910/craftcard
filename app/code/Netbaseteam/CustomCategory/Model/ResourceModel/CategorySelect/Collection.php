<?php
namespace Netbaseteam\CustomCategory\Model\ResourceModel\CategorySelect;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Netbaseteam\CustomCategory\Model\CategorySelect', 'Netbaseteam\CustomCategory\Model\ResourceModel\CategorySelect');
    }
}
