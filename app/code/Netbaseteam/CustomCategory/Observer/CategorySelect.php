<?php

namespace Netbaseteam\CustomCategory\Observer;

class CategorySelect implements \Magento\Framework\Event\ObserverInterface
{
    protected $request;
    protected $_categorySelect;
    protected $connection;
    protected $resource;
    protected $_helper;
    public function __construct(
        \Netbaseteam\CustomCategory\Model\CategorySelect $categorySelect,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\RequestInterface $request,
        \Netbaseteam\CustomCategory\Helper\Data $helper
    )
    {
        $this->_helper = $helper;
        $this->_categorySelect = $categorySelect;
        $this->request = $request;
        $this->connection = $resource->getConnection();
        $this->resource = $resource;
    }

    public function insertMultiple($table, $data)
    {
        try {
            $tableName = $this->resource->getTableName($table);
            return $this->connection->insertMultiple($tableName, $data);
        } catch (\Exception $e) {

        }
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $catId = $this->request->getParam('entity_id');
        $model = $this->_categorySelect;
        $collection = $this->_categorySelect->getCollection();
        $selectedCat = json_decode($this->request->getParam('category_selected'));
        $arr = (array)$selectedCat;
        $catSelect = array_keys($arr);
        $oldProduct = $collection->addFieldToFilter('cat_id', $catId);
        $catSelected = [];
        foreach ($oldProduct as $item) {
            $catSelected[] = $item->getCatSelected();
        }
        $convert = array_flip($catSelect);
        $oldProducts = array_flip($catSelected);
        $insert = array_diff_key($convert, $oldProducts);
        $delete = array_diff_key($oldProducts, $convert);
        if (!empty($insert)) {
            $bulkInsert = [];
            foreach (array_flip($insert) as $key => $value) {
                $bulkInsert[] = [
                    'cat_selected' => $value,
                    'cat_id' => $catId
                ];
            }
            $this->connection->insertOnDuplicate('nb_custom_block_categories', $bulkInsert);
        }
        if (!empty($delete)) {
            $cond = ['cat_selected IN(?)' => array_keys($delete), 'cat_id=?' => $catId];
            $this->connection->delete('nb_custom_block_categories', $cond);
        }
        $this->_helper->flushCache();
    }
}