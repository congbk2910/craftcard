<?php
namespace Netbaseteam\CustomCategory\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            /**
             * Create table 'nb_custom_block_categories'
             */
            $table = $setup->getConnection()
                ->newTable($setup->getTable('nb_custom_block_categories'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'auto_increment' => true, 'primary' => true],
                    'Id'
                )
                ->addColumn(
                    'cat_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Cat Id'
                )
                ->addColumn(
                    'cat_selected',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Category Selected'
                )
                ->setComment(
                    'Multi Showing For Block'
                );

            $setup->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}