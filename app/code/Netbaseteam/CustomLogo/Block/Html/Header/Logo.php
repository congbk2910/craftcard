<?php
namespace Netbaseteam\CustomLogo\Block\Html\Header;

class Logo extends \Magento\Theme\Block\Html\Header\Logo {
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper,
        array $data = [])
    {
        parent::__construct($context, $fileStorageHelper, $data);
    }

    /**
     * Get logo image URL
     *
     * @return string
     */
    public function getLogoSrcMobile()
    {
        if (empty($this->_data['logo_src_mobile'])) {
            $this->_data['logo_src_mobile'] = $this->_getLogoUrlMobile();
        }
        return $this->_data['logo_src_mobile'];
    }
    /**
     * Retrieve logo image URL
     *
     * @return string
     */
    protected function _getLogoUrlMobile()
    {
        $folderName = \Magento\Config\Model\Config\Backend\Image\Logo::UPLOAD_DIR;
        $storeLogoMobilePath = $this->_scopeConfig->getValue(
            'design/header/logo_src_mobile',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $pathMobile = $folderName . '/' . $storeLogoMobilePath;
        $logoUrlMobile = $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $pathMobile;
        if ($storeLogoMobilePath !== null && $this->_isFile($pathMobile)) {
            $urlMobile = $logoUrlMobile;
        } elseif ($this->getLogoFile()) {
            $urlMobile = $this->getViewFileUrl($this->getLogoFile());
        } else {
            $urlMobile = $this->getViewFileUrl('images/logo.svg');
        }
        return $urlMobile;
    }
}