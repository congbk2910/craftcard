<?php
namespace Netbaseteam\CustomLogo\Plugin\Model\Design\Backend;

class Logo {
    public function afterGetAllowedExtensions(\Magento\Theme\Model\Design\Backend\Logo $subject, $result) {
        $subject = ['jpg', 'jpeg', 'gif', 'png', 'svg'];
        return $subject;
    }
}