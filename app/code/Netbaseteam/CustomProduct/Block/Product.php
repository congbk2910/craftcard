<?php
namespace Netbaseteam\CustomProduct\Block;

use Magento\Catalog\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;

class Product extends Template
{

    protected $_productLoader;

    public function __construct(
        Template\Context $context,
        \Magento\Catalog\Model\ProductFactory $productLoader,
        array $data = []
    )
   {
       parent::__construct($context, $data);
       $this->_productLoader = $productLoader;
   }

   public function getCustomAttr($pid) {
       return $this->_productLoader->create()->load($pid);
   }
}