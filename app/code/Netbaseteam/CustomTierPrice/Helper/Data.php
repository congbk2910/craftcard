<?php

namespace Netbaseteam\CustomTierPrice\Helper;
use Magento\Directory\Model\CurrencyFactory;
use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObject;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class Data
 * @package Netbaseteam\CustomTierPrice\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var
     */
    protected $_currencyFactory;
    protected $_objectManager;

    public function __construct(
        CurrencyFactory $currencyFactory,
        ObjectManagerInterface $objectManager,
        Context $context)
    {
        $this->_objectManager = $objectManager;
        $this->_currencyFactory =$currencyFactory;
        parent::__construct($context);
    }

    public function getCurrencySymbol() {
        return $this->_currencyFactory->create()->getCurrencySymbol();
    }
    public function hasDupes($array)
    {
        return count($array) !== count(array_unique($array));
    }

    /**
     * Validation
     *
     * @param $mes
     *
     * @return mixed
     */
    public function showError($mes)
    {
        $response = $this->getObject(DataObject::class);

        $response->setError(true);
        $response->setMessage(__($mes));

        return $response;
    }
    /**
     * @param $path
     *
     * @return mixed
     */
    public function getObject($path)
    {
        return $this->_objectManager->get($path);
    }
}
