<?php

namespace Netbaseteam\CustomTierPrice\Model\Config\Source;

/**
 * TierPrice types mode source.
 */
class ProductPriceOptions
{
    const VALUE_FIXED    = 'fixed';
    const VALUE_PERCENT  = 'percent';
    const PER_UNIT = 'nb_per_unit';

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::VALUE_FIXED, 'label' => __('Fixed')],
            ['value' => self::VALUE_PERCENT, 'label' => __('Percent')],
            ['value' => self::PER_UNIT, 'label' => __('Per Unit')],
        ];
    }
}
