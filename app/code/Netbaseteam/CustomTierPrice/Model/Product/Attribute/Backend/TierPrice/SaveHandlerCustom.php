<?php

namespace Netbaseteam\CustomTierPrice\Model\Product\Attribute\Backend\TierPrice;

use Magento\Catalog\Model\Product\Attribute\Backend\TierPrice\SaveHandler;

/**
 * Process tier price data for handled new product
 */
class SaveHandlerCustom extends SaveHandler
{
    /**
     * Get additional tier price fields.
     *
     * @param array $objectArray
     * @return array
     */
    protected function getAdditionalFields($objectArray) : array
    {
        $percentageValue = $this->getPercentage($objectArray);
        $perUnit   = $this->getPerUnit($objectArray);
        return [
            'value'             => ($percentageValue || $perUnit) ? null : $objectArray['price'],
            'percentage_value'  => $percentageValue ?: null,
            'nb_per_unit' => $perUnit ?: null,
        ];
    }
    /**
     * Check whether price has per unit value.
     *
     * @param array $priceRow
     *
     * @return null
     */
    private function getPerUnit($priceRow)
    {
        return isset($priceRow['nb_per_unit']) && is_numeric($priceRow['nb_per_unit'])
            ? $priceRow['nb_per_unit']
            : null;
    }
}
