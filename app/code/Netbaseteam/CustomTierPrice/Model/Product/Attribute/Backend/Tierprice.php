<?php

namespace Netbaseteam\CustomTierPrice\Model\Product\Attribute\Backend;

use Exception;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Catalog\Model\Attribute\ScopeOverriddenValue;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Backend\GroupPrice\AbstractGroupPrice;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Type\Price;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\Backend\Tierprice as ProductAttributeTierprice;
use Magento\Customer\Api\GroupManagementInterface;
use Magento\Customer\Model\Group;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Magento\Framework\EntityManager\Operation\Read\ReadAttributes;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Phrase;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Tierprice
 * @package Netbaseteam\CustomTierPrice\Model\Product\Attribute\Backend
 */
class Tierprice extends AbstractGroupPrice
{
    /**
     * Catalog product attribute backend tierprice
     *
     * @var ProductAttributeTierprice
     */
    protected $_productAttributeBackendTierprice;

    /**
     * @var ReadAttributes
     */
    protected $readAttributes;

    /**
     * @param CurrencyFactory $currencyFactory
     * @param StoreManagerInterface $storeManager
     * @param CatalogHelper $catalogData
     * @param ScopeConfigInterface $config
     * @param FormatInterface $localeFormat
     * @param Type $catalogProductType
     * @param GroupManagementInterface $groupManagement
     * @param ProductAttributeTierprice $productAttributeTierprice
     * @param ReadAttributes $readAttributes
     * @param ScopeOverriddenValue|null $scopeOverriddenValue
     */
    public function __construct(
        CurrencyFactory $currencyFactory,
        StoreManagerInterface $storeManager,
        CatalogHelper $catalogData,
        ScopeConfigInterface $config,
        FormatInterface $localeFormat,
        Type $catalogProductType,
        GroupManagementInterface $groupManagement,
        ProductAttributeTierprice $productAttributeTierprice,
        ReadAttributes $readAttributes,
        ScopeOverriddenValue $scopeOverriddenValue = null
    ) {
        $this->_productAttributeBackendTierprice = $productAttributeTierprice;
        $this->readAttributes                    = $readAttributes;

        parent::__construct(
            $currencyFactory,
            $storeManager,
            $catalogData,
            $config,
            $localeFormat,
            $catalogProductType,
            $groupManagement,
            $scopeOverriddenValue
        );
    }

    /**
     * Retrieve resource instance
     *
     * @return ProductAttributeTierprice
     */
    protected function _getResource()
    {
        return $this->_productAttributeBackendTierprice;
    }

    /**
     * Add price qty to unique fields
     *
     * @param array $objectArray
     *
     * @return array
     * @throws Exception
     */
    protected function _getAdditionalUniqueFields($objectArray)
    {
        try {
            $uniqueFields        = parent::_getAdditionalUniqueFields($objectArray);
            $uniqueFields['qty'] = $objectArray['price_qty'] * 1;

            return $uniqueFields;
        } catch (Exception $e) {
            throw new Exception(__('Required fields cannot empty'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function getAdditionalFields($objectArray)
    {
        $percentageValue = $this->getPercentage($objectArray);
        $perUnit   = $this->getPerUnit($objectArray);
        return [
            'value'             => ($percentageValue || $perUnit) ? null : $objectArray['price'],
            'percentage_value'  => $percentageValue ?: null,
            'nb_per_unit' => $perUnit ?: null,
        ];
    }

    /**
     * Error message when duplicates
     *
     * @return Phrase
     */
    protected function _getDuplicateErrorMessage()
    {
        return __('We found a duplicate website, tier price, customer group and quantity.');
    }

    /**
     * Whether tier price value fixed or percent of original price
     *
     * @param Price $priceObject
     *
     * @return bool
     */
    protected function _isPriceFixed($priceObject)
    {
        return $priceObject->isTierPriceFixed();
    }

    /**
     * By default attribute value is considered non-scalar that can be stored in a generic way
     *
     * @return bool
     */
    public function isScalar()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function validate($object)
    {
        $attribute = $this->getAttribute();
        $priceRows = $object->getData($attribute->getName());
        $priceRows = array_filter((array) $priceRows);

        foreach ($priceRows as $priceRow) {
            $percentage    = $this->getPercentage($priceRow);
            $perUnit = $this->getPerUnit($priceRow);
            if ($perUnit !== null && (!$this->isPositiveOrZero($perUnit))) {
                throw new LocalizedException(
                    __('Per Unit value must be a number greater than 0.')
                );
            }
            if ($percentage !== null && (!$this->isPositiveOrZero($percentage) || $percentage > 100)) {
                throw new LocalizedException(
                    __('Percentage value must be a number between 0 and 100.')
                );
            }
        }

        return parent::validate($object);
    }

    /**
     * @inheritdoc
     */
    protected function validatePrice(array $priceRow)
    {
        if (!$this->getPercentage($priceRow) && !$this->getPerUnit($priceRow)) {
            parent::validatePrice($priceRow);
        }
    }

    /**
     * @inheritdoc
     * @throws NoSuchEntityException
     */
    protected function modifyPriceData($object, $data)
    {
        /** @var Product $object */
        $data  = parent::modifyPriceData($object, $data);
        $price = $object->getPrice();
        foreach ($data as $key => $tierPrice) {
            $percentageValue = $this->getPercentage($tierPrice);
            $perUnit   = $this->getPerUnit($tierPrice);
            if ($percentageValue) {
                $data[$key]['price']         = $price * (1 - $percentageValue / 100);
                $data[$key]['website_price'] = $data[$key]['price'];
            } elseif ($perUnit) {
                $data[$key]['price']         = $tierPrice['nb_per_unit'];
                $data[$key]['website_price'] = $data[$key]['price'];
            }
        }
        $data = $this->prepareTierPriceData($data, $object);

        return $data;
    }

    /**
     * @param array $data
     * @param Product $object
     *
     * @return mixed
     * @throws NoSuchEntityException
     */
    private function prepareTierPriceData($data, $object)
    {
        $objectManager = ObjectManager::getInstance();
        $customerSession = $objectManager->create(CustomerSession::class);
        $customerId      = $customerSession ? (int) $customerSession->getCustomerId() : null;
        if (!$customerId || empty($specificCustomerData)) {
            return $data;
        }
        if (is_string($specificCustomerData)) {
            $specificCustomerData = Data::jsonDecode($specificCustomerData);
        }
        $data = $this->prepareDataForSpecificCustomer($data, $specificCustomerData, $object, $customerId);

        return $data;
    }

    /**
     * @param array $data
     * @param array $specificCustomerData
     * @param Product $object
     * @param int $customerId
     *
     * @return array
     */
    private function prepareDataForSpecificCustomer($data, $specificCustomerData, $object, $customerId)
    {
        $specificCustomersData = $this->prepareSpecificCustomersData($specificCustomerData, $object, $customerId);

        if (!empty($specificCustomersData)) {
            foreach ($specificCustomersData as $specificCustomer) {
                $isNew = true;
                foreach ($data as &$tierPrice) {
                    if ($specificCustomer['website_id'] === $tierPrice['website_id']
                        && $specificCustomer['price_qty'] === $tierPrice['price_qty']
                    ) {
                        if ($tierPrice['price'] > $specificCustomer['price']) {
                            $tierPrice['price']             = $specificCustomer['price'];
                            $tierPrice['percentage_value']  = $specificCustomer['percentage_value'];
                            $tierPrice['nb_per_unit'] = $specificCustomer['nb_per_unit'];
                            $tierPrice['website_price']     = $specificCustomer['website_price'];
                        }
                        $isNew = false;
                        break;
                    }
                }
                unset($tierPrice);
                if ($isNew) {
                    $data[] = $specificCustomer;
                }
            }
        }
        usort($data, function ($tierA, $tierB) {
            return ($tierA['price_qty'] <= $tierB['price_qty']) ? -1 : 1;
        });

        return $data;
    }

    /**
     * @param array $specificCustomerData
     * @param Product $object
     * @param int $customerId
     *
     * @return array
     */
    private function prepareSpecificCustomersData($specificCustomerData, $object, $customerId)
    {
        $specificCustomersData = [];
        foreach ($specificCustomerData as $datum) {
            if ((int) $datum['customer_id'] === $customerId) {
                $price               = $object->getPrice();
                $datum['all_groups'] = 1;
                $datum['cust_group'] = Group::CUST_GROUP_ALL;
                switch ($datum['value_type']) {
                    case 'fixed':
                        $datum['percentage_value']  = null;
                        $datum['nb_per_unit'] = null;
                        break;
                    case 'percent':
                        $datum['nb_per_unit'] = null;
                        $datum['price']             = $price * (1 - $datum['percentage_value'] / 100);
                        break;
                    case 'nb_per_unit':
                        $datum['percentage_value'] = null;
                        $datum['price']            = ($price - $datum['nb_per_unit']) > 0
                            ? ($price - $datum['nb_per_unit'])
                            : 0;
                        break;
                }
                $datum['website_price'] = $datum['price'];

                $specificCustomersData[] = $datum;
            }
        }

        return $specificCustomersData;
    }


    /**
     * @param array $valuesToUpdate
     * @param array $oldValues
     *
     * @return boolean
     */
    protected function updateValues(array $valuesToUpdate, array $oldValues)
    {
        $isChanged = false;
        foreach ($valuesToUpdate as $key => $value) {
            if ((!empty($value['value']) && $oldValues[$key]['price'] != $value['value'])
                || $this->getPercentage($oldValues[$key]) != $this->getPercentage($value)
                || $this->getPerUnit($oldValues[$key]) != $this->getPerUnit($value)
            ) {
                $price = new DataObject(
                    [
                        'value_id'          => $oldValues[$key]['price_id'],
                        'value'             => $value['value'],
                        'percentage_value'  => $this->getPercentage($value),
                        'nb_per_unit' => $this->getPerUnit($value)
                    ]
                );
                $this->_getResource()->savePriceData($price);

                $isChanged = true;
            }
        }

        return $isChanged;
    }

    /**
     * Check whether price has percentage value.
     *
     * @param array $priceRow
     *
     * @return null
     */
    private function getPercentage($priceRow)
    {
        return isset($priceRow['percentage_value']) && is_numeric($priceRow['percentage_value'])
            ? $priceRow['percentage_value']
            : null;
    }

    /**
     * Check whether price has per unit value.
     *
     * @param array $priceRow
     *
     * @return null
     */
    private function getPerUnit($priceRow)
    {
        return isset($priceRow['nb_per_unit']) && is_numeric($priceRow['nb_per_unit'])
            ? $priceRow['nb_per_unit']
            : null;
    }
}
