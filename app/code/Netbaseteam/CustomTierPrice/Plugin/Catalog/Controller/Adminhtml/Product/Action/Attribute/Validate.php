<?php

namespace Netbaseteam\CustomTierPrice\Plugin\Catalog\Controller\Adminhtml\Product\Action\Attribute;

use Magento\Framework\Controller\Result\JsonFactory;
use Netbaseteam\CustomTierPrice\Helper\Data;
use Netbaseteam\CustomTierPrice\Model\Config\Source\ProductPriceOptions;

/**
 * Class Validate
 * @package Netbaseteam\CustomTierPrice\Plugin\Catalog\Controller\Adminhtml\Product\Action\Attribute
 */
class Validate
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Data
     */
    protected $helper;

    public function __construct(
        JsonFactory $resultJsonFactory,
        Data $helper
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper            = $helper;
    }

    public function afterExecute(
        \Magento\Catalog\Controller\Adminhtml\Product\Action\Attribute\Validate $subject,
        $response
    ) {
        $mpTierPriceData    = $subject->getRequest()->getParam('nbTierPriceData');
        $mpTierPriceData    = json_decode($mpTierPriceData, true);
        if ($mpTierPriceData) {
            $response = $this->validateTierPrice($mpTierPriceData, true, $response);
        }
        return $response;
    }

    protected function validateTierPrice($tierPrice = [], $isMpTierPriceData = true, $response)
    {
        $dupTierPriceQty = [];
        foreach ($tierPrice as $item) {
            $dupTierPriceQty[] = $item['price_qty'];

            if (empty($item['price_qty'])) {
                $response = $this->helper->showError('Required fields cannot empty');

                return $this->resultJsonFactory->create()->setJsonData($response->toJson());
            }

            if ($item['value_type'] == ProductPriceOptions::VALUE_PERCENT && (!isset($item['percentage_value']) || (isset($item['percentage_value']) && $item['percentage_value'] <= 0))) {
                $response = $this->helper->showError('Discount Percent price must be a number greater than 0.');

                return $this->resultJsonFactory->create()->setJsonData($response->toJson());
            }

            if ($item['value_type'] == ProductPriceOptions::PER_UNIT && (!isset($item['nb_per_unit']) || (isset($item['nb_per_unit']) && $item['nb_per_unit'] <= 0))) {
                $response = $this->helper->showError('Per Unit price must be a number greater than 0.');

                return $this->resultJsonFactory->create()->setJsonData($response->toJson());
            }
        }

        if ($this->helper->hasDupes($dupTierPriceQty)) {
            $response = $this->helper->showError('We found a duplicate website, tier price for special customer(s) and quantity.');

            if ($isMpTierPriceData) {
                $response = $this->helper->showError('We found a duplicate website, tier price, customer group and quantity.');
            }

            return $this->resultJsonFactory->create()->setJsonData($response->toJson());
        }

        return $response;
    }
}
