<?php


namespace Netbaseteam\CustomTierPrice\Plugin\Catalog\Controller\Adminhtml\Product;

use Magento\Framework\App\RequestInterface;

/**
 * Class Builder
 * @package Netbaseteam\CustomTierPrice\Plugin\Catalog\Controller\Product
 */
class Builder
{
    /**
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $subject
     * @param RequestInterface $request
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeBuild(
        \Magento\Catalog\Controller\Adminhtml\Product\Builder $subject,
        RequestInterface $request
    )
    {
        $params = $request->getPost('product');

        if (!isset($params['nb_specific_customer'])) {
            $params['nb_specific_customer'] = [];
            $request->setPostValue('product', $params);
        }

        return [$request];
    }
}
