<?php

namespace Netbaseteam\CustomTierPrice\Plugin\Catalog\Model\ResourceModel\Product\Attribute\Backend;

use Magento\Framework\Db\Select;

/**
 * Class Tierprice
 * @package Netbaseteam\CustomTierPrice\Plugin\Catalog\Model\Product\Attribute\Backend
 */
class Tierprice
{
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\Backend\Tierprice $tierprice
     * @param Select $select
     *
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetSelect(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\Backend\Tierprice $tierprice,
        $select
    ) {
        $select->columns(['nb_per_unit', 'nb_per_unit']);
        return $select;
    }
}
