<?php

namespace Netbaseteam\CustomTierPrice\Plugin\Catalog\Pricing\Price;

use Magento\Catalog\Model\Product;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class TierPrice
 * @package Netbaseteam\CustomTierPrice\Plugin\Catalog\Pricing\Price
 */
class TierPrice
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;


    /**
     * TierPrice constructor.
     *
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    )
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Magento\Catalog\Pricing\Price\Tierprice $tierprice
     * @param $tierPriceList
     *
     * @return mixed
     */
    public function afterGetTierPriceList(
        \Magento\Catalog\Pricing\Price\Tierprice $tierprice,
        $tierPriceList
    )
    {
        $tierPriceList = $this->prepareDataForApplicableOnly($tierPriceList, $tierprice->getProduct());
        return $tierPriceList;
    }

    /**
     * @param array $data
     * @param Product|SaleableInterface $object
     *
     * @return mixed
     */
    public function prepareDataForApplicableOnly($data, $object)
    {
        usort($data, function ($tierA, $tierB) {
            return ($tierA['price_qty'] <= $tierB['price_qty']) ? -1 : 1;
        });
        $min = null;
        $prevKey = null;
        $currentQty = null;
        foreach ($data as $key => &$tierPrice) {
            if ($min === null) {
                $min = $object->getTypeId() === 'bundle'
                    ? $tierPrice['percentage_value']
                    : $tierPrice['website_price'];
                $prevKey = $key;
                $currentQty = $tierPrice['price_qty'];
                continue;
            }
            if ($object->getTypeId() === 'bundle') {
                if ($tierPrice['percentage_value'] > $min) {
                    $min = $tierPrice['percentage_value'];
                    if ($tierPrice['price_qty'] === $currentQty) {
                        unset($data[$prevKey]);
                    }
                } else {
                    unset($data[$key]);
                }
            } elseif ($tierPrice['website_price'] < $min) {
                $min = $tierPrice['website_price'];
                if ($tierPrice['price_qty'] === $currentQty) {
                    unset($data[$prevKey]);
                }
            }
            else {
            //    unset($data[$key]);
            }
            $prevKey = $key;
            $currentQty = $tierPrice['price_qty'];
        }
        unset($tierPrice);
        return $data;
    }
}
