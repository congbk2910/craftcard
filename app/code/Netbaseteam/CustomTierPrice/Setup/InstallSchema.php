<?php

namespace Netbaseteam\CustomTierPrice\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer  = $setup;
        $connection = $installer->getConnection();
        $installer->startSetup();

        $tableName = $setup->getTable('catalog_product_entity_tier_price');

        if (!$connection->tableColumnExists($tableName, 'nb_per_unit')) {
            $connection->addColumn($tableName, 'nb_per_unit', [
                'type'     => Table::TYPE_DECIMAL,
                'nullable' => true,
                'length'   => '5,2',
                'comment'  => 'Custom Per Unit Tier Price',
                'after'    => 'value'
            ]);
        }

        $installer->endSetup();
    }
}
