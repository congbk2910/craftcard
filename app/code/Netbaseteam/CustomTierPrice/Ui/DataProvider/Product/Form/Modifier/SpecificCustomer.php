<?php

namespace Netbaseteam\CustomTierPrice\Ui\DataProvider\Product\Form\Modifier;

use Magento\Backend\Model\UrlInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Config\Model\ResourceModel\Config\Data as ConfigValueResource;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\Config\Value as ConfigValue;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Website;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Price;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Hidden;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Modal;;
use Netbaseteam\CustomTierPrice\Model\Config\Source\ProductPriceOptions;

/**
 * Class SpecificCustomer
 * @package Netbaseteam\CustomTierPrice\Ui\DataProvider\Product\Modifier
 */
class SpecificCustomer extends AbstractModifier
{
    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @type array
     */
    protected $_meta;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var DirectoryHelper
     */
    protected $directoryHelper;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @var ConfigValue
     */
    protected $configValue;

    /**
     * @var ConfigValueResource
     */
    protected $configValueResource;

    /**
     * @var ProductPriceOptions
     */
    private $productPriceOptions;


    /**
     * SpecificCustomer constructor.
     *
     * @param DirectoryHelper $directoryHelper
     * @param StoreManagerInterface $storeManager
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     * @param RequestInterface $request
     * @param UrlInterface $url
     * @param ConfigValue $configValue
     * @param ConfigValueResource $configValueResource
     * @param ProductPriceOptions $productPriceOptions
     */
    public function __construct(
        DirectoryHelper $directoryHelper,
        StoreManagerInterface $storeManager,
        LocatorInterface $locator,
        ArrayManager $arrayManager,
        RequestInterface $request,
        UrlInterface $url,
        ConfigValue $configValue,
        ConfigValueResource $configValueResource,
        ProductPriceOptions $productPriceOptions
    ) {
        $this->locator             = $locator;
        $this->arrayManager        = $arrayManager;
        $this->request             = $request;
        $this->storeManager        = $storeManager;
        $this->directoryHelper     = $directoryHelper;
        $this->url                 = $url;
        $this->configValue         = $configValue;
        $this->configValueResource = $configValueResource;
        $this->productPriceOptions = $productPriceOptions;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @param array $meta
     *
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $this->_meta = $meta;
        $tierPricePath = $this->arrayManager->findPath(
            ProductAttributeInterface::CODE_TIER_PRICE,
            $this->_meta,
            null,
            'children'
        );
        if ($tierPricePath) {
            $this->customizePerUnit();
        }

        return $this->_meta;
    }

    /**
     * Customize Discount Fixed field
     *
     * @return $this
     */
    protected function customizePerUnit()
    {
        $tierPricePath = $this->arrayManager->findPath(
            ProductAttributeInterface::CODE_TIER_PRICE,
            $this->_meta,
            null,
            'children'
        );
        if (!$tierPricePath) {
            return $this;
        }

        $pricePath = $this->arrayManager->findPath(
            'price_value',
            $this->_meta,
            $tierPricePath
        );

        $this->_meta = $this->arrayManager->merge(
            $this->arrayManager->slicePath($pricePath, 0, -1),
            $this->_meta,
            $this->getUpdatedTierPriceStructure()
        );

        return $this;
    }

    /**
     * @return array
     */
    private function getUpdatedTierPriceStructure()
    {
        $priceTypeOptions = $this->productPriceOptions->toOptionArray();
        $firstOption      = $priceTypeOptions ? current($priceTypeOptions) : null;

        return [
            'price_value' => [
                'children' => [
                    'value_type'                        => [
                        'arguments' => [
                            'data' => [
                                'options' => $priceTypeOptions,
                                'config'  => [
                                    'prices' => [
                                        ProductPriceOptions::PER_UNIT => '${ $.parentName }.'
                                            . ProductPriceOptions::PER_UNIT,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    ProductPriceOptions::PER_UNIT => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'name'          => '${ $.parentName }.nb_per_unit',
                                    'componentType' => Field::NAME,
                                    'formElement'   => Input::NAME,
                                    'dataType'      => Price::NAME,
                                    'addbefore'     => $this->getStore()
                                        ->getBaseCurrency()
                                        ->getCurrencySymbol(),
                                    'validation'    => [
                                        'validate-number' => true,
                                    ],
                                    'visible'       => $firstOption
                                        && $firstOption['value'] === ProductPriceOptions::PER_UNIT,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Retrieve store
     *
     * @return StoreInterface
     */
    protected function getStore()
    {
        return $this->locator->getStore();
    }
}
