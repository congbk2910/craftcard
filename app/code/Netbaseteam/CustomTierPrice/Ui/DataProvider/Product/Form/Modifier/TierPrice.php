<?php

namespace Netbaseteam\CustomTierPrice\Ui\DataProvider\Product\Form\Modifier;

use Magento\Backend\Model\UrlInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Config\Source\ProductPriceOptionsInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Config\Model\ResourceModel\Config\Data as ConfigValueResource;
use Magento\Customer\Api\GroupManagementInterface;
use Magento\Customer\Model\Customer\Source\GroupSourceInterface;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\Config\Value as ConfigValue;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Module\Manager as ModuleManager;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Website;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Price;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Hidden;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Modal;
use Netbaseteam\CustomTierPrice\Model\Config\Source\ProductPriceOptions;

/**
 * Class TierPrice
 * @package Netbaseteam\CustomTierPrice\Ui\DataProvider\Product\Form\Modifier
 */
class TierPrice extends AbstractModifier
{
    /**
     * @var ProductPriceOptions
     */
    private $productPriceOptions;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var DirectoryHelper
     */
    protected $directoryHelper;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ModuleManager
     * @since 101.0.0
     */
    protected $moduleManager;

    /**
     * @var GroupSourceInterface
     */
    private $customerGroupSource;

    /**
     * @var GroupManagementInterface
     */
    protected $groupManagement;

    /**
     * @var ConfigValue
     */
    protected $configValue;

    /**
     * @var ConfigValueResource
     */
    protected $configValueResource;
    

    /**
     * @var array
     */
    private $meta;

    /**
     * @param ProductPriceOptions $productPriceOptions
     * @param ArrayManager $arrayManager
     * @param LocatorInterface $locator
     * @param UrlInterface $url
     * @param StoreManagerInterface $storeManager
     * @param DirectoryHelper $directoryHelper
     * @param RequestInterface $request
     * @param ModuleManager $moduleManager
     * @param GroupSourceInterface $customerGroupSource
     * @param GroupManagementInterface $groupManagement
     * @param ConfigValue $configValue
     * @param ConfigValueResource $configValueResource
    
     */
    public function __construct(
        ProductPriceOptions $productPriceOptions,
        ArrayManager $arrayManager,
        LocatorInterface $locator,
        UrlInterface $url,
        StoreManagerInterface $storeManager,
        DirectoryHelper $directoryHelper,
        RequestInterface $request,
        ModuleManager $moduleManager,
        GroupSourceInterface $customerGroupSource,
        GroupManagementInterface $groupManagement,
        ConfigValue $configValue,
        ConfigValueResource $configValueResource
    ) {
        $this->productPriceOptions = $productPriceOptions;
        $this->arrayManager        = $arrayManager;
        $this->locator             = $locator;
        $this->url                 = $url;
        $this->storeManager        = $storeManager;
        $this->directoryHelper     = $directoryHelper;
        $this->request             = $request;
        $this->moduleManager       = $moduleManager;
        $this->customerGroupSource = $customerGroupSource;
        $this->groupManagement     = $groupManagement;
        $this->configValue         = $configValue;
        $this->configValueResource = $configValueResource;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @param array $meta
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function modifyMeta(array $meta)
    {
        $this->meta                             = $meta;
        $groupCode                              = 'product_details';
        $this->meta[$groupCode]                 = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Fieldset::NAME,
                        'label'         => __('Tier Price'),
                        'collapsible'   => false,
                        'dataScope'     => self::DATA_SCOPE_PRODUCT,
                        'sortOrder'     => 10,
                        'provider'      => 'nb_tier_price_update.tier_price_data_source',
                    ]
                ]
            ],
            'children'  => [
                'tier_price'              => $this->getTierPriceStructure(),
            ],
        ];

        return $this->meta;
    }
    
    /**
     * Check tier_price attribute scope is global
     *
     * @return bool
     */
    protected function isScopeGlobal()
    {
        return false;
    }

    /**
     * Show website column and switcher for group price table
     *
     * @return bool
     */
    protected function isMultiWebsites()
    {
        return !$this->storeManager->isSingleStoreMode();
    }

    /**
     * Get websites list
     *
     * @return array
     * @throws NoSuchEntityException
     */
    protected function getWebsites()
    {
        $websites = [
            [
                'label' => __('All Websites') . ' [' . $this->directoryHelper->getBaseCurrencyCode() . ']',
                'value' => 0,
            ]
        ];

        if (!$this->isScopeGlobal() && $this->storeManager->getStore()->getId()) {
            /** @var Website $website */
            $website = $this->getStore()->getWebsite();

            $websites[] = [
                'label' => $website->getName() . '[' . $website->getBaseCurrencyCode() . ']',
                'value' => $website->getId(),
            ];
        } elseif (!$this->isScopeGlobal()) {
            $websitesList = $this->storeManager->getWebsites();
            foreach ($websitesList as $website) {
                /** @var Website $website */
                $websites[] = [
                    'label' => $website->getName() . '[' . $website->getBaseCurrencyCode() . ']',
                    'value' => $website->getId(),
                ];
            }
        }

        return $websites;
    }

    /**
     * Retrieve default value for website
     *
     * @return int
     */
    public function getDefaultWebsite()
    {
        if ($this->isShowWebsiteColumn() && !$this->isAllowChangeWebsite()) {
            return $this->getStore()->getWebsiteId();
        }

        return 0;
    }

    /**
     * Check is allow change website value for combination
     *
     * @return bool
     */
    protected function isAllowChangeWebsite()
    {
        return !(
            !$this->isShowWebsiteColumn()
            || $this->getStore()->getId()
        );
    }

    /**
     * Show group prices grid website column
     *
     * @return bool
     */
    protected function isShowWebsiteColumn()
    {
        return !($this->isScopeGlobal() || $this->storeManager->isSingleStoreMode());
    }

    /**
     * Get tier price dynamic rows structure
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getTierPriceStructure()
    {
        $priceTypeOptions = $this->productPriceOptions->toOptionArray();
        $firstOption      = $priceTypeOptions ? current($priceTypeOptions) : null;

        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType'       => 'dynamicRows',
                        'component'           => 'Magento_Catalog/js/components/dynamic-rows-tier-price',
                        'label'               => __('Customer Group Price'),
                        'renderDefaultRecord' => false,
                        'recordTemplate'      => 'record',
                        'dataScope'           => '',
                        'dndConfig'           => [
                            'enabled' => false,
                        ],
                        'disabled'            => false,
                        'required'            => false,
                        'sortOrder'           => 20,
                        'additionalClasses'   => 'nb-specific-customer',
                        'provider'            => 'nb_tier_price_update.tier_price_data_source',
                        'template'            => 'Netbaseteam_CustomTierPrice/ui/dynamic-rows/templates/default',
                    ],
                ],
            ],
            'children'  => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'isTemplate'    => true,
                                'is_collection' => true,
                                'component'     => 'Magento_Ui/js/dynamic-rows/record',
                                'dataScope'     => '',
                            ],
                        ],
                    ],
                    'children'  => [
                        'website_id'   => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'dataType'      => Text::NAME,
                                        'formElement'   => Select::NAME,
                                        'componentType' => Field::NAME,
                                        'dataScope'     => 'website_id',
                                        'label'         => __('Website'),
                                        'options'       => $this->getWebsites(),
                                        'value'         => $this->getDefaultWebsite(),
                                        'visible'       => $this->isMultiWebsites(),
                                        'disabled'      =>
                                            $this->isShowWebsiteColumn() && !$this->isAllowChangeWebsite(),
                                        'sortOrder'     => 10,
                                        'provider'      => 'nb_tier_price_update.tier_price_data_source',

                                    ],
                                ],
                            ],
                        ],
                        'cust_group'   => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement'   => Select::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType'      => Text::NAME,
                                        'dataScope'     => 'cust_group',
                                        'label'         => __('Customer Group'),
                                        'options'       => $this->getCustomerGroups(),
                                        'value'         => $this->getDefaultCustomerGroup(),
                                        'sortOrder'     => 20,
                                        'provider'      => 'nb_tier_price_update.tier_price_data_source',

                                    ],
                                ],
                            ],
                        ],
                        'price_qty'    => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement'   => Input::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType'      => Number::NAME,
                                        'label'         => __('Quantity'),
                                        'dataScope'     => 'price_qty',
                                        'sortOrder'     => 30,
                                        'validation'    => [
                                            'required-entry'             => true,
                                            'validate-greater-than-zero' => true,
                                            'validate-digits'            => true,
                                        ],
                                        'provider'      => 'nb_tier_price_update.tier_price_data_source',
                                        'template'      => 'Netbaseteam_CustomTierPrice/ui/form/element/input-require',
                                    ],
                                ],
                            ],
                        ],
                        'price_value'  => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType'     => Container::NAME,
                                        'formElement'       => Container::NAME,
                                        'dataType'          => Price::NAME,
                                        'component'         => 'Magento_Ui/js/form/components/group',
                                        'label'             => __('Price'),
                                        'enableLabel'       => true,
                                        'dataScope'         => '',
                                        'additionalClasses' => 'control-grouped',
                                        'sortOrder'         => 40,
                                        'provider'          => 'nb_tier_price_update.tier_price_data_source'
                                    ],
                                ],
                            ],
                            'children'  => [
                                ProductAttributeInterface::CODE_TIER_PRICE_FIELD_VALUE_TYPE       => [
                                    'arguments' => [
                                        'data' => [
                                            'options' => $priceTypeOptions,
                                            'config'  => [
                                                'componentType' => Field::NAME,
                                                'formElement'   => Select::NAME,
                                                'dataType'      => 'text',
                                                'component'     => 'Magento_Catalog/js/tier-price/value-type-select',
                                                'provider'      => 'nb_tier_price_update.tier_price_data_source',
                                                'prices'        => [
                                                    ProductPriceOptionsInterface::VALUE_FIXED   => '${ $.parentName }.'
                                                        . ProductAttributeInterface::CODE_TIER_PRICE_FIELD_PRICE,
                                                    ProductPriceOptionsInterface::VALUE_PERCENT => '${ $.parentName }.'
                                                        . ProductAttributeInterface::CODE_TIER_PRICE_FIELD_PERCENTAGE_VALUE,
                                                    ProductPriceOptions::PER_UNIT         => '${ $.parentName }.'
                                                        . ProductPriceOptions::PER_UNIT,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                ProductAttributeInterface::CODE_TIER_PRICE_FIELD_PRICE            => [
                                    'arguments' => [
                                        'data' => [
                                            'config' => [
                                                'provider' => 'nb_tier_price_update.tier_price_data_source',

                                                'componentType' => Field::NAME,
                                                'formElement'   => Input::NAME,
                                                'dataType'      => Price::NAME,
                                                'label'         => __('Price'),
                                                'enableLabel'   => true,
                                                'dataScope'     => 'price',
                                                'addbefore'     => $this->getStore()
                                                    ->getBaseCurrency()
                                                    ->getCurrencySymbol(),
                                                'sortOrder'     => 40,
                                                'validation'    => [
                                                    'required-entry'             => true,
                                                    'validate-greater-than-zero' => true,
                                                    'validate-number'            => true,
                                                ],
                                                'imports'       => [
                                                    'priceValue' => '${ $.provider }:data.product.price',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                ProductAttributeInterface::CODE_TIER_PRICE_FIELD_PERCENTAGE_VALUE => [
                                    'arguments' => [
                                        'data' => [
                                            'config' => [
                                                'provider' => 'nb_tier_price_update.tier_price_data_source',

                                                'componentType' => Field::NAME,
                                                'formElement'   => Input::NAME,
                                                'dataType'      => Price::NAME,
                                                'addbefore'     => '%',
                                                'validation'    => [
                                                    'validate-number'     => true,
                                                    'less-than-equals-to' => 100
                                                ],
                                                'visible'       => $firstOption && $firstOption['value'] === ProductPriceOptionsInterface::VALUE_PERCENT,
                                            ],
                                        ],
                                    ],
                                ],
                                ProductPriceOptions::PER_UNIT                               => [
                                    'arguments' => [
                                        'data' => [
                                            'config' => [
                                                'provider' => 'nb_tier_price_update.tier_price_data_source',

                                                'name'          => '${ $.parentName }.nb_per_unit',
                                                'componentType' => Field::NAME,
                                                'formElement'   => Input::NAME,
                                                'dataType'      => Price::NAME,
                                                'addbefore'     => $this->getStore()
                                                    ->getBaseCurrency()
                                                    ->getCurrencySymbol(),
                                                'validation'    => [
                                                    'validate-number' => true,
                                                ],
                                                'visible'       => $firstOption
                                                    && $firstOption['value'] === ProductPriceOptions::PER_UNIT,
                                            ],
                                        ],
                                    ],
                                ],
                                'price_calc'                                                      => [
                                    'arguments' => [
                                        'data' => [
                                            'config' => [
                                                'provider' => 'nb_tier_price_update.tier_price_data_source',

                                                'componentType' => Container::NAME,
                                                'component'     => 'Magento_Catalog/js/tier-price/percentage-processor',
                                                'visible'       => false
                                            ],
                                        ],
                                    ]
                                ]
                            ],
                        ],
                        'actionDelete' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => 'actionDelete',
                                        'dataType'      => Text::NAME,
                                        'label'         => '',
                                        'sortOrder'     => 50,
                                        'provider'      => 'nb_tier_price_update.tier_price_data_source',

                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Retrieve allowed customer groups
     *
     * @return array
     */
    private function getCustomerGroups()
    {
        if (!$this->moduleManager->isEnabled('Magento_Customer')) {
            return [];
        }

        return $this->customerGroupSource->toOptionArray();
    }

    /**
     * Retrieve default value for customer group
     *
     * @return int
     * @throws LocalizedException
     */
    private function getDefaultCustomerGroup()
    {
        return $this->groupManagement->getAllCustomersGroup()->getId();
    }

    /**
     * Retrieve store
     *
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    protected function getStore()
    {
        return $this->storeManager->getStore($this->request->getParam('store') ?: 0);
    }
}
