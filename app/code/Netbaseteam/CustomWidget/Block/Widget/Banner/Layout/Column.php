<?php
namespace Netbaseteam\CustomWidget\Block\Widget\Banner\Layout;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Column extends Template implements BlockInterface {

    protected $_template = "widget/banner/layout/column.phtml";

}