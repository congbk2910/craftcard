<?php

namespace Netbaseteam\CustomWidget\Block\Widget;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Widget\Helper\Conditions;

class Category extends Template implements BlockInterface
{
    protected $_category;

    /**
     * @var Conditions
     */
    protected $conditionsHelper;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var string
     */
    protected $_template = "widget/category/category_teaser.phtml";

    public function __construct(
        Template\Context $context,
        \Magento\Catalog\Model\Category $category,
        Conditions $conditionsHelper,
        Registry $registry,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->conditionsHelper = $conditionsHelper;
        $this->_category = $category;
        $this->_registry = $registry;
    }
}