<?php
namespace Netbaseteam\CustomWidget\Helper\Wysiwyg;

class Images extends \Magento\Cms\Helper\Wysiwyg\Images
{
    /**
     * Prepare Image insertion declaration for Wysiwyg or textarea(as_is mode)
     *
     * @param string $filename Filename transferred via Ajax
     * @param bool $renderAsTag Leave image HTML as is or transform it to controller directive
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getImageHtmlDeclaration($filename, $renderAsTag = false)
    {
        $fileUrl = $this->getCurrentUrl() . $filename;
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $mediaPath = str_replace($mediaUrl, '', $fileUrl);
        $directive = sprintf('{{media url="%s"}}', $mediaPath);
        if ($renderAsTag) {
            $src = $this->isUsingStaticUrlsAllowed() ? $fileUrl : $this->escaper->escapeHtml($directive);
            $html = sprintf('<img src="%s" alt="" />', $src);
        } else {
            $html = $fileUrl;
//            if ($this->isUsingStaticUrlsAllowed()) {
//            } else {
//                $directive = $this->urlEncoder->encode($directive);
//                $html = $this->_backendData->getUrl(
//                    'cms/wysiwyg/directive',
//                    [
//                        '___directive' => $directive,
//                        '_escape_params' => false,
//                    ]
//                );
//            }
        }
        return $html;
    }
}
