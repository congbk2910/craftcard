<?php
/**
 * Adminhtml catart list block
 *
 */
namespace Netbaseteam\Onlinedesign\Block\Adminhtml;

class CatTemplate extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_cattemplate';
        $this->_blockGroup = 'Netbaseteam_Onlinedesign';
        $this->_headerText = __('Category Template');
        $this->_addButtonLabel = __('Add New Category Template');
        parent::_construct();
        if ($this->_isAllowedAction('Netbaseteam_Onlinedesign::cattemplate_manage')) {
            $this->buttonList->update('add', 'label', __('Add New Category Template'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
