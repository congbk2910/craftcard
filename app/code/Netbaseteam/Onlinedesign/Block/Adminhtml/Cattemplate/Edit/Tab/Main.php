<?php
namespace Netbaseteam\Onlinedesign\Block\Adminhtml\Cattemplate\Edit\Tab;

use Netbaseteam\Onlinedesign\Model\Cattemplate;

/**
 * Cms page edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface {
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    protected $_resourceModel;
    protected $_helper;
    protected $_collectionCatTemplate;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Netbaseteam\Onlinedesign\Model\Cattemplate $collectionCatTemplate,
        \Netbaseteam\Onlinedesign\Helper\Data $helper,
        \Netbaseteam\Onlinedesign\Model\ResourceModel\Cattemplate $resourceModel,
        array $data = []
    ) {
        $this->_collectionCatTemplate = $collectionCatTemplate;
        $this->_systemStore = $systemStore;
        $this->_helper = $helper;
        $this->_resourceModel = $resourceModel;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm() {
        /* @var $model \Magento\Cms\Model\Page */
        $model = $this->_coreRegistry->registry('cattemplate');
        $parentCategory = $this->_helper->getCustomParentCategory();
        $params = $this->getRequest()->getParams();
        $catId = isset($params['cat_id']) ? $params['cat_id'] : '';
        if ($this->_isAllowedAction('Netbaseteam_Onlinedesign::cattemplate_manage')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('cattemplate_main_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Category Information')]);
        if ($model->getCatId()) {
            $fieldset->addField('cat_id', 'hidden', ['name' => 'cat_id']);
        }
        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true,
                'disabled' => $isElementDisabled,
            ]
        );
        $fieldset->addField(
            'parent_cate_id',
            'select',
            [
                'name' => 'parent_cate_id',
                'label' => __('Parent Category'),
                'title' => __('Parent Category'),
                'values' => $parentCategory
            ]
        );
        $fieldset->addField(
            'template_image',
            'file',
            [
                'name' => 'template_image',
                'label' => __('Category Image'),
                'title' => __('Category Image'),
                'required' => $model->getCatId() ? false : true,
                'after_element_html' => $this->getImageHtml('template_image', $catId)
            ]
        );

        $this->_eventManager->dispatch('adminhtml_cattemplate_edit_tab_main_prepare_form', ['form' => $form]);
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return __('Category Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return __('Category Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab() {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden() {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * @param $field
     * @param $templateID
     * @param $storeId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getImageHtml($field, $catId)
    {

        $templateImg = $this->_resourceModel->getTemplateImage($catId);

        if ($templateImg) {
            $imageUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $templateImg[0]['template_image'];
            if ($templateImg[0]['template_image'] == 1) {
                $imageUrl = $this->_helper->getImageDefaultProcess();
            }
        }

        $html = '';
        if (!empty($templateImg[0]) && isset($templateImg[0]['template_image'])) {
            $html .= '<p style="margin-top: 5px">';
            $html .= '<image style="min-width:100px;max-width:100px;" src="' . $imageUrl . '" />';
            $html .= '<input type="hidden" value="' . $templateImg[0]['template_image'] . '" name="old_' . $field . '"/>';
            $html .= '</p>';
        }

        return $html;
    }
}
