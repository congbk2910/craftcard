<?php

namespace Netbaseteam\Onlinedesign\Block\Adminhtml\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

class ImageShapes extends AbstractRenderer
{
    public function render(\Magento\Framework\DataObject $row)
    {
        $result = '';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get("\Netbaseteam\Onlinedesign\Helper\Data");
        $list = $helper->nbdesigner_read_json_setting($helper->plugin_path_data() . 'shapes.json');
        $shapes_id = $row['shapes_id'];
        $key = array_search($shapes_id, array_column($list['cliparts']['items'], 'id'));
        $shapes_index_found = $key;
        if (isset($list['cliparts']['items'][$shapes_index_found])) {
            $shapes_data = $list['cliparts']['items'][$shapes_index_found];
            if (isset($shapes_data["url"])) {
                $result = '<img src="' . $shapes_data["url"] . '" width="100" />';
            }
        }
        return $result;
    }
}
