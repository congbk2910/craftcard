<?php
namespace Netbaseteam\Onlinedesign\Block\Adminhtml\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

class Links extends AbstractRenderer
{
    public function render(\Magento\Framework\DataObject $row){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product_id = $row->getId();
        $product = $objectManager->create("Magento\Catalog\Model\Product")->load($product_id);
        $frontendUrl = $objectManager->get('\Magento\Framework\UrlInterface');
        $link_admindesign = $frontendUrl->getBaseUrl().'onlinedesign/index/design';
        $link_admindesign .= "?product_id=" . $product_id;
        $helper = $objectManager->get("\Netbaseteam\Onlinedesign\Helper\Data");
        $preDesign = $helper->getPredesignCollection($product_id);
        if(!empty($preDesign)) {
            $link_admindesign .= "&nbd_item_key=" . $preDesign;
        }
        $status = $helper->getStatusDesign($row['entity_id']);
        $create_template = "";
        $linkPreview = $frontendUrl->getBaseUrl().'onlinedesign/index/design?product_id=' . $product_id;
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('nbdesigner_predesign');
        $sql = "SELECT * FROM " . $tableName." where product_id = ".$product_id;
        $ret_temp = $connection->fetchAll($sql);
        if($status) {
            if(sizeof($ret_temp)) {
                $create_template = '
                    <a href="'.$link_admindesign.'&task=predesign&rd=predesign'.'" target="_blank">Update Predesign</a>
                    <span> | </span>
                ';
            } else {
                $create_template = '<a href="'.$link_admindesign.'&task=predesign&rd=predesign'.'" target="_blank">Create Predesign</a>
                    <span> | </span>
                ';
            }
        }
        $result = $create_template.'
            <a href="'.$this->getUrl('onlinedesign/index/edit', ['id' => $product_id]).'">Manage Design</a>
        ';

        return $result;
    }
}
