<?php
/**
 * Adminhtml shapes list block
 *
 */
namespace Netbaseteam\Onlinedesign\Block\Adminhtml;

class Shapes extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_shapes';
        $this->_blockGroup = 'Netbaseteam_Onlinedesign';
        $this->_headerText = __('Shapes');
        $this->_addButtonLabel = __('Add New Shapes');
        parent::_construct();
        if ($this->_isAllowedAction('Netbaseteam_Onlinedesign::shapes_list')) {
            $this->buttonList->update('add', 'label', __('Add New Shapes'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
