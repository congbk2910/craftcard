<?php
namespace Netbaseteam\Onlinedesign\Block\Adminhtml\Shapes\Edit\Tab;

/**
 * Cms page edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    protected $_dataOnlinedesign;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
		\Netbaseteam\Onlinedesign\Helper\Data $dataOnlinedesign,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_dataOnlinedesign = $dataOnlinedesign;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('shapes');
        if ($this->_isAllowedAction('Netbaseteam_Onlinedesign::shapes_list')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('shapes_main_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Shapes Information')]);
        if ($model->getId()) {
            $fieldset->addField('shapes_id', 'hidden', ['name' => 'shapes_id']);
        }
		$helper = $this->_dataOnlinedesign;
		$result = "";
		$list = $helper->nbdesigner_read_json_setting($helper->plugin_path_data().'shapes.json');
		$shapes_id = $this->getRequest()->getParam('shapes_id');
        $shapes_url = "";
        $key = array_search($shapes_id, array_column($list['cliparts']['items'], 'id'));
		$shapes_index_found = $key;
		if (isset($list['cliparts']['items'][$shapes_index_found])) {
			$shapes_data = $list['cliparts']['items'][$shapes_index_found];
			if(isset($shapes_data["url"])) {
				$shapes_url  =  $shapes_data["url"];
				if($model->getId())
				$result = '<img src="'.$shapes_data["url"].'" width="100" />';
			}
		}
		$fieldset->addField(
            'filename',
            'image',
            [
                'name' => 'filename',
                'label' => __('File'),
                'title' => __('File'),
				'note' => 'Allow extensions: .svg',
                'required'  => true
            ]
        );
		
		if($shapes_url) {
			$fieldset->addField('image', 'note', array(
			  'label'     => __('Preview'),
			  'name'      => 'image',
			  'text'     => $result,
			));
		}
		
		$fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        $this->_eventManager->dispatch('adminhtml_shapes_edit_tab_main_prepare_form', ['form' => $form]);
        $form->setValues($model->getData());
        $this->setForm($form);
	
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Shapes Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Shapes Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
