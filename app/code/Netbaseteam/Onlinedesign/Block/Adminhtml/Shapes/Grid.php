<?php
namespace Netbaseteam\Onlinedesign\Block\Adminhtml\Shapes;

/**
 * Adminhtml Onlinedesign grid
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {
	/**
	 * @var \Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\CollectionFactory
	 */
	protected $_collectionFactory;

	/**
	 * @var \Netbaseteam\Onlinedesign\Model\Onlinedesign
	 */
	protected $_onlinedesign;

	/**
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Magento\Backend\Helper\Data $backendHelper
	 * @param \Netbaseteam\Onlinedesign\Model\Onlinedesign $onlinedesignPage
	 * @param \Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\CollectionFactory $collectionFactory
	 * @param array $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Backend\Helper\Data $backendHelper,
		\Netbaseteam\Onlinedesign\Model\Onlinedesign $onlinedesign,
		\Netbaseteam\Onlinedesign\Model\ResourceModel\Shapes\CollectionFactory $collectionFactory,
		array $data = []
	) {
		$this->_collectionFactory = $collectionFactory;
		$this->_onlinedesign = $onlinedesign;
		parent::__construct($context, $backendHelper, $data);
	}

	/**
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();
		$this->setId('shapesGrid');
		$this->setDefaultSort('shapes_id');
		$this->setDefaultDir('DESC');
		$this->setUseAjax(true);
		$this->setSaveParametersInSession(true);
	}

	/**
	 * Prepare collection
	 *
	 * @return \Magento\Backend\Block\Widget\Grid
	 */
	protected function _prepareCollection() {
		$collection = $this->_collectionFactory->create();
		/* @var $collection \Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\Collection */
		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	/**
	 * Prepare columns
	 *
	 * @return \Magento\Backend\Block\Widget\Grid\Extended
	 */
	protected function _prepareColumns() {
	    $this->addColumn(
			'image',
			[
				'header' => __('Image'),
				'index' => 'image',
				'align' => 'center',
				'width' => '250px',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id',
				'filter' => false,
				'sortable' => false,
				'renderer' => '\Netbaseteam\Onlinedesign\Block\Adminhtml\Renderer\ImageShapes',
			]
		);

		$this->addColumn('title', ['header' => __('Title'), 'index' => 'title']);
		$this->addColumn(
			'action',
			[
				'header' => __('Edit'),
				'type' => 'action',
				'align' => 'center',
				'getter' => 'getId',
				'actions' => [
					[
						'caption' => __('Edit'),
						'url' => [
							'base' => '*/*/edit',
							'params' => ['store' => $this->getRequest()->getParam('store')],
						],
						'field' => 'shapes_id',
					],
				],
				'sortable' => false,
				'filter' => false,
				'header_css_class' => 'col-action',
				'column_css_class' => 'col-action',
			]
		);

		return parent::_prepareColumns();
	}

	/**
	 * @return $this
	 */
	protected function _prepareMassaction() {
		$this->setMassactionIdField('shapes_id');
		$this->getMassactionBlock()->setFormFieldName('shapesid');

		$this->getMassactionBlock()->addItem(
			'delete',
			[
				'label' => __('Delete'),
				'url' => $this->getUrl('onlinedesign/*/massDelete'),
				'confirm' => __('Are you sure?'),
			]
		);

		return $this;
	}

	/**
	 * Row click url
	 *
	 * @param \Magento\Framework\Object $row
	 * @return string
	 */
	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', ['shapes_id' => $row->getId()]);
	}

	/**
	 * Get grid url
	 *
	 * @return string
	 */
	public function getGridUrl() {
		return $this->getUrl('*/*/grid', ['_current' => true]);
	}
}
