<?php
/**
 * Adminhtml color list block
 *
 */
namespace Netbaseteam\Onlinedesign\Block\Adminhtml;

class SpecialColor extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_specialcolor';
        $this->_blockGroup = 'Netbaseteam_Onlinedesign';
        $this->_headerText = __('Special Color');
        $this->_addButtonLabel = __('Add New Special Color');
        parent::_construct();
        if ($this->_isAllowedAction('Netbaseteam_Onlinedesign::onlinedesign_color')) {
            $this->buttonList->update('add', 'label', __('Add New Special Color'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
