<?php

namespace Netbaseteam\Onlinedesign\Block;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Netbaseteam\Onlinedesign\Model\Mydesigns;
use Magento\Catalog\Block\Product\ListProduct;

/**
 * Onlinedesign content block
 */
class Mydesign extends \Magento\Framework\View\Element\Template {
	/**
	 * Onlinedesign collection
	 *
	 * @var \Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\Collection
	 */
	protected $_orderCollection = null;

	/**
	 * Onlinedesign factory
	 *
	 * @var \Netbaseteam\Onlinedesign\Model\OnlinedesignFactory
	 */
	protected $_orderCollectionFactory;

	/** @var \Netbaseteam\Onlinedesign\Helper\Data */
	protected $_dataHelper;

	/**
	 * @var \Magento\Customer\Model\Session
	 */
	protected $_customerSessionFactory;

	/**
	 * @var \Netbaseteam\Onlinedesign\Model\OnlinedesignFactory
	 */
	protected $_onlineDesignCollectionFactory;

	/**
	 * @var \Magento\Sales\Model\Order
	 */
	protected $order;

	protected $_mydesignFactory;

	public $listProductBlock;
    protected $_productCollectionFactory;
	/**
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\CollectionFactory $_onlineDesignCollectionFactory
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\CollectionFactory $_onlineDesignCollectionFactory,
		\Magento\Sales\Model\OrderFactory $orderCollectionFactory,
		\Netbaseteam\Onlinedesign\Model\MydesignsFactory $mydesignsFactory,
		\Netbaseteam\Onlinedesign\Helper\Data $dataHelper,
		\Magento\Customer\Model\SessionFactory $customerSessionFactory,
		\Magento\Sales\Model\Order $order,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        ListProduct $listProductBlock,
		array $data = []
	) {
	    $this->_mydesignFactory = $mydesignsFactory;
		$this->_orderCollectionFactory = $orderCollectionFactory;
		$this->_onlineDesignCollectionFactory = $_onlineDesignCollectionFactory;
		$this->_dataHelper = $dataHelper;
		$this->_customerSessionFactory = $customerSessionFactory;
		$this->order = $order;
		parent::__construct(
			$context,
			$data
		);
        $this->listProductBlock = $listProductBlock;
        $this->_productCollectionFactory = $productCollectionFactory;
	}

    public function getProductCollection()
    {
        /** @var $collection Collection */
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->load();
        return $collection;
    }
    public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }

	/**
	 * @return int
	 */
    public function getCustomerId() {
		$customer = $this->_customerSessionFactory->create();
		return $customer->getCustomerId();
	}

	public function filterDesignByCustomer($customerId) {
         return $this->_mydesignFactory->create()->getCollection()->addFieldToFilter('user_id', $customerId);
    }

	/**
	 * Retrieve onlinedesign collection
	 *
	 * @return \Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\Collection
	 */
	protected function _getCollection() {
		$collection = $this->_orderCollectionFactory->create()->getCollection();
		return $collection;
	}

	/**
	 * order collection
	 */
	public function getCollection() {
		$this->_orderCollection = $this->_getCollection()->addAttributeToFilter('customer_id', $this->getCustomerId());
		$this->_orderCollection->setOrder('created_at', 'desc');
		return $this->_orderCollection;
	}

	/**
	 * get collection
	 * @return \Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\Collection
	 */
	public function getProductOnlineDesignId($productId) {
		$collection = $this->_onlineDesignCollectionFactory->create()->addFieldToFilter('product_id', $productId)
			->addFieldToFilter('status', 1);
		return $collection;
	}

	/**
	 * @param $orderId
	 * @return string
	 */
	public function getOrderItems($orderId) {
		$order = $this->order->load($orderId);

		$order->getAllVisibleItems();
		$orderItems = $order->getItemsCollection()->addAttributeToSelect('*')->load();
		return $orderItems;
	}
}
