<?php

namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Cattemplate;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Netbaseteam_Onlinedesign::cattemplate_manage');
    }

    /**
     * Delete action
     *
     * @return void
     */
    public function execute()
    {
        // check if we know what should be deleted
        $ids = $this->getRequest()->getParam('cattemplate');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($ids) {

            try {
                // init model and delete
                foreach ($ids as $id) {
                    $model = $this->_objectManager->create('Netbaseteam\Onlinedesign\Model\Cattemplate');
                    $row = $model->load($id)->getData();
                    $modelTemplate = $this->_objectManager
                        ->create('Netbaseteam\Onlinedesign\Model\Template')->getCollection()
                        ->addFieldToFilter("cat_id", $id);


                    if (count($modelTemplate)) {
                        foreach ($modelTemplate as $m) {
                            $modelTemplatetDel = $this->_objectManager->create('Netbaseteam\Onlinedesign\Model\Template');
                            $template = $modelTemplatetDel->load($m->getId());
                            $template->setCatId('0');
                            $template->save();
                        }
                    }

//                    process delete the parent category
                    $parentCat = $row['parent_cate_id'];
                    $collection = $model->getCollection()->addFieldToFilter('parent_cate_id', $id);
                    if (count($collection)) {
                        foreach ($collection as $value) {
                            $modelCategory = $model->load($value['cat_id']);
                            $modelCategory->setParentCateId($parentCat);
                            $modelCategory->save();
                        }
                    }


                    //  delete image file in the pub/media
                    $fileName = $row['template_image'];
                    $_file = $this->_objectManager->create('Magento\Framework\Filesystem\Driver\File');
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $mediaRootDir = $mediaDirectory->getAbsolutePath();

                    if ($_file->isExists($mediaRootDir . $fileName)) {
                        $_file->deleteFile($mediaRootDir . $fileName);
                    }

                    $model->load($id);
                    $model->delete();
                }
                // display success message
                $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', count($ids)));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['page_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a data to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
