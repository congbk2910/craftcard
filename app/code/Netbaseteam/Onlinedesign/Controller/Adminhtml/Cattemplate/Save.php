<?php

namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Cattemplate;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Backend\App\Action {
    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;
    protected $_fileSystem;
    protected $_storeManager;
    protected $_fileUploaderFactory;
    /**
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     */
    public function __construct(
        Action\Context $context,
        PostDataProcessor $dataProcessor,
        Filesystem $fileSystem,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory) {
        $this->dataProcessor = $dataProcessor;
        $this->_fileSystem = $fileSystem;
        $this->_storeManager = $storeManager;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Netbaseteam_Onlinedesign::cattemplate_manage');
    }

    /**
     * Save action
     *
     * @return void
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();
        $image = $this->getRequest()->getFiles();
        if ($data) {
            $data = $this->dataProcessor->filter($data);
            $model = $this->_objectManager->create('Netbaseteam\Onlinedesign\Model\Cattemplate');
            $id = $this->getRequest()->getParam('cat_id');
            if ($id) {
                $model->load($id);
            }
            $data_actual['title'] = $data['title'];
            $data_actual['parent_cate_id'] = isset($data['parent_cate_id']) ? $data['parent_cate_id'] : '' ;
            /**save design_image**/
            $path = $this->_fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('nbdesigner/template_image/');
            $templateImage = '';
            if (isset($image['template_image'])) {
                $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'nbdesigner/template_image/';
                if (isset($data['template_image']['delete'])) {
                    $templateImage = '';
                } else if ($image['template_image']['name'] != '') {
                    /*save image*/
                    try {
                        $uploader = $this->_fileUploaderFactory->create(['fileId' => $image['template_image']]);
                        $fileType = ['jpg', 'jpeg', 'gif', 'png'];
                        $uploader->setAllowedExtensions($fileType);
                        $uploader->setAllowRenameFiles(true);
                        $uploader->save($path);
                        $fileName = $uploader->getUploadedFileName();
                        $templateImage = 'nbdesigner/template_image/' . $fileName;
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());
                        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                        return $resultRedirect;
                    }
                } else {
                    $templateImage = (isset($data['old_template_image'])) ? $data['old_template_image'] : '';
                }
            } else {
                $templateImage = (isset($data['old_template_image'])) ? $data['old_template_image'] : '';
            }
            $data_actual['template_image'] = $templateImage;
            $model->addData($data_actual);
            if (!$this->dataProcessor->validate($data)) {
                $this->_redirect('*/*/edit', ['cat_id' => $model->getId(), '_current' => true]);
                return;
            }

            try {
                $model->save();

                /************************************************/

                $this->messageManager->addSuccess(__('The Data has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['cat_id' => $model->getId(), '_current' => true]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', ['cat_id' => $this->getRequest()->getParam('cat_id')]);
            return;
        }
        $this->_redirect('*/*/');
    }
}
