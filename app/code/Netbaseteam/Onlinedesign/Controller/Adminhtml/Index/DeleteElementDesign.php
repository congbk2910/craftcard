<?php

namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\View\Result\PageFactory;
use Netbaseteam\Onlinedesign\Helper\Data;

class DeleteElementDesign extends Action
{
    const MEDIA_PATH = 'nbdesigner';
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_helper;
    protected $_template;
    protected $_resultJsonFactory;
    protected $_preDesign;
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Filesystem $filesystem,
        Data $helper,
        JsonFactory $resultJsonFactory,
        \Netbaseteam\Onlinedesign\Model\Predesign $preDesign,
        PageFactory $resultPageFactory
    ) {
        $this->_preDesign = $preDesign;
        $this->filesystem = $filesystem;
        $this->_helper = $helper;
        $this->resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $pid = $this->getRequest()->getParam('product_id');
        $dataIndex = $this->getRequest()->getParam('data_index');
        $folderDesign = $this->getPredesignCollection($pid);
        if($folderDesign !== null) {
        $baseDir = $this->getBaseDir();
        $path = $baseDir . '/designs/' .$folderDesign;
            if (file_exists($path)) {
                $jsonFile = $path . '/design.json';
                $content = file_get_contents($jsonFile);
                $data = json_decode($content, true);
                $data = array($data);
                $data[0]['frame_' . $dataIndex]['objects'] = "";
                $data = $data[0];
                file_put_contents($jsonFile, json_encode($data));
            }
        }
    }
    public function getPredesignCollection($pid) {
        $folder = null;
        $preDesignCollection = $this->_preDesign->getCollection()->addFieldToFilter('product_id', $pid);
        foreach ($preDesignCollection as $data) {
            $folder = $data->getFolder();
        }
        return $folder;
    }
    public function getBaseDir()
    {
        $path = $this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(self::MEDIA_PATH);
        return $path;
    }
}
