<?php

namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\PageFactory;

class DeleteListImages extends Action
{
    const MEDIA_PATH = 'nbdesigner';
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_helper;
    protected $_template;
    protected $_resultJsonFactory;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Netbaseteam\Onlinedesign\Helper\Data $helper,
        JsonFactory $resultJsonFactory,
        PageFactory $resultPageFactory
    ) {
        $this->filesystem = $filesystem;
        $this->_helper = $helper;
        $this->resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $pid = $this->getRequest()->getParam('id');
        $path = $this->getPathProductImages() . $pid;
        $response = array();
        if(file_exists($path)) {
            $all_files = glob($path . "/*.*");
            if(count($all_files) > 0) {
                for ($i = 0; $i < count($all_files); $i++)
                {
                    $image_name = $all_files[$i];
                    $supported_format = array('jpg', 'jpeg', 'png', 'svg');
                    $ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
                    if (in_array($ext, $supported_format)) {
                        @unlink($image_name);
                        $response['mes'] = "successfully";
                    } else {
                        continue;
                    }
                }
            }
        }
        return $result->setData($response);
    }
    public function getPathProductImages() {
        return $this->getBaseDir() . '/productimages/';
    }
    public function getBaseDir()
    {
        return $this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath(self::MEDIA_PATH);
    }
}
