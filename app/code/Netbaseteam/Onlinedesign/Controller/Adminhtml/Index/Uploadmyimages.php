<?php
namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Index;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;

class Uploadmyimages extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{
	/**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_helper;
	protected $_resultJsonFactory;

	/**
     * @param \Magento\Framework\App\Action\Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Netbaseteam\Onlinedesign\Helper\Data $helper,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->_resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
        parent::__construct($context);
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    /**
     * Default Onlinedesign Index page
     *
     * @return void
     */
    public function execute()
    {
		$response = array();
		$response['mes'] = '';

		$helper =  $this->_helper;
        $pid = $this->getRequest()->getParam('pid');
        if(!$pid) {
            $pid = $this->getRequest()->getParam('id');
        }
		$product_id_path = $helper->getBaseDir().'/productimages/'.$pid;
		if(!is_dir($product_id_path)){
			mkdir($product_id_path, 0777, true);
		}

		$uploader = $this->_objectManager->create(
		  'Magento\MediaStorage\Model\File\Uploader',
		  ['fileId' => 'file']
		);
        $file = $uploader->validateFile();
        $filetype = array('jpeg', 'jpg', 'png', 'PNG', 'JPEG', 'JPG');
        $name = time() . urlencode($file["name"]);
        if(strpos($name, '%2') !== 'false') {
            $name = str_replace('%2', '+', $name);
        }
        $path = $product_id_path . "/" . $name;
        $file_ext = pathinfo($name, PATHINFO_EXTENSION);
        if (in_array(strtolower($file_ext), $filetype)) {
            if ($file["size"] < 1000000) {
                @move_uploaded_file($file['tmp_name'], $path);
                $response['mes'] = $name;
            } else {
                $response['mes'] = "FILE_SIZE_ERROR";
            }
        } else {
            $response['mes'] = "FILE_TYPE_ERROR";
            $response['file_type'] = "not accept";
        }
        $result = $this->_resultJsonFactory->create();
        return $result->setData($response);
    }
}
