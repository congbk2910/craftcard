<?php

namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Order\Create;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Helper\Reorder as ReorderHelper;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Reorder\UnavailableProductsProvider;

class Reorder extends \Magento\Sales\Controller\Adminhtml\Order\Create\Reorder
{

    /**
     * @var UnavailableProductsProvider
     */
    private $unavailableProductsProvider;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ReorderHelper
     */
    private $reorderHelper;

    public function __construct(
        Action\Context $context,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Framework\Escaper $escaper,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        UnavailableProductsProvider $unavailableProductsProvider, OrderRepositoryInterface $orderRepository, ReorderHelper $reorderHelper)
    {
        $this->unavailableProductsProvider = $unavailableProductsProvider;
        $this->orderRepository = $orderRepository;
        $this->reorderHelper = $reorderHelper;
        parent::__construct($context, $productHelper, $escaper, $resultPageFactory, $resultForwardFactory, $unavailableProductsProvider, $orderRepository, $reorderHelper);
    }

    public function execute()
    {
        $this->_getSession()->clearStorage();
        $orderId = $this->getRequest()->getParam('order_id');
        /** @var Order $order */
        $order = $this->orderRepository->get($orderId);
        if (!$this->reorderHelper->canReorder($order->getEntityId())) {
            return $this->resultForwardFactory->create()->forward('noroute');
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$order->getId()) {
            $resultRedirect->setPath('sales/order/');
            return $resultRedirect;
        }

        $unavailableProducts = $this->unavailableProductsProvider->getForOrder($order);
        if (count($unavailableProducts) > 0) {
            foreach ($unavailableProducts as $sku) {
                $this->messageManager->addErrorMessage(
                    sprintf('Product "%s" not found. This product is no longer available.', $sku)
                );
            }
            $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
        } else {
            $order->setReordered(true);
            $this->_getSession()->setUseOldShippingMethod(true);
            $this->_getOrderCreateModel()->initFromOrder($order);
            $resultRedirect->setPath('sales/order_create?order_id='.$orderId);
        }
        return $resultRedirect;
    }
}