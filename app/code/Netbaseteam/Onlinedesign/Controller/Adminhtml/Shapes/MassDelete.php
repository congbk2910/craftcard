<?php

namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Shapes;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Netbaseteam\Onlinedesign\Model\ResourceModel\Shapes\CollectionFactory;

class MassDelete extends \Magento\Backend\App\Action {
	/**
	 * @var Filter
	 */
	protected $filter;

/**
 * @var CollectionFactory
 */
	protected $collectionFactory;

	protected $_shapes;

	protected $_helper;

/**
 * @param Context $context
 * @param Filter $filter
 * @param CollectionFactory $collectionFactory
 */
	public function __construct(
		Context $context,
		Filter $filter,
		CollectionFactory $collectionFactory,
		\Netbaseteam\Onlinedesign\Helper\Data $helper,
		\Netbaseteam\Onlinedesign\Model\Shapes $shapes
	) {
		$this->filter = $filter;
		$this->collectionFactory = $collectionFactory;
		$this->_helper = $helper;
		$this->_shapes = $shapes;
		parent::__construct($context);
	}

	/**
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
	public function execute() {
		$shapesIds = $this->getRequest()->getParam('shapesid');
		if (!is_array($shapesIds) || empty($shapesIds)) {
			$this->messageManager->addError(__('Please select shapes(s).'));
		} else {
			try {
                $count = 0;
				foreach ($shapesIds as $shapeId) {
					$shapes = $this->_shapes->load($shapeId);
                    $shapes->delete();
					$this->nbdesigner_delete_art($shapeId);
                    $count++;
				}
				$this->messageManager->addSuccess(
					__('A total of %1 record(s) have been deleted.', $count)
				);
			} catch (\Exception $e) {
				$this->messageManager->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/');
	}

	public function nbdesigner_delete_art($id) {
	  //  var_dump($id); die;
		$helper = $this->_objectManager->get('Netbaseteam\Onlinedesign\Helper\Data');
		$path = $helper->plugin_path_data() . 'shapes.json';
		$list = $helper->nbdesigner_read_json_setting($path);
        $key = array_search($id, array_column($list['cliparts']['items'], 'id'));
		$id_found = $key;
		$file_art = $list['cliparts']['items'][$id_found]["file"];
		try {
			unlink($file_art);
		} catch (\Exception $e) {

        }
		$helper->nbdesigner_delete_shapes_json_setting($path, $id);
	}
}