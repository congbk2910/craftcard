<?php

namespace Netbaseteam\Onlinedesign\Controller\Adminhtml\Shapes;

use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\TypeListInterface;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * File Uploader factory
     *
     * @var \Magento\Core\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;
    protected $_storeManager;
    /**
     * @param Action\Context $context
     * @param PostDataProcessor $dataProcessor
     */
    public function __construct(
        Action\Context $context,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManager $storeManager,
        PostDataProcessor $dataProcessor,
        TypeListInterface $cacheTypeList
    )
    {
        $this->dataProcessor = $dataProcessor;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->cacheTypeList = $cacheTypeList;
        parent::__construct($context);
        $this->_storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Netbaseteam_Onlinedesign::shapes_list');
    }

    /**
     * Save action
     *
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('shapes_id');
            try {
                $uploader = $this->_objectManager->create(
                    'Magento\MediaStorage\Model\File\Uploader',
                    ['fileId' => 'filename']
                );
                $file = $uploader->validateFile();
            } catch (\Exception $e) {
                if(!$id) {
                    $this->messageManager->addError(__("Please choose an shapes image"));
                    $this->_redirect('*/*/new');
                    return;
                }
            }
            if(isset($file)) {
                $fName = explode(".", $file["name"]);
                if($data["title"] == "") {
                    $data["title"] = trim($fName[0]);
                }
            }
            $data = $this->dataProcessor->filter($data);
            $model = $this->_objectManager->create('Netbaseteam\Onlinedesign\Model\Shapes');
            if ($id) {
                $model->load($id);
            }
            if (isset($data['filename'])) {
                $imageData = $data['filename'];
            } else {
                $imageData = array();
            }
            unset($data['filename']);
            $model->addData($data);
            if (!$this->dataProcessor->validate($data)) {
                $this->_redirect('*/*/edit', ['shapes_id' => $model->getId(), '_current' => true]);
                return;
            }
            try {
                $helper = $this->_objectManager->get('Netbaseteam\Onlinedesign\Helper\Data');
                if (isset($imageData['delete']) && $model->getFilename()) {
                    $helper->removeImage($model->getFilename());
                    $model->setFilename(null);
                }
                $model->save();
                $shapes_id = 0;
                $update = false;
                $list = $helper->nbdesigner_read_json_setting($helper->plugin_path_data().'shapes.json');
                if ($id) {
                    $shapes_id = $model->getId();
                    $shapes_index_found = $helper->indexFound($shapes_id, $list, "id");
                    $update = true;
                    if (isset($list[$shapes_index_found])) {
                        $shapes_data = $list[$shapes_index_found];
                    }
                }
                $shapes = array();
                $shapes['name'] = $data["title"];
                $shapes['id'] = $model->getId();
                if(isset($file["name"]) && $file["name"] != '' && $file['size']) {
                    $path1 = $helper->getBaseDir().'/shapes/';
                    if(!is_dir($path1)){
                        mkdir($path1, 0777, TRUE);
                    }
                    $year	= strftime("%Y");
                    $path2 = $path1."/".$year;
                    if(!is_dir($path2)){
                        mkdir($path2, 0777);
                    }
                    $month 	= strftime("%m");
                    $upload_path = $path2."/".$month;
                    if(!is_dir($upload_path)){
                        mkdir($upload_path, 0777);
                    }

                    /* Starting upload */
                    $uploader = $this->_fileUploaderFactory->create(['fileId' => 'filename']);
                    $uploader->setAllowedExtensions(['svg', 'png', 'jpg', 'jpeg']);
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $uploader->setAllowCreateFolders(true);
                    $uploader->save($upload_path);
                    $fileName = $uploader->getUploadedFileName();
                    $shapes['file'] = $this->_storeManager->getStore()->getBaseUrl() . 'pub/media/nbdesigner/shapes'. "/".$year."/".$month."/".$fileName;
                    $shapes['url'] = $this->_storeManager->getStore()->getBaseUrl() . 'pub/media/nbdesigner/shapes'. "/".$year."/".$month."/".$fileName;
                } else {
                    $shapes_id = $model->getId();
                    $shapes_index_found = $helper->indexFound($shapes_id, $list, "id");
                    if (isset($list[$shapes_index_found])) {
                        $shapes_data = $list[$shapes_index_found];
                        $shapes['file'] = $shapes_data["file"];
                        $shapes['url'] = $shapes_data["url"];
                    }
                }

                if ($update) {
                    $helper->nbdesigner_update_list_shapes($shapes, $shapes_id, "update");
                } else {
                    $shapes_id = $model->getId();
                    $helper->nbdesigner_update_list_shapes($shapes, $shapes_id, "add");
                }
                $this->cacheTypeList->cleanType('full_page');
                $this->messageManager->addSuccess(__('The Data has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['shapes_id' => $model->getId(), '_current' => true]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', ['shapes_id' => $this->getRequest()->getParam('shapes_id')]);
            return;
        }
        $this->_redirect('*/*/');
    }
}
