<?php
namespace Netbaseteam\Onlinedesign\Controller\Index;

class CheckNbDesign extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_storeManager;
    protected $_resultJsonFactory;
    protected $_helper;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Netbaseteam\Onlinedesign\Helper\Data $data,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_helper = $data;
        $this->_storeManager = $storeManager;
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $pid = $this->getRequest()->getParam('pid');
        $response = array();
        $designSettings = $this->_helper->_getOnlineDesignByProduct($pid);
        $predesignFolder = $this->_helper->getPredesignCollection($pid);
        foreach ($designSettings as $data) {
            $response['design'] = $data->getContentDesign();
        }
        if (!isset($response['design']) || $response['design'] == "" ) {
            $response['design'] = 0;
            $response['nbd_item_key'] = $predesignFolder;
        } else {
            $response['design'] = 1;
            $response['nbd_item_key'] = $predesignFolder;
        }
        return $result->setData($response);
    }
}
