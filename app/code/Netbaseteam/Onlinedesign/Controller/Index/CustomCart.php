<?php

namespace Netbaseteam\Onlinedesign\Controller\Index;

use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class CustomCart extends \Magento\Framework\App\Action\Action
{
    /**
     * Result page factory
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory;
     */
    protected $_resultJsonFactory;
    protected $formKey;
    protected $cart;
    protected $product;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        FormKey $formKey,
        Cart $cart,
        Product $product
    )
    {
        parent::__construct($context);
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->product = $product;
    }
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();
        $response = array(
            'status'=>'error',
            'message'=>'Error, Please try again'
        );
        $params = $this->getRequest()->getParams();
        $productId = isset($params['product']) ? (int)$params['product'] : 0;
        if($productId > 0)
        {
            $dataDesign = [];
            try {
                $params['form_key'] = $this->formKey->getFormKey();
                $_product = $this->product->load($productId);
                $this->cart->addProduct($_product, $params);
                $this->cart->save();
                $dataDesign['flag'] = true;
                return $resultJson->setData($dataDesign);
            }
            catch(\Exception $e) {
                $response['message'] = $e->getMessage();
            }
        }
        return $resultJson->setData($response);
    }
}