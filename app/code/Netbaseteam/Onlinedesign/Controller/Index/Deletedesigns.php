<?php

namespace Netbaseteam\Onlinedesign\Controller\Index;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Cache\TypeListInterface;
class Deletedesigns extends \Magento\Framework\App\Action\Action
{

    protected $_resourceConnection;
    protected $_pageFactory;
    protected $_messageManager;
    protected $cacheTypeList;
    public function __construct(
        TypeListInterface $cacheTypeList,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\App\Action\Context $context
    )
    {
        $this->cacheTypeList = $cacheTypeList;
        $this->_messageManager = $messageManager;
        $this->_pageFactory = $pageFactory;
        $this->_resourceConnection = $resourceConnection;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $folderId = $this->getRequest()->getParam('folder');
        if(!empty($folderId)) {
            $connection = $this->_resourceConnection->getConnection();
            $themeTable = $this->_resourceConnection->getTableName('nbdesigner_mydesigns');
            $sql = "DELETE FROM $themeTable WHERE folder='$folderId'";
            $connection->query($sql);
            $this->cacheTypeList->cleanType('full_page');
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            $this->_messageManager->addSuccess('Delete Preview Design Successfully');
        }
        return $resultRedirect;
    }
}