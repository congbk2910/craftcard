<?php

namespace Netbaseteam\Onlinedesign\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Netbaseteam\Onlinedesign\Model\ResourceModel\Onlinedesign\Collection;
use \Magento\Framework\Registry;

class Design extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_helper;
    protected $_layout;
    protected $registry;
    protected $_configurable;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Registry $registry,
        \Magento\Framework\App\Action\Context $context,
        \Netbaseteam\Onlinedesign\Helper\Data $helper,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Framework\View\LayoutInterface $layout,
        PageFactory $resultPageFactory
    ) {
        $this->_configurable = $configurable;
        $this->registry = $registry;
        $this->resultPageFactory = $resultPageFactory;
        $this->_helper = $helper;
        $this->_layout = $layout;
        parent::__construct($context);
    }

    /**
     * Default Onlinedesign Index page
     *
     * @return void
     */
    public function execute()
    {
        require_once $this->_helper->getLibOnlineDesign() . '/Onlinedesign/includes/class-qrcode.php';
        require_once $this->_helper->getLibOnlineDesign() . '/Onlinedesign/includes/class.nbdesigner.php';
        require_once $this->_helper->getLibOnlineDesign() . '/Onlinedesign/includes/class.helper.php';
        require_once $this->_helper->getLibOnlineDesign() . '/Onlinedesign/includes/class-util.php';
        $productId = $this->getRequest()->getParam('product_id');
        $taskParam = $this->getRequest()->getParam('task');
        $blockDesign = $this->_view->getLayout()->createBlock('Netbaseteam\Onlinedesign\Block\Onlinedesign');
        $statusUploadDesign = (int)$blockDesign->getStatusUploadDesign($productId);
        $childItem = $this->_configurable->getParentIdsByChild($productId);
        if(!empty($childItem)) {
            $statusUploadDesign = 0;
        }
        if ($statusUploadDesign == 0 || $statusUploadDesign == null || $taskParam == 'preview' || $taskParam == 'predesign' || $taskParam == 'edit_design') {
            $this->_view->loadLayout();
            $this->getResponse()->setBody(
                $this->_layout->createBlock('Netbaseteam\Onlinedesign\Block\Onlinedesign')->setTemplate('Netbaseteam_Onlinedesign::nbdesigner-frontend-template.phtml')->toHtml()
            );
        } elseif ($statusUploadDesign && $this->getRequest()->getParam('noupload')){
            $this->_view->loadLayout();
            $this->getResponse()->setBody(
                $this->_layout->createBlock('Netbaseteam\Onlinedesign\Block\Onlinedesign')->setTemplate('Netbaseteam_Onlinedesign::nbdesigner-frontend-template.phtml')->toHtml()
            );
        } else {
            $this->_view->loadLayout();
            $this->getResponse()->setBody(
                $this->_layout->createBlock('Netbaseteam\Onlinedesign\Block\Onlinedesign')->setTemplate('Netbaseteam_Onlinedesign::sections.phtml')->toHtml()
            );
        }
    }
}
