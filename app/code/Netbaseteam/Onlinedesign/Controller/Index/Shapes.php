<?php
namespace Netbaseteam\Onlinedesign\Controller\Index;

class Shapes extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_storeManager;
    protected $_resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_storeManager = $storeManager;
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $file_data = $this->getRequest()->getParam('get_svg');
        $json_file = [];
        $json_file['data'] = file_get_contents($file_data, true);
        return $result->setData($json_file);
    }
}