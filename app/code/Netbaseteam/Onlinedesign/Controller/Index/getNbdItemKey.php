<?php
namespace Netbaseteam\Onlinedesign\Controller\Index;

class getNbdItemKey extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_storeManager;
    protected $_resultJsonFactory;
    protected $_helper;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Netbaseteam\Onlinedesign\Helper\Data $data,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_helper = $data;
        $this->_storeManager = $storeManager;
        $this->_pageFactory = $pageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $pid = $this->getRequest()->getParam('pid');
        $response = array();
        $preDesign = $this->_helper->getPredesignCollection($pid);
        if (!empty($preDesign)) {
            $response['folder'] = $preDesign;
        } else {
            $response['folder'] ='undefined';
        }
        return $result->setData($response);
    }
}
