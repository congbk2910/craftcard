<?php

namespace Netbaseteam\Onlinedesign\Helper;
use Magento\Framework\App\Cache\TypeListInterface;
use Netbaseteam\Onlinedesign\Model\MydesignsFactory;
class Integrate extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_dataConfig;
    protected $_dataData;
    protected $_checkoutSession;
    protected $_quoteFactory;
    public $_storeManager;
    public $mydesignFactory;
    protected $_customerSession;
    protected $_cacheTypeList;
    protected $_quoteItemFactory;
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Netbaseteam\Onlinedesign\Helper\Config $dataConfig,
        MydesignsFactory  $mydesignFactory,
        TypeListInterface $cacheTypeList,
        \Magento\Customer\Model\Session $session,
        \Netbaseteam\Onlinedesign\Helper\Data $dataData
    )
    {
        $this->_quoteItemFactory = $quoteItemFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_customerSession = $session;
        $this->mydesignFactory = $mydesignFactory;
        $this->_storeManager=$storeManager;
        $this->_dataConfig = $dataConfig;
        $this->_dataData = $dataData;
        $this->_quoteFactory = $quoteFactory;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function getUploadCustomer($_item)
    {
        if ($this->_dataConfig->isEnableModule()) {
            $itemId = $_item->getId();
            $session = $this->_checkoutSession;
            $quote_id = $session->getQuoteId();
            $quote_item_id = $_item->getId();
//            $quote = $this->_quoteItemFactory->create()->getCollection()->addFieldToFilter('item_id', $itemId);
//            $quote->getAllVisibleItems();
            $quoteItems = $this->_quoteItemFactory->create()->getCollection()->addFieldToFilter('item_id', $itemId);
            foreach ($quoteItems as $qItem) {
                if ($qItem->getId() == $quote_item_id && $qItem->getSessionFile() != "") {
                    $sessionFile = $qItem->getSessionFile();
                    $replaceFileName = str_replace($qItem->getProductId() . '/', '', $sessionFile);
                    $convertPid = explode('/', $replaceFileName);
                    if(isset($convertPid[0]) && $convertPid) {
                        if(isset($convertPid[1])) {
                            $replaceFileName = str_replace($convertPid[0] . '/', $qItem->getProductId() . '/' . $convertPid[1] . '/', $sessionFile);
                        }
                    }

                    $output_dir = $this->_dataData->getBaseUploadDir() . "/"  . $qItem->getProductId() . '/' . $replaceFileName . '/' . $replaceFileName;
                    $jsonFile = $output_dir . '.json';
                    if (file_exists($jsonFile)) {
                        $str = file_get_contents($jsonFile);
                        $rows = json_decode($str, true);
                        $html = $this->_dataData->getTitleUpload() . '<br /><div>';
                        for ($i = 0; $i < count($rows); $i++) {
                            foreach ($rows[$i] as $row) {
                                $ext = strtolower($this->_dataData->nbdesigner_get_extension($row['file']));
                                $replaceFileNameUpload = str_replace($qItem->getProductId() . '/', '', $row['file']);
                                $convertData = str_replace($convertPid[0] . '/', $qItem->getProductId() . '/', $sessionFile);
                                $dataFile = $this->_dataData->getBaseUrlUpload() . '/' . $convertData . '/' . $replaceFileNameUpload;
                                if ($ext == 'psd' || $ext == 'pdf' || $ext == 'ai' || $ext == 'eps' || $ext == 'zip' || $ext == 'svg') {
                                    $checkFiles = $this->_dataData->get_thumb_file($ext, $dataFile);
                                    $html .= '<a target="_blank" href="' . $dataFile . '"> <img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $checkFiles . '"/> </a>';
                                } else {
                                    $html .= '<a target="_blank" href="' . $dataFile . '"> <img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $dataFile . '"/> </a>';
                                }

                            }
                        }
                        return $html;
                    }
                }
            }
        }
    }

    public function getUploadOrder($_item)
    {
        $objectManagerr = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManagerr->create('\Magento\Sales\Model\Order')->load($_item->getOrderId());
        $order->getAllVisibleItems();
        $orderItems = $order->getItemsCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', array('eq' => $_item->getSku()))
            ->load();
        foreach ($orderItems as $qItem) {
            if ($qItem->getSessionFile() != "") {
                $sessionFile = $qItem->getSessionFile();
                $replaceFileName = str_replace($qItem->getProductId() . '/', '', $sessionFile);
                $convertPid = explode('/', $replaceFileName);
                if(isset($convertPid[0]) && $convertPid) {
                    if(isset($convertPid[1])) {
                        $replaceFileName = str_replace($convertPid[0] . '/', $qItem->getProductId() . '/' . $convertPid[1] . '/', $sessionFile);
                    }
                }
                $output_dir = $this->_dataData->getBaseUploadDir() . "/"  . $qItem->getProductId() . '/' . $replaceFileName . '/' . $replaceFileName;
                $jsonFile = $output_dir . '.json';
                if (file_exists($jsonFile)) {
                    $str = file_get_contents($jsonFile);
                    $rows = json_decode($str, true);
                    $html = $this->_dataData->getTitleUpload() . '<br /><div>';
                    for ($i = 0; $i < count($rows); $i++) {
                        foreach ($rows[$i] as $row) {
                            $ext = strtolower($this->_dataData->nbdesigner_get_extension($row['file']));
                            $replaceFileNameUpload = str_replace($qItem->getProductId() . '/', '', $row['file']);
                            $convertData = str_replace($convertPid[0] . '/', $qItem->getProductId() . '/', $sessionFile);
                            $dataFile = $this->_dataData->getBaseUrlUpload() . '/' . $convertData . '/' . $replaceFileNameUpload;
                            if ($ext == 'psd' || $ext == 'pdf' || $ext == 'ai' || $ext == 'eps' || $ext == 'zip' || $ext == 'svg') {
                                $checkFiles = $this->_dataData->get_thumb_file($ext, $dataFile);
                                $html .= '<a target="_blank" href="' . $dataFile . '"> <img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $checkFiles . '"/> </a>';
                            } else {
                                $html .= '<a target="_blank" href="' . $dataFile . '"> <img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $dataFile . '"/> </a>';
                            }

                        }
                    }
                    return $html;
                }
            }
        }
    }
    public function getUploadOrderEmail($orderId, $sku)
    {
        $objectManagerr = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManagerr->create('\Magento\Sales\Model\Order')->load($orderId);
        $order->getAllVisibleItems();
        $orderItems = $order->getItemsCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', array('eq' => $sku))
            ->load();
        foreach ($orderItems as $qItem) {
            if ($qItem->getSessionFile() != "") {
                $sessionFile = $qItem->getSessionFile();
                $replaceFileName = str_replace($qItem->getProductId() . '/', '', $sessionFile);
                $output_dir = $this->_dataData->getBaseUploadDir() . "/" . $sessionFile . '/' . $replaceFileName;
                $jsonFile = $output_dir . '.json';
                if (file_exists($jsonFile)) {
                    $str = file_get_contents($jsonFile);
                    $rows = json_decode($str, true);
                    $html = $this->_dataData->getTitleUpload() . '<br /><div>';
                    for ($i = 0; $i < count($rows); $i++) {
                        foreach ($rows[$i] as $row) {
                            $ext = strtolower($this->_dataData->nbdesigner_get_extension($row['file']));
                            $replaceFileNameUpload = str_replace($qItem->getProductId() . '/', '', $row['file']);
                            $dataFile = $this->_dataData->getBaseUrlUpload() . '/' . $sessionFile . '/' . $replaceFileNameUpload;
                            if ($ext == 'psd' || $ext == 'pdf' || $ext == 'ai' || $ext == 'eps' || $ext == 'zip' || $ext == 'svg') {
                                $checkFiles = $this->_dataData->get_thumb_file($ext, $dataFile);
                                $html .= '<a target="_blank" href="' . $dataFile . '"> <img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $checkFiles . '"/> </a>';
                            } else {
                                $html .= '<a target="_blank" href="' . $dataFile . '"> <img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $dataFile . '"/> </a>';
                            }

                        }
                    }
                    return $html;
                }
            }
        }
    }

    public function renderDesignSaveForLate($pid) {
        $folderDesign = null;
        if ($this->_dataConfig->isEnableModule()) {
            if ($this->_customerSession->isLoggedIn()) {
                $folderDesign = '';
                $data_design = $this->mydesignFactory->create()->getCollection()->addFieldToFilter('product_id', $pid);
                foreach ($data_design as $item) {
                    $folderDesign = $item->getFolder();
                }
            }
        }
        return $folderDesign;
    }

    public function showVirtualProductDesign($_item)
    {
        /* online product design */
        $data_design = "";
        $html = "";
        if ($this->_dataConfig->isEnableModule()) {
            $session = $this->_checkoutSession;
            $quote_id = $session->getQuoteId();
            $product = $_item->getProduct();
            $quote_item_id = $_item->getId();
            if($product->getTypeId() == 'configurable' && $_item->getNbdesignerJson() == "") {
                $quote_item_id = $_item->getOptionByCode('simple_product')->getProduct()->getId();
            }
            $quote = $this->_quoteFactory->create()->load($quote_id);
            $quote->getAllVisibleItems();
            $quoteItems = $quote->getItemsCollection();
            $data_folder = null;
            foreach ($quoteItems as $qItem) {
                $productId = $qItem->getId();
                if($product->getTypeId() == 'configurable' && $_item->getNbdesignerJson() == "") {
                    $productId = $qItem->getProductId();
                }
                if ($productId == $quote_item_id && $qItem->getNbdesignerJson() != "") {
                    $data_design = $qItem->getNbdesignerJson();
                }
            }

            $list = json_decode($data_design);
            if ($data_design != "") {
                if ($list) {
                    $html = $this->_dataData->getTilteList() . '<br /><div class="nb-design-cart">';
                    $iData = $_item->getProduct();
                    foreach ($list as $img) {
                        $url = $this->convert_path_to_url($img);
                        //fix bug image not background
                        $src = str_replace('thumbs', 'preview', $url);
//                        $src = str_replace('/preview', '', $url);
                        $realtime = $src . '?t=' . time();
                        $html .= '<img class="nb-image-item" width="200" height="200" style="border-radius: 3px; border: 1px solid #ddd; display: inline-block;" src="' . $realtime . '"/>';
                    }
                    $html .= '</div>';
                }
            }
        }
        return $html;
    }
    public function renderButtonEditDesign($_item)
    {
        $html = "";
        $visualLayout = $this->_dataData->getDesignLayout($_item->getProduct()->getProductId());
        if ($visualLayout == 1) {
            $linkEditDesign = $_item->getProduct()->getProductUrl();
            $linkEditDesign .= "?nbdv-task=edit-checkout&product_id=" . $_item->getProduct()->getProductId();
            $linkEditDesign .= '&nbd_item_key=' . $this->getNbSessionId($_item) . '&md=co';
            $textEdit = __('Edit Design');
            $html .= '<a class="button nbd-edit-design" href="' . $linkEditDesign . '"> ' . $textEdit . ' </a>';
        } else {
            if ($this->getNbSessionId($_item)) {
                $linkEditDesign = $this->_storeManager->getStore()->getBaseUrl() . 'onlinedesign/index/design';
                $linkEditDesign .= "?task=edit&product_id=" . $_item->getProductId();
                $linkEditDesign .= '&nbd_item_key=' . $this->getNbSessionId($_item) . '&md=co';
                $textEdit = __('Edit Design');
                $html .= '<a class="button nbd-edit-design" href="' . $linkEditDesign . '"> ' . $textEdit . ' </a>';
            }
        }
        return $html;
    }
    public function convert_path_to_url($path)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $path_dir = explode('media', $path);
        return $baseUrl . 'pub/media/' . $path_dir[1];
    }

    public function getNbSessionId($_item){
        $session = $this->_checkoutSession;
        $quote_id = $session->getQuoteId();
        $quote_item_id = $_item->getId();
        $product = $_item->getProduct();
        if($product->getTypeId() == 'configurable' && $_item->getNbdesignerJson() == "") {
            $quote_item_id = $_item->getOptionByCode('simple_product')->getProduct()->getId();
        }
        $quote = $this->_quoteFactory->create()->load($quote_id);
        $quote->getAllVisibleItems();
        $quoteItems = $quote->getItemsCollection();
        foreach ($quoteItems as $qItem) {
            $productId = $qItem->getId();
            if($product->getTypeId() == 'configurable' && $_item->getNbdesignerJson() == "") {
                $productId = $qItem->getProductId();
            }
            if ($productId == $quote_item_id && $qItem->getNbdesignerJson() != "" || $qItem->getNbdesignerSession() && !$qItem->getNbdesignerJson()) {
                $data_design = $qItem->getNbdesignerSession();
            }
        }
        return isset($data_design) ? $data_design : "";
    }

    /**
     * show online design attach file on admin order item
     * @param $_item
     * @return string
     */
    public function showOnlineDesignAttachFile($_item)
    {
        $html = "";
        if ($data_design = $_item['nbdesigner_json']) {
            $html = $this->_dataData->getTilteList() . '<br /><div>';
            if (!empty(json_decode($data_design))) {
                foreach (json_decode($data_design) as $img) {
                    $url = $this->convert_path_to_url($img);
                   // $src = str_replace('thumbs', 'preview', $url);
                    $src = str_replace('/preview', '', $url);
                    $html .= '<img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $src . '"/>';
                }
                $html .= '</div>';
            }
        }

        return $html;
    }

    public function showDesignOrder($_item)
    {
        $objectManagerr = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManagerr->create('\Magento\Sales\Model\Order')->load($_item->getOrderId());
        $order->getAllVisibleItems();
        $orderItems = $order->getItemsCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', array('eq' => $_item->getSku()))
            ->load();
        $data_design = "";
        foreach ($orderItems as $sItem) {
            if (($sItem->getNbdesignerJson() != null || $sItem->getNbdesignerJson() != "") && $sItem->getNbdesignerSku() == $_item->getSku()) {
                $data_design = $sItem->getNbdesignerJson();
            }
        }
        $helper = $this->_dataData;
        if ($data_design != "") {
            $html = $helper->getTilteList() . '<br /><div>';
            $list = json_decode($data_design);
            foreach ($list as $img) {
                $url = $this->convert_path_to_url($img);
                $src = str_replace('thumbs', 'preview', $url);
                $html .= '<img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $src . '"/>';
            }
            $html .= '</div>';
            return $html;
        }
    }
    public function showDesignOrderEmail($orderId, $sku)
    {
        $objectManagerr = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManagerr->create('\Magento\Sales\Model\Order')->load($orderId);
        $order->getAllVisibleItems();
        $orderItems = $order->getItemsCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', array('eq' => $sku))
            ->load();
        $data_design = "";
        foreach ($orderItems as $sItem) {
            if (($sItem->getNbdesignerJson() != null || $sItem->getNbdesignerJson() != "") && $sItem->getNbdesignerSku() == $sku) {
                $data_design = $sItem->getNbdesignerJson();
            }
        }
        $helper = $this->_dataData;
        if ($data_design != "") {
            $html = $helper->getTilteList() . '<br /><div>';
            $list = json_decode($data_design);
            foreach ($list as $img) {
                $url = $this->convert_path_to_url($img);
                $src = str_replace('thumbs', 'preview', $url);
                $html .= '<img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $src . '"/>';
            }
            $html .= '</div>';
            return $html;
        }
    }
    public function showDesignReorder($_item, $orderId)
    {
        $objectManagerr = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManagerr->create('\Magento\Sales\Model\Order')->load($orderId);
        $order->getAllVisibleItems();
        $orderItems = $order->getItemsCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', array('eq' => $_item->getSku()))
            ->load();
        $data_design = "";
        foreach ($orderItems as $sItem) {
            if (($sItem->getNbdesignerJson() != null || $sItem->getNbdesignerJson() != "") && $sItem->getNbdesignerSku() == $_item->getSku()) {
                $data_design = $sItem->getNbdesignerJson();
            }
        }
        $helper = $this->_dataData;
        if ($data_design != "") {
            $html = $helper->getTilteList() . '<br /><div>';
            $list = json_decode($data_design);
            foreach ($list as $img) {
                $url = $this->convert_path_to_url($img);
                $src = str_replace('thumbs', 'preview', $url);
                $html .= '<img width="60" height="60" style="border-radius: 3px; border: 1px solid #ddd; margin-top: 5px; margin-right: 5px; display: inline-block;" src="' . $src . '"/>';
            }
            $html .= '</div>';
            return $html;
        }
    }
}