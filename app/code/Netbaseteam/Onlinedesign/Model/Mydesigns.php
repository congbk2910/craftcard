<?php

namespace Netbaseteam\Onlinedesign\Model;


/**
 * Onlinedesign Model
 *
 */
class Mydesigns extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Netbaseteam\Onlinedesign\Model\ResourceModel\Mydesigns');
    }

}
