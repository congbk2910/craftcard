<?php
namespace Netbaseteam\Onlinedesign\Model\Observer;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Sales\Model\Order\ItemFactory;

class Gotocheckout implements ObserverInterface
{
    protected $_productFactory;
    protected $_helper;
    protected $_request;
    protected $_session;
    protected $_cacheTypeList;
    protected $_salesOrderItemFactory;
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Session\SessionManager $session,
        ItemFactory $salesOrderItemFactory,
        TypeListInterface $cacheTypeList,
        \Magento\Framework\App\RequestInterface $request,
        \Netbaseteam\Onlinedesign\Helper\Data $helper,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableproduct
    ){
        $this->_salesOrderItemFactory = $salesOrderItemFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_productFactory = $productFactory;
        $this->_session = $session;
        $this->_request = $request;
        $this->_helper = $helper;
        $this->configurableproduct = $configurableproduct;
    }

    public function _getProductData($sku){
        $product = $this->_productFactory->create();
        return $product->loadByAttribute('sku', $sku);
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote_item = $observer->getEvent()->getQuoteItem();
        $postValues = $this->_request->getPostValue();
        $folderDesign = $this->_request->getParam('folder_design');
        $productId = isset($postValues['product']) ? $postValues['product'] : $quote_item->getProductId();
        $product = $quote_item->getProduct();
        // save online design in quote_ite
        $dataSession = $this->_session->getData();
        $session_id = $this->_session->getSessionId();
        $pid = (isset($dataSession['nbdesigner_pid']) && $dataSession['nbdesigner_pid']) ? $dataSession['nbdesigner_pid'] : '';
        if (isset($postValues['super_attribute']) && $product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            $attributes = $postValues['super_attribute'];
            $simple_product = $this->configurableproduct->getProductByAttributes($attributes, $product);
            $productId = $simple_product->getId();
            if($pid == $postValues['product'] ) {
                $pid = $productId;
            }
        }
        if($folderDesign) {
            $session_id = $folderDesign;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $helper = $objectManager->get("\Netbaseteam\Onlinedesign\Helper\Data");
            $path = $helper->plugin_path_data().'designs/'.$folderDesign.'/'.'preview'.'/';
            $nbdesigner_json = '';
            if (file_exists($path) && $handle = opendir($path)) {
                while (false !== ($entry = readdir($handle))) {
                    $files[] = $entry;
                }
                $images=preg_grep('/\.png$/i', $files);
                if (count($images)) {
                    foreach ($images as $image) {
                        $nbdesigner_json = trim($nbdesigner_json . ','. json_encode($path.$image), ',');
                    }
                }
                closedir($handle);
            }
            $quote_item->setData('nbdesigner_sku', $quote_item->getSku());
            $quote_item->setData('nbdesigner_json', '['.$nbdesigner_json.']');
            $quote_item->setData('nbdesigner_pid', $pid);
            $quote_item->setData('nbdesigner_session', $session_id);
        }
        $orderId = $this->_request->getParam('order_id');
//        if($orderId) {
//            $collection = $this->_salesOrderItemFactory->create()->getCollection()->addFieldToFilter('order_id', $orderId);
//            foreach ($collection as $item) {
//                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
//                $logger = new \Zend\Log\Logger();
//                $logger->addWriter($writer);
//                $logger->info($item->getProductId());
//                if($productId == $item->getProductId()) {
//                    $sessionFile = explode('/', $item->getSessionFile());
//                    $dataSession = isset($sessionFile[1]) && $sessionFile ? $sessionFile[1] : "";
//                    $quote_item->setData('nbdesigner_sku', $item->getSku());
//                    $quote_item->setData('nbdesigner_json', $item->getNbdesignerJson());
//                    $quote_item->setData('nbdesigner_pid', $item->getProductId());
//                    $quote_item->setData('nbdesigner_session', $dataSession);
//                }
//            }
//        }
        if($productId == $pid && !$folderDesign && !$orderId) {
            $design_json_file = (isset($dataSession['nbdesigner_json']) && $dataSession['nbdesigner_json']) ? $dataSession['nbdesigner_json'] : '';
            $quote_item->setData('nbdesigner_sku', $quote_item->getSku());
            $quote_item->setData('nbdesigner_json', $design_json_file);
            $quote_item->setData('nbdesigner_pid', $pid);
            $quote_item->setData('nbdesigner_session', $session_id);
            unset($dataSession['nbdesigner_pid']);
            unset($dataSession['nbdesigner_json']);
        }
        //end

        $product_type = $quote_item->getProductType();
        $parent_pid = $productId;
        $child_pid = $quote_item->getProductId();
        $child_sku = $quote_item->getSku();
        $output_dir = $this->_helper->getBaseUploadDir()."/".$parent_pid."/";
        $jsonFile = $output_dir.$session_id.'.json';
        if (file_exists($jsonFile)){
            $str    = file_get_contents($jsonFile);
            $rows   = json_decode($str, true);
            for($i=0; $i < count($rows); $i++){
                $tmp = array();$tmp1 = array();
                foreach($rows[$i] as $row) {
                    /* \zend_debug::dump($row["parent_pid"]); */
                    if ($row["parent_pid"] == $parent_pid && $row["child_pid"] == ""){
                        $tmp["order_id"] = "";
                        $tmp["file"] = $row["file"];
                        $tmp["parent_pid"] = $row["parent_pid"];
                        $tmp["child_pid"] = $child_pid;
                        $tmp["comment"] = $row["comment"];
                        $tmp["child_sku"] = $child_sku;
                        $tmp1[] = $tmp;
                    } else {
                        $tmp1[] = $row;
                    }

                }
                $content[]  = $tmp1;
            }
            file_put_contents($jsonFile, json_encode($content));

        }
        $quote_item->setSessionFile($parent_pid."/".$session_id);
        $this->_session->regenerateId();
    }
}
