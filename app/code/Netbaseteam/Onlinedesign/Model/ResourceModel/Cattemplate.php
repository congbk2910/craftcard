<?php

namespace Netbaseteam\Onlinedesign\Model\ResourceModel;

/**
 * Catart Resource Model
 */
class Cattemplate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected $_mainTable;
    protected function _construct()
    {
        $this->_mainTable = 'nbcattemplate';
        $this->_init($this->_mainTable, 'cat_id');
    }

    public function getTemplateImage($catId) {
        if ($catId) {
            $read = "Select template_image FROM " . $this->_mainTable . " Where cat_id  = " . $catId ;
            $result = $this->getConnection()->fetchAll($read);

            return $result;
        }
    }
}
