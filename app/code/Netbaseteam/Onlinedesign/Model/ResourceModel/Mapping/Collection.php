<?php

namespace Netbaseteam\Onlinedesign\Model\ResourceModel\Mapping;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection implements SearchResultInterface
{
    /**
     * @var string
     */
    protected $_idFieldName = 'template_mapping_id';
    /**
     * Define resource model.
     */

    public function __construct(\Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory, \Psr\Log\LoggerInterface $logger, \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy, \Magento\Framework\Event\ManagerInterface $eventManager, \Magento\Framework\DB\Adapter\AdapterInterface $connection = null, \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null)
    {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    protected function _construct()
    {
        $this->_init('Netbaseteam\Onlinedesign\Model\Mapping', 'Netbaseteam\Onlinedesign\Model\ResourceModel\Mapping');
    }

    public function setItems(array $items = null)
    {
        // TODO: Implement setItems() method.
    }

    public function getAggregations()
    {
        // TODO: Implement getAggregations() method.
    }

    public function setAggregations($aggregations)
    {
        // TODO: Implement setAggregations() method.
    }

    public function getSearchCriteria()
    {
        // TODO: Implement getSearchCriteria() method.
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    public function getTotalCount()
    {
        // TODO: Implement getTotalCount() method.
    }

    public function setTotalCount($totalCount)
    {
        // TODO: Implement setTotalCount() method.
    }
}
