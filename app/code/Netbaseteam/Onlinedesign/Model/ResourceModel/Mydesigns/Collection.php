<?php

namespace Netbaseteam\Onlinedesign\Model\ResourceModel\Mydesigns;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Netbaseteam\Onlinedesign\Model\Mydesigns', 'Netbaseteam\Onlinedesign\Model\ResourceModel\Mydesigns');
    }
}
