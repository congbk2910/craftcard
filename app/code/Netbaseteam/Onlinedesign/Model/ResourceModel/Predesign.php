<?php

namespace Netbaseteam\Onlinedesign\Model\ResourceModel;

class Predesign extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('nbdesigner_predesign', 'id');
    }
}