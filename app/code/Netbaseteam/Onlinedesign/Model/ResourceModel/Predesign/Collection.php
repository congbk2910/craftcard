<?php
namespace Netbaseteam\Onlinedesign\Model\ResourceModel\Predesign;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Netbaseteam\Onlinedesign\Model\Predesign', 'Netbaseteam\Onlinedesign\Model\ResourceModel\Predesign');
    }

}
