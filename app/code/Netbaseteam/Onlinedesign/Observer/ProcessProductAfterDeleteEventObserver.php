<?php
namespace Netbaseteam\Onlinedesign\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProcessProductAfterDeleteEventObserver implements ObserverInterface
{
    /**
     * Call an API to product delete from ERP 
     * after delete product from Magento
     *
     * @param   Observer $observer
     * @return  $this
     */
    public function execute(Observer $observer)
    {
        $eventProduct = $observer->getEvent()->getProduct();
        $productId = $eventProduct->getId();
        if ($productId) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resources = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection= $resources->getConnection();
            $table_name =  $resources->getTableName('nb_onlinedesign');
            $table_name_store =  $resources->getTableName('nb_onlinedesign_store');

            $sql = "DELETE FROM {$table_name} WHERE product_id = {$productId}";
            $sql_store = "DELETE FROM {$table_name_store} WHERE product_id = {$productId}";
            $connection->query($sql);
            $connection->query($sql_store);
        }

        return $this;
    }
}