<?php

namespace Netbaseteam\Onlinedesign\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\ItemFactory;
class ReorderDesigns implements ObserverInterface {

    /**
     * @var \Magento\Framework\App\Request\Http
     */

    protected $_request;
    /**
     * @var ItemFactory
     */
    protected $_salesOrderItemFactory;

    public function __construct(
        ItemFactory $salesOrderItemFactory,
        \Magento\Framework\App\Request\Http $request
    )
    {
        $this->_salesOrderItemFactory = $salesOrderItemFactory;
        $this->_request = $request;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getData('order');
        $oldOrderId = $this->_request->getParam('old_order_id');
        if($oldOrderId) {
            $collection = $this->_salesOrderItemFactory->create()->getCollection()->addFieldToFilter('order_id', $oldOrderId);
            $newOrder = $this->_salesOrderItemFactory->create()->getCollection()->addFieldToFilter('order_id', $order->getId());
            foreach ($collection as $item) {
                $productId = $item->getProductId();
                foreach ($newOrder as $data) {
                    if($productId == $data->getProductId()) {
                        $sessionFile = explode('/', $item->getSessionFile());
                        $dataSession = isset($sessionFile[1]) && $sessionFile ? $sessionFile[1] : "";
                        $data->setData('nbdesigner_sku', $item->getSku());
                        $data->setData('nbdesigner_json', $item->getNbdesignerJson());
                        $data->setData('nbdesigner_pid', $item->getProductId());
                        $data->setData('nbdesigner_session', $dataSession);
                        $data->save();
                    }
                }
            }
        }
    }
}