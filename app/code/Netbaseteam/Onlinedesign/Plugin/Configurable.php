<?php
namespace Netbaseteam\Onlinedesign\Plugin;

/**
 * HTTP response plugin for frontend.
 */
class Configurable
{


    public function aroundGetJsonConfig(\Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject, callable $proceed )
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $helper = $objectManager->create('Magento\ConfigurableProduct\Helper\Data');
        $decode = $objectManager->create('Magento\Framework\Json\DecoderInterface');
        $encode = $objectManager->create('Magento\Framework\Json\EncoderInterface');
        $result = $proceed();

        $data = $decode->decode($result);
        $currentProduct = $subject->getProduct();
        $options = $helper->getOptions($currentProduct, $subject->getAllowProducts());

        if (count($options["index"])){
            foreach ($options["index"] as $k=>$v) {
                $default = $options["index"][$k];
                foreach ($default as $key=>$value) {
                    $data['defaultValues'][$key] = $value;
                }
                break;
            }
        }

        $defaultValues = [];

        foreach ($data['attributes'] as $attributeId => $attribute) {
            foreach ($attribute['options'] as $option) {
                $optionId = $option['id'];
                $defaultValues[$attribute['code']] = $optionId;
                break;
            }
        }

        $data['preselect'] = $defaultValues;

        $newRe = $encode->encode($data);
        return $newRe;
    }
}
