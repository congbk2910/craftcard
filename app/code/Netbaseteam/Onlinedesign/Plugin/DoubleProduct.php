<?php
namespace Netbaseteam\Onlinedesign\Plugin;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Copier;
use Netbaseteam\Onlinedesign\Model\OnlinedesignFactory as Onlinedesign;
use Netbaseteam\Onlinedesign\Model\TemplateFactory as Template;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Netbaseteam\Onlinedesign\Helper\Data;

/**
 * HTTP response plugin for frontend.
 */
class DoubleProduct
{
    private $onlinedesign;
    private $templateFactory;
    private $fileSystem;
    protected $fileSystemIo;
    protected $helper;

    public function __construct(
        Onlinedesign $onlinedesign,
        Template $templateFactory,
        Filesystem $filesystem,
        File $filesystemIo,
        Data $helper
    )
    {
        $this->onlinedesign = $onlinedesign;
        $this->templateFactory = $templateFactory;
        $this->fileSystem = $filesystem;
        $this->fileSystemIo = $filesystemIo;
        $this->helper = $helper;
    }

    public function afterCopy(
        Copier $subject,
        Product $result,
        Product $product
    ) {
        $newProductId = $result->getId();
        $curProductId = $product->getId();
        $designId='';
        $onlinedesign = $this->onlinedesign->create();
        $collection = $onlinedesign->getCollection();

        foreach ($collection->getData() as $v) {
            if ($v['product_id'] == $curProductId) {
                $designId = $v['onlinedesign_id'];
            }
        }

        if (!empty($designId)) {
            $oldInfDesign = $onlinedesign->load($designId);
            $data = $oldInfDesign->getData();
            unset($data['onlinedesign_id']);
            $data['product_id'] = $newProductId;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $model = $objectManager->create('Netbaseteam\Onlinedesign\Model\Onlinedesign');
            $model->addData($data);
            $model->save();
        }

        $templateCollec = $this->templateFactory->create()->getCollection();

        foreach ($templateCollec->getData() as $value) {
            if($value['product_id'] == $curProductId) {
                $this->cloneTemplate($newProductId, $value);
            }
        }

//        predesign
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resources = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection= $resources->getConnection();

        $table_name =  $resources->getTableName('nbdesigner_predesign');
        $sql = "SELECT * FROM {$table_name} WHERE product_id = {$curProductId}";
        $designs = $connection->fetchAll($sql);

        if (isset($designs[0]) && $designs[0]) {
            $this->clonePredesign($newProductId, $designs[0]);
        }


        return $result;
    }

    public function clonePredesign($productId, $row){
        $nameFolder = substr(md5(uniqid()), 0, 5) . rand(1, 100) . time();
        $this->coppyFolder($nameFolder, $row);

        $product_id = $productId;
        $name = $row['name'];
        $image = str_replace($row['folder'],$nameFolder,$row['image']);
        require_once $this->helper->getLibOnlineDesign() . '/Onlinedesign/includes/class.nbdesigner.php';
        $nbdesigner = new \Nbdesigner_Plugin();

        $nbdesigner->nbdesigner_insert_table_predesign($product_id, $nameFolder, $image, $name);
    }

    public function cloneTemplate($productId, $row) {
        $nameFolder = substr(md5(uniqid()), 0, 5) . rand(1, 100) . time();
        $this->coppyFolder($nameFolder, $row);

        $product_id = $productId;
        $variation_id = $row['variation_id'];
        $nbd_item_key = $nameFolder;
        $image = str_replace($row['folder'],$nameFolder,$row['image']);
        $name = $row['name'];
//        $colors = $row['colors'];
        $catId = $row['cat_id'];

        require_once $this->helper->getLibOnlineDesign() . '/Onlinedesign/includes/class.nbdesigner.php';
        $nbdesigner = new \Nbdesigner_Plugin();

        $nbdesigner->nbdesigner_insert_table_templates($product_id, $variation_id, $nbd_item_key, $image, $name, 0, 1, 0,$catId);

    }

    public function coppyFolder($name, $oldinf){
        $mediapath = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath().'nbdesigner/designs/'.$name;
        $framePath = $mediapath.'/preview';
        $oldpath = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath().'nbdesigner/designs/'.$oldinf['folder'];
        $oldFramepath = $oldpath.'/preview';
        $this->fileSystemIo->mkdir($mediapath, 0775);
        $this->fileSystemIo->mkdir($framePath, 0775);

        $FramePath = $oldFramepath.'/frame_0.png';
        $copyFramePath  = $framePath.'/frame_0.png';
        $this->fileSystemIo->cp($FramePath, $copyFramePath);

        $file = [
            '/frame_0.png',
            '/frame_0_svg.svg',
            '/used_font.json',
            '/upload.json',
            '/product.json',
            '/option.json',
            '/design.json',
            '/config.json'
        ];
        foreach ($file as $filename) {
            $newFilePath  = $oldpath.$filename;
            $oldFilePath  = $mediapath.$filename;
            $this->fileSystemIo->cp($newFilePath, $oldFilePath);
        }
    }
}
