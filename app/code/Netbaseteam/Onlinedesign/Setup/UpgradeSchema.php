<?php

namespace Netbaseteam\Onlinedesign\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface {
	public function upgrade(SchemaSetupInterface $setup,
		ModuleContextInterface $context) {
		$setup->startSetup();
		if (version_compare($context->getVersion(), '1.0.1') < 0) {

			// Get module table
			$tableName = $setup->getTable('sales_order_item');

			// Check if the table already exists
			if ($setup->getConnection()->isTableExists($tableName) == true) {
				// Declare data
				$columns = [
					'nbdesigner_json' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_json',
					],
					'nbdesigner_pid' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_pid',
					],
					'nbdesigner_session' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_session',
					],
					'nbdesigner_src' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_src',
					],
					'nbdesigner_sku' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_sku',
					],
				];

				$connection = $setup->getConnection();
				foreach ($columns as $name => $definition) {
					$connection->addColumn($tableName, $name, $definition);
				}
			}

			// Get module table
			$tableName = $setup->getTable('quote_item');

			// Check if the table already exists
			if ($setup->getConnection()->isTableExists($tableName) == true) {
				// Declare data
				$columns = [
					'nbdesigner_json' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_json',
					],
					'nbdesigner_pid' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_pid',
					],
					'nbdesigner_session' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_session',
					],
					'nbdesigner_src' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_src',
					],
					'nbdesigner_sku' => [
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => false,
						'comment' => 'nbdesigner_sku',
					],
				];

				$connection2 = $setup->getConnection();
				foreach ($columns as $name => $definition) {
					$connection2->addColumn($tableName, $name, $definition);
				}
			}
		}

		if (version_compare($context->getVersion(), '1.0.5') < 0) {
			// Get module table
			$tableName = $setup->getTable('nb_onlinedesign');
			// Check if the table already exists
			if ($setup->getConnection()->isTableExists($tableName) == true) {
				$setup->getConnection()->addColumn(
					$setup->getTable('nb_onlinedesign'),
					'design_image',
					[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'nullable' => true,
						'comment' => 'Design Image',
					]
				);
			}
		}
        if (version_compare($context->getVersion(), '4.4.5') < 0) {
            // Get module table
            $tableName = $setup->getTable('nb_onlinedesign');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('nb_onlinedesign'),
                    'folding_map',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'comment' => 'Show/Hide Folding Map',
                    ]
                );
            }
        }


        if (version_compare($context->getVersion(), '3.1.5') < 0) {
            // Get module table
            $tableName = $setup->getTable('nb_onlinedesign');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('nb_onlinedesign'),
                    'status_upload_design',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Status Upload Design',
                    ]
                );
            }
        }
        if (version_compare($context->getVersion(), '4.0.1') < 0) {
            // Get module table
            $tableName = $setup->getTable('nb_onlinedesign');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('nb_onlinedesign'),
                    'use_visual_layout',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Use Visual Layout',
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '4.3.0') < 0) {
            /**
             * Create table 'nb_template_mapping'
             */
            $table = $setup->getConnection()
                ->newTable($setup->getTable('nb_template_mapping'))
                ->addColumn(
                    'template_mapping_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Template Mapping Id'
                )
                ->addColumn(
                    'field_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Field Name'
                )
                ->addColumn(
                    'connect_field',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Store Id'
                )
                ->setComment(
                    'Template Mapping Item'
                );

            $setup->getConnection()->createTable($table);
        }


		if (version_compare($context->getVersion(), '1.1.0') < 0) {
			/**
			 * Create table 'nb_onlinedesign_store'
			 */
			$table = $setup->getConnection()
				->newTable($setup->getTable('nb_onlinedesign_store'))
				->addColumn(
					'onlinedesign_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					['unsigned' => true, 'nullable' => false, 'primary' => true],
					'Entity Id'
				)
				->addColumn(
					'product_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					['unsigned' => true, 'nullable' => false],
					'Product Id'
				)
				->addColumn(
					'store_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					['unsigned' => true, 'nullable' => false],
					'Store Id'
				)
				->addColumn(
					'design_image',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					null,
					['unsigned' => true, 'nullable' => false],
					'Design Image'
				)
				->setComment('Blocks To Stores Relations');

			$setup->getConnection()->createTable($table);
		}

        if (version_compare($context->getVersion(), '4.3.8') < 0) {
            /**
             * Create table 'nb_shapes'
             */
            $table = $setup->getConnection()
                ->newTable($setup->getTable('nb_shapes'))
                ->addColumn(
                    'shapes_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'auto_increment' => true, 'nullable' => false, 'primary' => true],
                    'Shapes Id'
                )
                ->addColumn(
                    'file_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Filed Name'
                )
                ->addColumn(
                    'title',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Title'
                )
                ->setComment('Shapes List on Editor');

            $setup->getConnection()->createTable($table);
        }

		if (version_compare($context->getVersion(), '3.1.2', '<')) {
			$nbCatArtTable = $setup->getTable('nb_catart');
			if ($setup->getConnection()->isTableExists($nbCatArtTable) == true) {
				$setup->getConnection()->addColumn(
					$setup->getTable($nbCatArtTable),
					'status',
					[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
						'unsigned' => true,
						'nullable' => true,
						'default' => false,
						'comment' => 'Status category',
					]
				);
			}
		}

        if (version_compare($context->getVersion(), '4.0.3', '<')) {
            $nbTemplate = $setup->getTable('nbdesigner_templates');
            if ($setup->getConnection()->isTableExists($nbTemplate) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable($nbTemplate),
                    'image',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'unsigned' => true,
                        'nullable' => true,
                        'default' => false,
                        'comment' => 'Image Template',
                    ]
                );
            }
        }

		if (version_compare($context->getVersion(), '3.1.4', '<')) {
			$nbCatArtTable = $setup->getTable('nb_catart');
			if ($setup->getConnection()->isTableExists($nbCatArtTable) == true) {
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$sql = "ALTER TABLE " . $nbCatArtTable . " AUTO_INCREMENT = 12";
				$setup->getConnection()->query($sql);
			}
		}

        if (version_compare($context->getVersion(), '4.3.2') < 0) {
            $previewTable = $setup->getTable('netbaseteam_onlinedesign_preview');
            if ($setup->getConnection()->isTableExists($previewTable) != true) {
                $tablePreview = $setup->getConnection()
                    ->newTable($previewTable)
                    ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'Id'
                    )
                    ->addColumn(
                        'preview_content',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        ['nullable' => false, 'default' => 1],
                        'Onlinedesign Preview'
                    )
                    ->setComment('Onlinedesign Preview ');
                $setup->getConnection()->createTable($tablePreview);
            }
        }

        if (version_compare($context->getVersion(), '4.3.3', '<')) {
            $nbTemplate = $setup->getTable('netbaseteam_onlinedesign_preview');
            if ($setup->getConnection()->isTableExists($nbTemplate) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable($nbTemplate),
                    'product_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'unsigned' => true,
                        'nullable' => true,
                        'default' => false,
                        'comment' => 'Product Id',
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '4.3.4', '<')) {
            $nbTemplate = $setup->getTable('netbaseteam_onlinedesign_preview');
            if ($setup->getConnection()->isTableExists($nbTemplate) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable($nbTemplate),
                    'folder',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'unsigned' => true,
                        'nullable' => true,
                        'default' => false,
                        'comment' => 'Folder',
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '4.4.0', '<')) {
            $nbTemplate = $setup->getTable('nbcattemplate');
            if ($setup->getConnection()->isTableExists($nbTemplate) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable($nbTemplate),
                    'parent_cate_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'unsigned' => true,
                        'nullable' => true,
                        'default' => false,
                        'comment' => 'Parent Category Id',
                    ]
                );
            }
        }

		if (version_compare($context->getVersion(), '3.1.6', '<')) {
			/**
             * Create table 'nbdesigner_mydesigns'
             */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('nbdesigner_mydesigns')
	            )->addColumn(
	                'id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
	                'Id'
	            )->addColumn(
	                'user_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'User Id'
	            )->addColumn(
	                'folder',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                255,
	                ['unsigned' => true, 'nullable' => true],
	                'Folder'
	            )->addColumn(
	                'product_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Product Id'
	            )->addColumn(
	                'variation_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Variation Id'
	            )->addColumn(
	                'price',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                255,
	                ['unsigned' => true, 'nullable' => true],
	                'Price'
	            )->addColumn(
                	'selling',
	                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
	                null,
	                ['nullable' => false, 'default' => '0'],
	                'Selling'
	            )
	            ->addColumn(
	                'vote',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Vote'
	            )
	            ->addColumn(
	                'publish',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Publish'
	            )
	            ->addColumn(
                'created_date',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
	                null,
	                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
	                'Created Date'
	            )
	            ->addColumn(
	                'hit',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Hit'
	            )
	            ->addColumn(
	                'sales',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Sales'
	            );
            $setup->getConnection()->createTable($table);
		}

        if (version_compare($context->getVersion(), '3.1.7') < 0) {
            // Get module table
            $tableName = $setup->getTable('quote_item');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('quote_item'),
                    'session_file',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Session File Design Upload',
                    ]
                );
            }
            $tableNameSales = $setup->getTable('sales_order_item');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableNameSales) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('sales_order_item'),
                    'session_file',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Session File Design Upload',
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '3.1.8', '<')) {
			/**
             * Create table 'nbdesigner_templates'
             */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('nbdesigner_templates')
	            )->addColumn(
	                'id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
	                'Id'
	            )->addColumn(
	                'user_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'User Id'
	            )->addColumn(
	                'folder',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                255,
	                ['unsigned' => true, 'nullable' => true],
	                'Folder'
	            )->addColumn(
	                'product_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Product Id'
	            )->addColumn(
	                'variation_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Variation Id'
	            )->addColumn(
	                'vote',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Vote'
	            )->addColumn(
	                'publish',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Publish'
	            )->addColumn(
	                'private',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Private'
	            )->addColumn(
	                'priority',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Priority'
	            )
	            ->addColumn(
                'created_date',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
	                null,
	                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
	                'Created Date'
	            )
	            ->addColumn(
	                'hit',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Hit'
	            )
	            ->addColumn(
	                'sales',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Sales'
	            )->addColumn(
	                'name',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                255,
	                ['unsigned' => true, 'nullable' => true],
	                'Name'
	            )->addColumn(
	                'tags',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                255,
	                ['unsigned' => true, 'nullable' => true],
	                'Tags'
	            )->addColumn(
	                'thumbnail',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Thumbnail'
	            );
            $setup->getConnection()->createTable($table);
		}

		if (version_compare($context->getVersion(), '4.3.1') < 0) {
			$setup->getConnection()->changeColumn(
               	$setup->getTable('nb_onlinedesign_store'),
               	'onlinedesign_id',
               	'onlinedesign_id',
               	[
                   	'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                   	'identity' => true,
                   	'unsigned' => true,
                   	'nullable' => false,
                   	'primary' => true
               	]
           	);
        }

        if (version_compare($context->getVersion(), '4.3.5', '<')) {
            /**
             * Create table 'nbdesigner_catTem'
             */
            $tableCatTemplate = $setup->getTable('nbcattemplate');
            if (!$setup->getConnection()->isTableExists($tableCatTemplate)) {
                $table = $setup->getConnection()->newTable(
                    $setup->getTable('nbcattemplate')
                )->addColumn(
                    'cat_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Entity Id'
                )->addColumn(
                    'title',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Category Template'
                )->addColumn(
                    'template_image',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Template Image'
                );
                $setup->getConnection()->createTable($table);
            }

            //add the column cat_id
            $nbTemplate = $setup->getTable('nbdesigner_templates');
            $connection = $setup->getConnection();
            if (!$connection->tableColumnExists($setup->getTable('nbdesigner_templates'), 'cat_id')) {
                $setup->getConnection()->addColumn(
                    $setup->getTable($nbTemplate),
                    'cat_id',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'unsigned' => true,
                        'nullable' => true,
                        'default' => false,
                        'comment' => 'cat Template ID',
                    ]
                );
            }
        }
        
        if (version_compare($context->getVersion(), '4.4.1') < 0) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('nbcattemplate'),
                'cat_id',
                'cat_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'identity' => true,
                    'unsigned' => true,
                    'auto_increment' => true,
                    'nullable' => false,
                    'primary' => true
                ]
            );
        }

        //        add special color
        if (version_compare($context->getVersion(), '4.4.2', '<')) {
            /**
             * Create table 'nb_color_for product'
             */
            $tableCatTemplate = $setup->getTable('nb_special_color');
            if (!$setup->getConnection()->isTableExists($tableCatTemplate)) {
                $table = $setup->getConnection()->newTable(
                    $setup->getTable('nb_special_color')
                )->addColumn(
                    'color_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Entity Id'
                )->addColumn(
                    'color_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'color_name'
                )->addColumn(
                    'hex',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'hex'
                )->addColumn(
                    'product_ids',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    1000,
                    ['nullable' => true],
                    'product ids'
                );
                $setup->getConnection()->createTable($table);
            }
        }

        if (version_compare($context->getVersion(), '4.4.3') < 0) {
            /**
             * Create table 'nbdesigner_predesign'
             */
            $table = $setup->getConnection()->newTable(
                $setup->getTable('nbdesigner_predesign')
	            )->addColumn(
	                'id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
	                'Id'
	            )->addColumn(
	                'user_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'User Id'
	            )->addColumn(
	                'folder',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                255,
	                ['unsigned' => true, 'nullable' => true],
	                'Folder'
	            )->addColumn(
	                'product_id',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Product Id'
	            )->addColumn(
	                'name',
	                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	                255,
	                ['unsigned' => true, 'nullable' => true],
	                'Name'
	            )->addColumn(
	                'thumbnail',
	                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	                null,
	                ['unsigned' => true, 'nullable' => true],
	                'Thumbnail'
	            );
            $setup->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '4.4.4', '<')) {
            $nbPredesign = $setup->getTable('nbdesigner_predesign');
            if ($setup->getConnection()->isTableExists($nbPredesign) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable($nbPredesign),
                    'image',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'unsigned' => true,
                        'nullable' => true,
                        'default' => false,
                        'comment' => 'Image Predesign',
                    ]
                );
            }
        }

		$setup->endSetup();
	}
}