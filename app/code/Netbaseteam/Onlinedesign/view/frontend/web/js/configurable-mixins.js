define([
    'jquery',
], function ($) {
    'use strict';

    return function (widget) {
        $.widget('mage.configurable', widget, {
            /**
             * Override default options values settings with either URL query parameters or
             * initialized inputs values.
             * @private
             */
            _overrideDefaults: function () {


                var nbIndex = window.location.href.indexOf('&attribute');
                if(nbIndex !== -1) {
                    var query = window.location.href.substr(nbIndex + 1);
                    // var childIndex = query.indexOf('&');
                    // var optionQuery = query.replace('attribute','');
                    var optionQuery =  query.split('attribute').join('');
                    this._parseQueryParams(optionQuery);
                }
                this._super();
                // this._setInitialOptionsLabels();
            },

            _fillSelect: function (element){
                this._super(element);
                element.remove(0);

            },
            _configureForValues: function () {
                this._super();
                if (this.options.spConfig.defaultValues) {
                    var dataGallery = $(this.options.mediaGallerySelector),
                        option = this.options.spConfig.defaultValues,
                        self = this;

                    var interval = setInterval(function () {
                        var galleryObject = dataGallery.data('gallery');
                        if(galleryObject) {
                            self._changeProductImage();
                            clearInterval(interval);
                        };
                    }, 100);
                }
            },
        })

        return $.mage.configurable;
    };
});
