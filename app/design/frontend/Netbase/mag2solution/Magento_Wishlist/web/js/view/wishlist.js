/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Customer/js/customer-data'
], function (Component, customerData) {
    'use strict';

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.wishlist = customerData.get('wishlist');
            jQuery('.wishlist-item .link.wishlist .counter').addClass('nb-custom-wl');
            var sections = ['wishlist'];
            customerData.invalidate(sections);
            customerData.reload(sections, true);
        }
    });
});
